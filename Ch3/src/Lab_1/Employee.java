package Lab_1;

public class Employee {
	
	//Instance Fields
	private String _name;
	private int _idNumber;
	private String _department;
	private String _position;
	
	//Constructor
	public Employee()
	{
		
	}
	
	public Employee(String name, int idNum, String department, String position)
	{
		_name = name;
		_idNumber = idNum;
		_department = department;
		_position = position;
	}
	
	//Mutators
	public void setName(String name)
	{
		_name = name;
	}
	
	public void setIdNumber(int idNum)
	{
		_idNumber = idNum;
	}
	
	public void setDepartment(String department)
	{
		_department = department;
	}
	
	public void setPosition(String position)
	{
		_position = position;
	}
	
	
	//Accessors
	public String getName()
	{
		return _name;
	}
	
	public int getIdNumber()
	{
		return _idNumber;
	}
	
	public String getDepartment()
	{
		return _department;
	}
	
	public String getPosition()
	{
		return _position + "\n";
	}
}
