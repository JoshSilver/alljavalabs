package Lab_1;

public class EmployeeDriver {

	public static void main(String[] args) {
		
		//instantiate employees
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		
		//using a constructor
		Employee e4 = new Employee("Josh", 12345, "IT", "Programmer");
		
		//Susans' Info
		e1.setName("Susan Meyers");
		e1.setIdNumber(47899);
		e1.setDepartment("Accounting");
		e1.setPosition("Vice President");
		
		//Marks' Info
		e2.setName("Mark Jones");
		e2.setIdNumber(39119);
		e2.setDepartment("IT");
		e2.setPosition("Programmer");
		
		//Joys' Info
		e3.setName("Joy Rogers");
		e3.setIdNumber(81774);
		e3.setDepartment("Manufacturing");
		e3.setPosition("Engineer");
		
		//print out all info
		System.out.println(e4.getName());
		System.out.println(e4.getIdNumber());
		System.out.println(e4.getDepartment());
		System.out.println(e4.getPosition());
		
		System.out.println(e1.getName());
		System.out.println(e1.getIdNumber());
		System.out.println(e1.getDepartment());
		System.out.println(e1.getPosition());
		
		System.out.println(e2.getName());
		System.out.println(e2.getIdNumber());
		System.out.println(e2.getDepartment());
		System.out.println(e2.getPosition());
		
		System.out.println(e3.getName());
		System.out.println(e3.getIdNumber());
		System.out.println(e3.getDepartment());
		System.out.println(e3.getPosition());
	}

}
