package Lab_9;

public class Circle 
{
	//Instance Fields
	private double radius;
	private final double PI = 3.14159;
	
	//Constructor
	public Circle()
	{
		
	}
	public Circle(double radius)
	{
		this.radius = radius;
	}
	
	//Mutator Method
	public void setRadius(double radius)
	{
		this.radius = radius;
	}
	
	//Accessor Method
	public double getRadius()
	{
		return this.radius;
	}
	
	//Instance Method
	public double getArea()
	{
		double area = PI * (this.radius * this.radius);
		return area;
	}
	public double getDiameter()
	{
		double diameter = this.radius * 2f;
		return diameter;
	}
	public double getCircumference()
	{
		double circumference = 2f * PI * this.radius;
		return circumference;
	}
}
