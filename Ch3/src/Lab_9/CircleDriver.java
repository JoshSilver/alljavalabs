package Lab_9;
import java.util.Scanner;

public class CircleDriver {

	public static void main(String[] args) 
	{
		//Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);
		
		//ask user for the radius of a circle
		System.out.println("Input the radius of the circle...");
		double radius = keyboard.nextDouble();
		keyboard.close();
		
		//Instantiate circle object with constructor
		Circle circle = new Circle(radius);
		
		//Print Information
		System.out.printf("Area: %.2f\n", circle.getArea());
		System.out.printf("Diameter: %.2f\n", circle.getDiameter());
		System.out.printf("Circumference: %.2f\n", circle.getCircumference());
	}
}
