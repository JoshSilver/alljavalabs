package Lab_4;
import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) 
	{
		//Instance Fields
		double temp;
		
		//Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);
		
		//Request the temp in Fahrenheit
		System.out.println("What is the temperature in Fahrenheit?");
		temp = keyboard.nextDouble();
		
		//Instantiate Temperature Class
		Temperature ftemp = new Temperature(temp);
		
		//print Celsius
		System.out.printf("In Celsius the temperature is %.2f.", ftemp.getCelsius());
		
		System.out.println();
		
		//print Kelvin
		System.out.printf("In Kelvin the temperature is %.2f.", ftemp.getKelvin());
		
		//Close Scanner
		keyboard.close();
	}

}
