package Lab_4;

public class Temperature 
{
	//Instance Fields
	private double ftemp;
	
	//Constructor
	public Temperature(double ftemp)
	{
		this.ftemp = ftemp;
	}
	public Temperature()
	{
		
	}
	
	//Mutator Methods
	public void setFahrenheit(double ftemp)
	{
		this.ftemp = ftemp;
	}
	
	//Accessor Methods
	public double getFahrenheit()
	{
		return ftemp;
	}
	
	//Instance Methods
	public double getCelsius()
	{
		double celsius = (5f/9f) * this.ftemp - 32;
		return celsius;
	}
	public double getKelvin()
	{
		double kelvin = ((5f/9f) * (this.ftemp - 32)) + 273;
		return kelvin;
	}
}
