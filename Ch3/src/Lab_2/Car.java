package Lab_2;

public class Car 
{
	//Instance Fields
	private int yearModel;
	private String make;
	private int speed;
	
	//constructor
	public Car()
	{
		
	}
	public Car(int yearModel, String make)
	{
		this.yearModel = yearModel;
		this.make = make;
		this.speed = 0;
	}
	
	//accessor
	public int getYearModel()
	{
		return yearModel;
	}
	public String getMake()
	{
		return make;
	}
	public int getSpeed()
	{
		return speed;
	}
	
	//Instance Methods
	public void accelerate()
	{
		this.speed += 5;
	}
	public void brake()
	{
		this.speed -= 5;
	}
}
