package Lab_2;

public class CarDriver {

	public static void main(String[] args) 
	{
		//instantiate Car
		Car car1 = new Car(2018, "Corvette");
		
		//use accelerate method and print 5 times
		System.out.println("Speed = " + car1.getSpeed());
		car1.accelerate();
		System.out.println("Speed = " + car1.getSpeed());
		car1.accelerate();
		System.out.println("Speed = " + car1.getSpeed());
		car1.accelerate();
		System.out.println("Speed = " + car1.getSpeed());
		car1.accelerate();
		System.out.println("Speed = " + car1.getSpeed());
		car1.accelerate();
		System.out.println("Speed = " + car1.getSpeed());
		
		car1.brake();
		System.out.println("Speed = " + car1.getSpeed());
		car1.brake();
		System.out.println("Speed = " + car1.getSpeed());
		car1.brake();
		System.out.println("Speed = " + car1.getSpeed());
		car1.brake();
		System.out.println("Speed = " + car1.getSpeed());
		car1.brake();
		System.out.println("Speed = " + car1.getSpeed());
	}

}
