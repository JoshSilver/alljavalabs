package Lab_3;

public class PersonalInformation 
{
	// Instance Fields
	private String name;
	private String address;
	private int age;
	private String phoneNumber;
	
	//Mutator Methods
	public void setName(String name)
	{
		this.name = name;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
	public void setAge(int age)
	{
		this.age = age;
	}
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	
	//Accessor Methods
	public String getName()
	{
		return name;
	}
	public String getAddress()
	{
		return address;
	}
	public int getAge()
	{
		return age;
	}
	public String getPhoneNumber()
	{
		return phoneNumber;
	}
}
