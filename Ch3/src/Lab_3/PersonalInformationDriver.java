package Lab_3;

public class PersonalInformationDriver {

	public static void main(String[] args) 
	{
		//Instantiate 3 Objects
		PersonalInformation person1 = new PersonalInformation();
		PersonalInformation person2 = new PersonalInformation();
		PersonalInformation person3 = new PersonalInformation();
		
		//Set Attributes
		person1.setName("Josh");
		person1.setAddress("2165 Flamingo Dr.");
		person1.setAge(27);
		person1.setPhoneNumber("(314)680-5144");
		
		person2.setName("Lindsey");
		person2.setAddress("2165 Flamingo Dr.");
		person2.setAge(27);
		person2.setPhoneNumber("(314)837-3706");
		
		person3.setName("Nick");
		person3.setAddress("19 Holiday Dr.");
		person3.setAge(27);
		person3.setPhoneNumber("(314)956-0219");
		
		//Print Information
		System.out.println(person1.getName());
		System.out.println(person1.getAddress());
		System.out.println(person1.getAge());
		System.out.println(person1.getPhoneNumber());
		
		System.out.println();
		
		System.out.println(person2.getName());
		System.out.println(person2.getAddress());
		System.out.println(person2.getAge());
		System.out.println(person2.getPhoneNumber());
		
		System.out.println();
	
		System.out.println(person3.getName());
		System.out.println(person3.getAddress());
		System.out.println(person3.getAge());
		System.out.println(person3.getPhoneNumber());
	}

}
