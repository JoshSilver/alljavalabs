package Lab_7;

public class WidgetFactory 
{
	//Instance Variables
	private int numWidgetsProduced;
	
	//Mutator Method
	public void setNumWidgetsProduced(int numWidgetsProduced)
	{
		this.numWidgetsProduced = numWidgetsProduced;
	}
	
	//Instance Method
	public String numOfDays()
	{
		final int WIDGETSPERHOUR = 10;
		final int HOURSPERDAYWORKED = 16;
		final float WIDGETSPERDAY = ((float)WIDGETSPERHOUR * (float)HOURSPERDAYWORKED);
		float numOfDays = (float)this.numWidgetsProduced / WIDGETSPERDAY;
		return "It will take " + numOfDays + " day/s to make those Widgets.";
	}
}
