package Lab_7;
import java.util.Scanner;

public class WidgetFactoryDriver {

	public static void main(String[] args) 
	{
		//Variables
		int numOfWidgets;
		
		//Instantiate Object
		WidgetFactory widgetFactory = new WidgetFactory();
		
		//Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);
		
		//Ask how many Widgets to be made
		System.out.println("How many Widgets do you want made?");
		numOfWidgets = keyboard.nextInt();
		keyboard.close();
		
		//call setNumWidgetsProduced()
		widgetFactory.setNumWidgetsProduced(numOfWidgets);
		
		//Print Information
		System.out.println(widgetFactory.numOfDays());
	}

}
