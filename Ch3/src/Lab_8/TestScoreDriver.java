package Lab_8;
import java.util.Scanner;

public class TestScoreDriver {

	public static void main(String[] args) 
	{
		//Instantiate Scanner and TestScores Object
		Scanner keyboard = new Scanner(System.in);
		TestScores testScores = new TestScores();
		
		//request 3 test scores from user and set Instance Variables of the testScores object
		System.out.println("What is the score of the first test?");
		double score1 = keyboard.nextDouble();
		testScores.setTestScore1(score1);
		System.out.println("What is the score of the second test?");
		double score2 = keyboard.nextDouble();
		testScores.setTestScore2(score2);
		System.out.println("What is the score of the third test?");
		double score3 = keyboard.nextDouble();
		testScores.setTestScore3(score3);
		keyboard.close();
		
		System.out.printf("The average of the three test scores is %.2f.", testScores.calcAverage());
	}

}
