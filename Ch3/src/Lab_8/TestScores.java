package Lab_8;

public class TestScores 
{
	//Instance Fields
	private double testScore1;
	private double testScore2;
	private double testScore3;
	
	//Mutator Methods
	public void setTestScore1(double testScore)
	{
		this.testScore1 = testScore;
	}
	public void setTestScore2(double testScore)
	{
		this.testScore2 = testScore;
	}
	public void setTestScore3(double testScore)
	{
		this.testScore3 = testScore;
	}
	
	//Accessor Methods
	public double getTestScore1()
	{
		return this.testScore1;
	}
	public double getTestScore2()
	{
		return this.testScore2;
	}
	public double getTestScore3()
	{
		return this.testScore3;
	}
	
	//Instance Method
	public double calcAverage()
	{
		double average = (testScore1 + testScore2 + testScore3) / 3f;
		return average;
	}
}
