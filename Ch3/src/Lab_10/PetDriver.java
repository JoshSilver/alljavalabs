package Lab_10;
import java.util.Scanner;

public class PetDriver {

	public static void main(String[] args) 
	{
		//Instantiate Pet and Objects
		Pet pet = new Pet();
		Scanner keyboard = new Scanner(System.in);
		
		//Get Info From User
		System.out.println("What is the name of your pet?");
		String name = keyboard.nextLine();
		pet.setName(name);
		System.out.println("What kind of pet is it?");
		String type = keyboard.nextLine();
		pet.setType(type);
		System.out.println("How old is your pet?");
		String age = keyboard.nextLine();
		keyboard.close();
		pet.setAge(age);
		
		//Print Info Back To User
		System.out.printf("Name of pet: %s\n", name);
		System.out.printf("Type of pet: %s\n", type);
		System.out.printf("Age of pet: %s\n", age);
	}

}
