package Lab_10;

public class Pet 
{
	//Instance Fields
	private String name;
	private String type;
	private String age;
	
	//Mutator Methods
	public void setName(String name)
	{
		this.name = name;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public void setAge(String age)
	{
		this.age = age;
	}
	
	//Accessor Methods
	public String getName()
	{
		return this.name;
	}
	public String getType()
	{
		return this.type;
	}
	public String getAge()
	{
		return this.age;
	}
}
