package Lab_11;

public class PatientDriver {

	public static void main(String[] args) 
	{
		//Variables
		double total;
		//Instantiate Patient Object
		Patient patient = new Patient("Joshua", "S", "Silver", "(314) 680-5144", "2165 Flamingo Dr.", "Florissant", "Mo.", "63031", "Lindsey Walker", "(314) 837-3706");
		
		//Instantiate 3 Procedure Objects
		Procedure procedure1 = new Procedure("Physical Exam", "1/21/2018", "Dr. Irvine", 250);
		Procedure procedure2 = new Procedure("X-ray", "1/21/2018", "Dr. Jamison", 500);
		Procedure procedure3 = new Procedure("Blood Test", "1/21/2018", "Dr. Smith", 200);
		
		
		total = ((double)procedure1.getPrice() + procedure2.getPrice() + procedure3.getPrice());
		patient.display();
		System.out.println();
		procedure1.display();
		procedure2.display();
		procedure3.display();
		System.out.println("Total: $" + total);
	}
}
