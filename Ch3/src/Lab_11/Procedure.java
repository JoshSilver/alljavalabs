package Lab_11;

public class Procedure 
{
	//Instance Fields
	private String nameProcedure;
	private String dateProcedure;
	private String practioner;
	private int priceProcedure;
	
	//Constructors
	public Procedure()
	{
		
	}
	public Procedure(String name, String date, String practitioner, int price)
	{
		this.nameProcedure = name;
		this.dateProcedure = date;
		this.practioner = practitioner;
		this.priceProcedure = price;
	}
	
	//Mutator Methods
	public void setNameProcedure(String name)
	{
		this.nameProcedure = name;
	}
	public void setDateProcedure(String date)
	{
		this.dateProcedure = date;
	}
	public void setPractioner(String practioner)
	{
		this.practioner = practioner;
	}
	public void setPrice(int price)
	{
		this.priceProcedure = price;
	}
	
	//Accessor Methods
	public String getNameProcedure()
	{
		return this.nameProcedure;
	}
	public String getDateProcedure()
	{
		return this.dateProcedure;
	}
	public String getPractioner()
	{
		return this.practioner;
	}
	public int getPrice()
	{
		return this.priceProcedure;
	}
	
	//Instance Methods
	public void display()
	{
		System.out.printf("Procedure Name:\n%s\n", this.nameProcedure);
		System.out.printf("Date: %s\n", this.dateProcedure);
		System.out.printf("Practioner: %s\n", this.practioner);
		System.out.printf("Charge: %s\n\n", this.priceProcedure);
	}
	
}
