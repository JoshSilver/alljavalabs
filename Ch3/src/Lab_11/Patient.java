package Lab_11;

public class Patient 
{
	//Instance Fields
	private String firstName;
	private String middleName;
	private String lastName;
	private String phoneNumber;
	private String address;
	private String city;
	private String state;
	private String zip;
	private String emergencyContact;
	private String emergencyContactPhone;
	
	//Constructors
	public Patient()
	{
		
	}
	public Patient(String firstName, String middleName, String lastName, String phoneNumber, String address, String city, String state, String zip, String emergencyContact, String emergencyContactPhone)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.emergencyContact = emergencyContact;
		this.emergencyContactPhone = emergencyContactPhone;
	}
	
	//Mutator Methods
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public void setZip(String zip)
	{
		this.zip = zip;
	}
	
	//Accessor Methods
	public String getFirstName()
	{
		return this.firstName;
	}
	public String getMiddleName()
	{
		return this.middleName;
	}
	public String getLastName()
	{
		return this.lastName;
	}
	public String getPhoneNumber()
	{
		return this.phoneNumber;
	}
	public String getAddress()
	{
		return this.address;
	}
	public String getCity()
	{
		return this.city;
	}
	public String getState()
	{
		return this.state;
	}
	public String getZip()
	{
		return this.zip;
	}
	
	//Instance Methods
	public void display()
	{
		System.out.println("Patient Information:");
		System.out.printf("%s\n", this.firstName);
		System.out.printf("%s\n", this.middleName);
		System.out.printf("%s\n", this.lastName);
		System.out.printf("%s\n", this.phoneNumber);
		System.out.printf("%s %s %s %s\n\n", this.address, this.city, this.state, this.zip);
		System.out.println("Emergency Contact:");
		System.out.printf("%s\n", this.emergencyContact);
		System.out.printf("%s\n", this.emergencyContactPhone);
	}
}
