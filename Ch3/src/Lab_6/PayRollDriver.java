package Lab_6;
import java.util.Scanner;

public class PayRollDriver {

	public static void main(String[] args) 
	{
		//Variables
		double hourlyPayRate;
		int numHoursWorked;
		
		//Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);
		
		//Gather User Information
		System.out.println("What is your name?");
		String name = keyboard.nextLine();
		System.out.println("What is your Id Number?");
		String idNum = keyboard.nextLine();
		System.out.println("What is your hourly pay rate?");
		hourlyPayRate = keyboard.nextDouble();
		System.out.println("How many hours did you work?");
		numHoursWorked = keyboard.nextInt();
		
		//close Scanner
		keyboard.close();
		
		//Instantiate PayRoll Object
		PayRoll emp1 = new PayRoll(name, idNum);
		
		//Set hourlyPayRate and numHoursWorked
		emp1.setHourlyPayRate(hourlyPayRate);
		emp1.setNumHoursWorked(numHoursWorked);
		
		//Print Information
		System.out.printf("%s, if you have worked %d hours and your hourly pay rate is $%.2f, your gross pay will be $%.2f.", 
				emp1.getEmployeeName(), 
				emp1.getNumHoursWorked(), 
				emp1.getHourlyPayRate(), 
				emp1.calcGrossPay());
	}
}
