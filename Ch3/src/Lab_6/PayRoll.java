package Lab_6;

public class PayRoll 
{
	///////////////////
	//Instance Fields
	//////////////////
	private String employeeName;
	private String idNum;
	private double hourlyPayRate;
	private int numHoursWorked;
	
	/////////////////
	// Constructors
	////////////////
	public PayRoll(String employeeName, String idNum)
	{
		this.employeeName = employeeName;
		this.idNum = idNum;
	}
	public PayRoll()
	{
		
	}
	
	///////////////////
	//Mutator Methods 
	//////////////////
	public void setEmployeeName(String employeeName)
	{
		this.employeeName = employeeName;
	}
	public void setIdNum(String idNum)
	{
		this.idNum = idNum;
	}
	public void setHourlyPayRate(double hourlyPayRate)
	{
		this.hourlyPayRate = hourlyPayRate;
	}
	public void setNumHoursWorked(int numhoursWorked)
	{
		this.numHoursWorked = numhoursWorked;
	}
	
	////////////////////
	//Accessor Methods
	///////////////////
	public String getEmployeeName()
	{
		return this.employeeName;
	}
	public String getIdNum()
	{
		return this.idNum;
	}
	public double getHourlyPayRate()
	{
		return this.hourlyPayRate;
	}
	public int getNumHoursWorked()
	{
		return this.numHoursWorked;
	}
	
	////////////////////
	//Instance Methods
	///////////////////
	public double calcGrossPay()
	{
		double grossPay= this.numHoursWorked * this.hourlyPayRate;
		return grossPay;
	}
	
}
