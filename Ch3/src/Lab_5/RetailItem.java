package Lab_5;

public class RetailItem 
{
	//Instance Fields
	private String description;
	private int unitsOnHand;
	private double price;
	
	//Mutator Methods 
	public void setDescription(String description)
	{
		this.description = description;
	}
	public void setUnitsOnHand(int unitsOnHand)
	{
		this.unitsOnHand = unitsOnHand;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
	
	//Accessor Methods
	public String getDescription()
	{
		return description;
	}
	public int getUnitsOnHand()
	{
		return unitsOnHand;
	}
	public double getPrice()
	{
		return price;
	}
	
}
