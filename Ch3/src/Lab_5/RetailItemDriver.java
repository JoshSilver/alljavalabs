package Lab_5;

public class RetailItemDriver {

	public static void main(String[] args) 
	{
		//Instantiate objects
		RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();
		
		item1.setDescription("Jacket");
		item1.setUnitsOnHand(12);
		item1.setPrice(59.95);
		
		item2.setDescription("Designer Jeans");
		item2.setUnitsOnHand(40);
		item2.setPrice(34.95);
		
		item3.setDescription("Shirt");
		item3.setUnitsOnHand(20);
		item3.setPrice(24.95);
		
		System.out.printf("%s" +"\n" + "%d" + "\n" + "$%.2f\n\n", item1.getDescription(), item1.getUnitsOnHand(), item1.getPrice());
		System.out.printf("%s" +"\n" + "%d" + "\n" + "$%.2f\n\n", item2.getDescription(), item2.getUnitsOnHand(), item2.getPrice());
		System.out.printf("%s" +"\n" + "%d" + "\n" + "$%.2f\n\n", item3.getDescription(), item3.getUnitsOnHand(), item3.getPrice());
	}

}
