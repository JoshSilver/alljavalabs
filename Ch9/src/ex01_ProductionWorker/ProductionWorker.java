package ex01_ProductionWorker;

public class ProductionWorker extends Employee {
	/*
	 * fields
	 */
	private int shift;
	private double payRate;
	public final int DAY_SHIFT = 1;
	public final int NIGHT_SHIFT = 2;
	
	/*
	 * Constructors
	 */
	public ProductionWorker(String n, String num, String date, int shift, double rate)
	{
		super(n, num, date);
		this.shift = shift;
		this.payRate = rate;
	}
	public ProductionWorker()
	{
		
	}
	/*
	 * Mutators
	 */
	public void setShift(int s)
	{
		this.shift = s;
	}
	public void setPayRate(double p)
	{
		this.payRate = p;
	}
	/*
	 * Accessors
	 */
	public int getShift()
	{
		return this.shift;
	}
	public double getPayRate()
	{
		return this.payRate;
	}
	public String toString()
	{
		String str = "";
		str = String.format(super.toString() +
							"\nShift: %s" + 
							"\nPayrate: $%,.2f", 
							this.getShift(), this.getPayRate());
		return str;
	}
}

