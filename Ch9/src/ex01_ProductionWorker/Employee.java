package ex01_ProductionWorker;

public class Employee {
	/*
	 * Fields
	 */
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	/*
	 * Constructors
	 */
	public Employee(String n, String num, String date)
	{
		this.name = n;
		boolean isValid = isValidEmpNum(num);
		if (isValid)
		{
			this.employeeNumber = num;
		}
		else
		{
			this.employeeNumber = "-1";
		}
		this.hireDate = date;
	}
	public Employee()
	{
		
	}
	
	/*
	 * Mutators
	 */
	public void setName(String n)
	{
		this.name = n;
	}
	public void setEmployeeNumber(String e)
	{
		e.toUpperCase();
		boolean isValid = isValidEmpNum(e);
		if (isValid)
		{
			this.employeeNumber = e;
		}
		else
		{
			this.employeeNumber = "-1";
		}
	}
	public void setHireDate(String h)
	{
		this.hireDate = h;
	}
	
	/*
	 * Accessors
	 */
	public String getName()
	{
		return this.name;
	}
	public String getEmployerNumber()
	{
		return this.employeeNumber;
	}
	public String getHireDate()
	{
		return this.hireDate;
	}
	/*
	 * Methods
	 */
	private boolean isValidEmpNum(String e)
	{
		boolean isValid = true;
		int i = 0;
		
		if (e.length() != 6)
		{
			isValid = false;
		}
			
		
		while(isValid && i < 4)
		{
			if (!Character.isDigit(e.charAt(i)))
			{
				isValid = false;
			}
			i++;
		}
		
		if (isValid && e.charAt(4) != '-')
		{
			isValid = false;
		}
		
		if (isValid && e.charAt(5) < 'A' || isValid && e.charAt(5) > 'M')
		{
			isValid = false;
		}

		return isValid;
		
	}
	public String toString()
	{
		String str = " ";
		str = String.format("Employee name: %s" + 
							"\nEmployee number: %s" + 
							"\nHire date: %s", 
							this.name, this.employeeNumber, this.hireDate);
		return str;
	}
}
