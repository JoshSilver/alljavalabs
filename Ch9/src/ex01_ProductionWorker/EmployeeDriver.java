package ex01_ProductionWorker;

public class EmployeeDriver {

	public static void main(String[] args) {
		
		//with valid idNum
		ProductionWorker worker = new ProductionWorker("Josh", "1234-F", "10/13/2018", 1, 20.50);
		
		System.out.println(worker.toString());
		
		//with invalid idNum
		worker.setName("Josh Silver");
		worker.setPayRate(25.75);
		worker.setShift(2);
		worker.setEmployeeNumber("4321-Y");
		worker.setHireDate("12/25/2018");
		System.out.println();
		
		System.out.println(worker.toString());
	}
}
