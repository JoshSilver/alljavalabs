package ex03_TeamLeader;

public class TeamLeaderDriver {

	public static void main(String[] args) {
		
		TeamLeader teamLeader = new TeamLeader();
		
		teamLeader.setName("Silver, Josh");
		teamLeader.setPayRate(25.75);
		teamLeader.setShift(2);
		teamLeader.setEmployeeNumber("4281-A");
		teamLeader.setHireDate("12/25/2018");
		teamLeader.setMonthlyBonus(100);
		teamLeader.setRequiredTrainingHours(25);
		teamLeader.setTrainingHoursAttended(24.5);
		
		System.out.println(teamLeader.toString());
	}
}
