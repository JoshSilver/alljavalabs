package ex03_TeamLeader;
import ex01_ProductionWorker.ProductionWorker;

public class TeamLeader extends ProductionWorker {
	
	/*
	 * Fields
	 */
	private double monthlyBonus;
	private double requiredTrainingHours;
	public double trainingHoursAttended;
	
	/*
	 * Constructors
	 */
	public TeamLeader(String n, String num, String date, int shift, double rate, double monthlyBonus, double requiredTrainingHours, double trainingHoursAttended)
	{
		super(n, num, date, shift, rate);
		this.monthlyBonus = monthlyBonus;
		this.requiredTrainingHours = requiredTrainingHours;
		this.trainingHoursAttended = trainingHoursAttended;
	}
	public TeamLeader()
	{
		
	}
	
	/*
	 * Mutators
	 */
	public void setMonthlyBonus(double b)
	{
		monthlyBonus = b;
	}
	public void setRequiredTrainingHours(double r)
	{
		requiredTrainingHours = r;
	}
	public void setTrainingHoursAttended(double t)
	{
		trainingHoursAttended = t;
	}
	
	/*
	 * Accessors
	 */
	public double getMonthlyBonus()
	{
		return monthlyBonus;
	}
	public double getRequiredTrainingHours()
	{
		return requiredTrainingHours;
	}
	public double getTrainingHoursAttended()
	{
		return trainingHoursAttended;
	}
	/*
	 * toString method
	 */
	public String toString()
	{
		String str = "";
		str = String.format(super.toString() + 
							"\nMonthly Bonus: $%,.2f" + 
							"\nRequired Training Hours: %.2f" +
							"\nTraining Hours Attended: %.2f", 
							this.monthlyBonus, this.requiredTrainingHours, this.trainingHoursAttended);
		return str;
	}
}
