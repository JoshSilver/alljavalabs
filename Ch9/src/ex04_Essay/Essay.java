package ex04_Essay;

public class Essay extends GradedActivity {
	
	/*
	 * Fields
	 */
	private double grammar;
	private double spelling;
	private double correctLength;
	private double content;
	
	/*
	 * Mutators
	 */
	public Essay(double gr , double sp, double len, double cnt)
	{
		super();
		grammar = gr;
		spelling = sp;
		correctLength = len;
		content = cnt;
		setScore();
	}
	public Essay()
	{
		super();
	}
	
	public void setScore()
	{
		double score = this.grammar + this.spelling + this.correctLength + this.content;
		super.setScore(score);
	}
	public void setGrammar(double g)
	{
		grammar = g;
	}
	public void setSpelling(double s)
	{
		spelling = s;
	}
	public void setCorrectLength(double l)
	{
		correctLength = l;
	}
	public void setContent(double c)
	{
		content = c;
	}
	
	/*
	 * Accessors
	 */
	public double getGrammar()
	{
		return grammar;
	}
	public double getSpelling()
	{
		return spelling;
	}
	public double getCorrectLength()
	{
		return correctLength;
	}
	public double getContent()
	{
		return content;
	}
	
	/*
	 * toString
	 */
	public String toString()
	{
		String str = "";
		str = String.format("\nESSAY" + 
							"\nGrammar: %.2f" +
							"\nSpelling: %.2f" + 
							"\nCorrectLength: %.2f" + 
							"\nContent: %.2f" +
							super.toString(), 
							this.getGrammar(), this.getSpelling(), this.getCorrectLength(), this.getContent());
		return str;
	}
}


