package ex04_Essay;

import java.util.Scanner;

public class EssayDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
			System.out.println("What was your grade for grammar?");
			double g = keyboard.nextDouble();
			while(g > 30)
			{
				System.out.println("Grammar is scored out of 30 points.  What was your grade for grammar?");
				g = keyboard.nextDouble();
			}
			
			System.out.println("What was your grade for spelling?");
			double s = keyboard.nextDouble();
			while(s > 20)
			{
				System.out.println("Spelling is scored out of 20 points.  What was your grade for spelling?");
				s = keyboard.nextDouble();
			}
			
			System.out.println("What was your grade for length?");
			double len = keyboard.nextDouble();
			while(len > 30)
			{
				System.out.println("Length is scored out of 30 points.  What was your grade for length?");
				len = keyboard.nextDouble();
			}
			
			System.out.println("What was your grade for content?");
			double c = keyboard.nextDouble();
			while(c > 20)
			{
				System.out.println("Content is scored out of 20 points.  What was your grade for content?");
				c = keyboard.nextDouble();
			}
			keyboard.close();
			
			Essay essay = new Essay(g, s, len, c);
			essay.setScore();
			System.out.println(essay.toString());
	}
}
