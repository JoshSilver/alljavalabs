package HOT;

public class VehicleDriver {

	public static void main(String[] args) {
		
		//instantiate car object and use overridden accelerate method
		Vehicle car = new Car();
		car.accelerate();
		car.accelerate();
		car.accelerate();
		
		//instantiate truck object and use overridden accelerate method
		Vehicle truck = new Truck();
		truck.accelerate();
		truck.accelerate();
		truck.accelerate();
		//print info using the getSpeed() of the Vehicle class
		System.out.println("Car Speed: " + car.getSpeed());
		System.out.println("Truck Speed: " + truck.getSpeed());
	}
}
