package HOT;

public class Vehicle {
	
	protected double speed = 0;
	
	/*
	 * method
	 */
	public void accelerate() //overridden method
	{
		this.speed += 5;
	}
	/*
	 * accessor
	 */
	public double getSpeed()
	{
		return this.speed;
	}
}
