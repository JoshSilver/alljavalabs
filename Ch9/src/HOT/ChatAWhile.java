package HOT;

public class ChatAWhile {
	/*
	 * Fields
	 */
	private int[] areaCode = {262, 414, 608, 715, 815, 920};
	private double[] perMinRate = {.07, .10, .05, .16, .24, .14}; 
	
	/*
	 * methods
	 */
	public double getTotalCost(int area, int lengthOfCall)
	{
		double total = 0;
		int count = 0;
		//iterate over areaCode[]
		for(int i = 0; i < areaCode.length; i++)
		{
			if (area == areaCode[i])
			{
				//find the subscript of the area code
				count = i;
			}
		}
		//use count variable to multiply perMinRate by lengthOfCall
		total = perMinRate[count] * lengthOfCall;
		return total;
	}
	
}
