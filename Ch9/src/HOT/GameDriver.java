package HOT;

public class GameDriver {

	public static void main(String[] args) {
		
		//instantiate Game object
		Game game = new Game();
		game.setGameName("Cool Game Name");
		game.setNumPlayers(2);
		//print overridden toString()
		System.out.println(game.toString());
		
		GameWithTimeLimit gameWithTimeLimit = new GameWithTimeLimit();
		gameWithTimeLimit.setGameName("Game With TimeLimit");
		gameWithTimeLimit.setNumPlayers(4);
		gameWithTimeLimit.setTimeLimit(10);
		//print overridden toString()
		System.out.println(gameWithTimeLimit.toString());
	}

}
