package HOT;
import java.util.Scanner;

public class ChatAWhileDriver {

	public static void main(String[] args) {
		
		//instantiate ChatAwhile object and keyboard
		ChatAWhile chatAWhile = new ChatAWhile();
		Scanner keyboard = new Scanner(System.in);
		
		//get area code from user
		System.out.println("What is the area code?");
		int ac = keyboard.nextInt();
		
		//get length of call from user
		System.out.println("What was the length your call?");
		int length = keyboard.nextInt();
		
		//display total cost of the call using getTotalCost() of the ChatAWhile object
		System.out.printf("The call cost a total of $%.2f", chatAWhile.getTotalCost(ac, length));
		keyboard.close();
	}

}
