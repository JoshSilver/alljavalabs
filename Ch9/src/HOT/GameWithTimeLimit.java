package HOT;

public class GameWithTimeLimit extends Game {
	
	private int timeLimit = 0;
	
	/*
	 * getter
	 */
	public int getTimeLimit()
	{
		return this.timeLimit;
	}
	/*
	 * setter
	 */
	public void setTimeLimit(int num)
	{
		this.timeLimit = num;
	}
	
	/*
	 * toString
	 */
	@Override
	public String toString()
	{
		String str = "";
		str = String.format("\n" + 
							super.toString() + 
							"\nTimeLimit: %d", this.getTimeLimit());
		return str;
	}
}
