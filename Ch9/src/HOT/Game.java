package HOT;

public class Game {
	
	/*
	 * fields
	 */
	private String game;
	private int numPlayers;
	
	/*
	 * getter
	 */
	public String getGameName()
	{
		return this.game;
	}
	public int getNumPlayers()
	{
		return this.numPlayers;
	}
	/*
	 * setter
	 */
	public void setGameName(String gn)
	{
		this.game = gn;
	}
	public void setNumPlayers(int num)
	{
		this.numPlayers = num;
	}
	
	/*
	 * toString
	 */
	@Override
	public String toString()
	{
		String str = "";
		str = String.format("%s" +
							"\nNumber of players: %d",
							this.getGameName(), this.getNumPlayers());
		return str;
	}
}
