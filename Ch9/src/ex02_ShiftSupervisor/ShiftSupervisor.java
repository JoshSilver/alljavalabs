package ex02_ShiftSupervisor;
import ex01_ProductionWorker.Employee;

public class ShiftSupervisor extends Employee {
	
	/*
	 * Fields
	 */
	private double annualSalary;
	private double annualProductionBonus;
	
	/*
	 * Constructors
	 */
	public ShiftSupervisor(String n, String num, String date)
	{
		super(n, num, date);
	}
	public ShiftSupervisor()
	{
		
	}
	
	/*
	 * Mutators
	 */
	public void setAnnualSalary(double payRate)
	{
		this.annualSalary = payRate * 37.5 * 52;
	}
	public void setAnnualProductionBonus(boolean b)
	{
		if(b)
		{
			this.annualProductionBonus = this.annualSalary * .05;
		}
		else
			this.annualProductionBonus = 0;
	}
	
	/*
	 * Accessors
	 */
	public double getAnnualSalary()
	{
		return this.annualSalary;
	}
	public double getAnnualProductionBonus()
	{
		return this.annualProductionBonus;
	}
	
	/*
	 * toString
	 */
	public String toString()
	{
		String str = "";
		str = String.format(super.toString() + 
							"\nAnnual Salary: $%,.2f" + 
							"\nAnnual Production Bonus: $%,.2f", 
							this.annualSalary, this.annualProductionBonus);
		return str;
	}
}
