package ex02_ShiftSupervisor;

public class ShiftSupervisorDriver {

	public static void main(String[] args) {
		
		ShiftSupervisor shiftSupervisor = new ShiftSupervisor("Josh", "1234-D", "10/13/2018");
		
		shiftSupervisor.setAnnualSalary(18.0);
		shiftSupervisor.setAnnualProductionBonus(true);
		
		System.out.println(shiftSupervisor.toString());
		
		System.out.println();
		
		//Instance methods
		shiftSupervisor.setAnnualSalary(30.0);
		shiftSupervisor.setAnnualProductionBonus(false);
		
		//Superclass methods
		shiftSupervisor.setEmployeeNumber("4321-A");
		shiftSupervisor.setHireDate("October-25-2018");
		
		System.out.println(shiftSupervisor.toString());
	}
}
