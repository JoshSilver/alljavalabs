package ex05_CourseGrades;

import ex04_Essay.GradedActivity;

public class FinalExam extends GradedActivity {
	
	private final int NUM_QUESTIONS = 50;
	private int correctTotal;
	private int incorrectTotal;
	
	/*
	 * Constructor
	 */
	public FinalExam(int correct, int incorrect)
	{
		super();
		this.correctTotal = correct;
		this.incorrectTotal = incorrect;
		setScore();
	}
	public FinalExam()
	{
		super();
	}
	
	/*
	 * Mutators
	 */
	public void setCorrectTotal(int cor)
	{
		this.correctTotal = cor;
	}
	public void setIncorrectTotal(int inc)
	{
		this.incorrectTotal = inc;
	}
	public void setScore()
	{
		double score = 0;
		score = this.getCorrectTotal() * 2;
		super.setScore(score);
	}
	
	/*
	 * Accessors
	 */
	public int getCorrectTotal()
	{
		return this.correctTotal;
	}
	public int getIncorrectTotal()
	{
		return this.incorrectTotal;
	}
	public int getNumQuestions()
	{
		return this.NUM_QUESTIONS;
	}
	
	/*
	 * toString 
	 */
	@Override
	public String toString()
	{
		String str = "";
		str = String.format("\nFINAL EXAM" +
							"\nTotalQuestions: %d" +
							"\nCorrect Answers: %d" +
							"\nIncorrect Answers: %d" +
							super.toString(),
							this.getNumQuestions(), this.getCorrectTotal(), this.getIncorrectTotal());
		return str;
	}
	
}
