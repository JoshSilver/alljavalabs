package ex05_CourseGrades;
import ex04_Essay.Essay;
import ex04_Essay.GradedActivity;

public class CourseGrades implements Analyzable {
	 /*
	  * Fields
	  */

	private int NUM_GRADES = 4;
	private GradedActivity[] grades = new GradedActivity[NUM_GRADES];
	
	/*
	 * Constructor
	 */
	public CourseGrades()
	{
		
	}
	
	/*
	 * Mutators
	 */
	public void setLab(GradedActivity aLab)
	{
		grades[0] = new GradedActivity(aLab);
	}
	public void setPassFailExam(PassFailExam aPassFailExam)
	{
		grades[1] = new GradedActivity(aPassFailExam);
	}
	public void setEssay(Essay anEssay)
	{
		grades[2] = new GradedActivity(anEssay);
	}
	public void setFinalExam(FinalExam aFinalExam)
	{
		grades[3] = new GradedActivity(aFinalExam);
	}
	
	/*
	 * toString method
	 */
	public String toString()
	{
		String str = String.format("Lab Activity: %.2f" + 
									"\nPassFail Exam: %.2f" +
									"\nEssay: %.2f" + 
									"\nFinal Exam: %.2f", 
									grades[0].getScore(), grades[1].getScore(), 
									grades[2].getScore(), grades[3].getScore());
		return str;
	}
	
	/*
	 * Interface methods
	 */
	public double getAverage()
	{
		double total = 0;
		double avg = 0;
		for(int i = 0; i < grades.length; i++)
		{
			total += grades[i].getScore();
		}
		avg = total / grades.length;
		
		return avg;
	}
	
	public GradedActivity getHighest()
	{
		double high = grades[0].getScore();
		GradedActivity highest = new GradedActivity();
		for (int i = 0; i < grades.length; i++)
		{
			if(grades[i].getScore() > high)
			{
				high = grades[i].getScore();
				highest = grades[i];
			}
		}
		GradedActivity highScore = new GradedActivity(highest);
		return highScore;
	}
	
	public GradedActivity getLowest()
	{
		double low = grades[0].getScore();
		GradedActivity lowest = new GradedActivity();
		for (int i = 0; i < grades.length; i++)
		{
			if(grades[i].getScore() < low)
			{
				low = grades[i].getScore();
				lowest = grades[i];
			}
		}
		GradedActivity lowScore = new GradedActivity(lowest);
		return lowScore;
	}
	
}
