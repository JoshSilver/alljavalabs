package ex05_CourseGrades;

import ex04_Essay.GradedActivity;

public class LabActivity extends GradedActivity {
	
	/*
	 * Constructor
	 */
	public LabActivity(double s)
	{
		super();
		setScore(s);
	}
	public LabActivity()
	{
		super();
	}
	
	/*
	 * toString
	 */
	@Override
	public String toString()
	{
		String str = "";
		str = String.format("\nLAB ACTIVITY" + 
							super.toString());
		return str;
	}
}
