package ex05_CourseGrades;

import ex04_Essay.Essay;
import ex04_Essay.GradedActivity;

public class CourseGradesDriver {

	public static void main(String[] args) {
		
		//instantiate PassFailExam object and enter in number of correct answers
		PassFailExam passFailExam = new PassFailExam(8);
		
		//instantiate Essay object
		Essay essay = new Essay(29, 20, 30, 20);
		
		//instantiate FinalExam object
		FinalExam finalExam = new FinalExam(44, 6);
		
		//declare GradedActivity object and assign address of LabActivity
		//Example of polymorphism
		GradedActivity labActivity = new LabActivity(95);
		
		//instantiate CourseGrades
		//set courseGrades objects
		CourseGrades courseGrades = new CourseGrades();
		courseGrades.setEssay(essay);
		courseGrades.setFinalExam(finalExam);
		courseGrades.setLab(labActivity);
		courseGrades.setPassFailExam(passFailExam);
		
		System.out.println(courseGrades.toString());
		System.out.println(labActivity.toString());
		System.out.println(passFailExam.toString());
		System.out.println(essay.toString());
		System.out.println(finalExam.toString());
		
		System.out.println("\nAverage: " + courseGrades.getAverage());
		System.out.println("\nHighest score" + "\n" + courseGrades.getHighest().toString());
		System.out.println("\nLowest score" + "\n" + courseGrades.getLowest().toString());
	}
}
