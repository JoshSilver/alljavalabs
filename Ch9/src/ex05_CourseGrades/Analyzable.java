package ex05_CourseGrades;

import ex04_Essay.GradedActivity;

public interface Analyzable {
	
	double getAverage();
	GradedActivity getHighest();
	GradedActivity getLowest();
}
