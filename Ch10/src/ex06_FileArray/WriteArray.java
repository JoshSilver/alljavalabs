package ex06_FileArray;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class WriteArray {

	public static void writeArray(String fileName, int[] array) throws IOException
	{
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream(fileName));
		for(int i = 0; i < array.length; i++)
		{
			outputFile.writeInt(array[i]);
		}
		outputFile.close();
	}
	
	public static int[] readArray(String fileName, int[] readArray) throws IOException
	{
		int sub = 0;
		boolean endOfFile = false;
		DataInputStream inputFile = new DataInputStream(new FileInputStream(fileName));
		while(!endOfFile)
		{
			try
			{
				try
				{
					readArray[sub] = inputFile.readInt();
					sub++;
				}
				catch(EOFException e)
				{
					endOfFile = true;
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		inputFile.close();
		return readArray;
	}
}
