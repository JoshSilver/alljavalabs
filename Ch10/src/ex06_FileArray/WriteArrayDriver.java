package ex06_FileArray;

import java.io.IOException;

public class WriteArrayDriver {

	public static void main(String[] args) {
		
		int[] array = {8, 4, 6, 2, 1, 5};
		//write the above array to a binary file
		try {
			WriteArray.writeArray("file.dat", array);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int[] numArray = new int[array.length];
		//read the binary file created with the first method, then put into an array 
		try {
			numArray = WriteArray.readArray("file.dat", numArray);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//print the array acquired by writeArray() 
		for(int num : numArray)
			System.out.println(num);
	}
}