package ex07_FileEncryptionFilter;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileEncryption {
	
	public static void writeArray(String fileName, String[] array) throws IOException
	{
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream(fileName));
		for(int i = 0; i < array.length; i++)
		{
			outputFile.writeUTF(array[i]);
		}
		outputFile.close();
	}
	
	public static void writeEncryptedArray(String fileName, String[] array) throws IOException
	{
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream(fileName));
		//iterate over array
		for(int i = 0; i < array.length; i++)
		{
			//instantiate strb for each String in the array
			StringBuilder strb = new StringBuilder(array[i]);
			//iterate over strb
			for(int x = 0; x < strb.length(); x++)
			{
				//add 5 to character code
				char ch = (char) (strb.charAt(x) * 5);
				strb.setCharAt(x, ch);
			}
			strb.trimToSize();
			outputFile.writeUTF(strb.toString());
		}
		outputFile.close();
	}
	
	
	public static String[] readArray(String fileName, String[] readArray) throws IOException
	{
		int sub = 0;
		boolean endOfFile = false;
		DataInputStream inputFile = new DataInputStream(new FileInputStream(fileName));
		while(!endOfFile)
		{
			try
			{
				try
				{
					readArray[sub] = inputFile.readUTF();
					sub++;
				}
				catch(EOFException e)
				{
					endOfFile = true;
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		inputFile.close();
		return readArray;
	}
}
