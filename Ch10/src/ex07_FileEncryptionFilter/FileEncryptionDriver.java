package ex07_FileEncryptionFilter;

import java.io.IOException;

public class FileEncryptionDriver {

	public static void main(String[] args) {
		//filename where nameArray is being saved
		String fileName = "me.dat";
		
		//array of strings to be encrypted and saved to file
		String[] nameArray = {"Josh", "silver", "LindseY", "walker"};
		
		//write unencrypted nameArray to file
		try {
			FileEncryption.writeArray(fileName, nameArray);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//array that receives unencrypted array
		String[] array = new String[nameArray.length];
		
		//read the binary file created with FileEncryption.writeArray(), then put into an array 
		try {
			array = FileEncryption.readArray(fileName, array);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//encrypt the file and save to a new file
		String encryptedFileName = "encryptedFile.dat";
		try {
			FileEncryption.writeEncryptedArray(encryptedFileName, array);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//print info
		for(String str : array)
			System.out.println(str);
	}
}
