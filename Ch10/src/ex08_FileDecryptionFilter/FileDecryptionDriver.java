package ex08_FileDecryptionFilter;

import java.io.IOException;


public class FileDecryptionDriver {

	public static void main(String[] args) {
		
		//array for decrypted message
		String [] array = new String[4];
		try 
		{
			array = FileDecryption.readArray("encryptedFile.dat", array);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		//print decrypted message
		for(String str : array)
			System.out.println(str);
	}
}
