package ex08_FileDecryptionFilter;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;

public class FileDecryption {
	
	public static String[] readArray(String fileName, String[] readArray) throws IOException
	{
		int sub = 0;
		boolean endOfFile = false;
		DataInputStream inputFile = new DataInputStream(new FileInputStream(fileName));
		//read binary file
		while(!endOfFile) 
		{
			try 
			{
				try 
				{
					readArray[sub] = inputFile.readUTF();
					sub++;
				}
				catch (EOFException e) 
				{
				endOfFile = true;
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		
		//==============================================//
		//decrypt readArray and put into array
		String[] array = new String[readArray.length];
		
		for(int i = 0; i < readArray.length; i++)
		{
			//instantiate strb for each word in the array
			StringBuilder strb = new StringBuilder(readArray[i]);
			//iterate over strb
			for(int x = 0; x < strb.length(); x++)
			{
				//add 5 to character code
				char ch = (char) (strb.charAt(x) / 5);
				strb.setCharAt(x, ch);
			}
			array[i] = strb.toString();
			strb.trimToSize();
		}
		inputFile.close();
		return array;
	}
}
