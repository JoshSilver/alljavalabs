package ex01_TestScores;

public class TestScoresDriver {

	public static void main(String[] args) {
		
		//send testScores an array that throws an IllegalArgumentException when getAverage() is called
		double[] array = {95, -1, 101};
		try
		{
			TestScores testScores = new TestScores(array);
			testScores.getAverage();
		}
		catch(IllegalArgumentException e)
		{
			System.out.println(e.getMessage());
		}
	}
}
