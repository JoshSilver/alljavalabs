package ex10_BankAccountRandomFile;

import java.io.IOException;

public class CreateBankFile {

	public static void main(String[] args) throws NegativeStartingBalance, NegativeInterestRate, IOException {
		
		//create BankAccountfile file
		BankAccountFile inputAccount = new BankAccountFile("BankAccount.dat");
		
		//create BankAccountObjects
		BankAccount account = new BankAccount(10.0, .05, 0);
		BankAccount account2 = new BankAccount(20.0, .05, 0);
		
		//write account objects to a RandomAccessFile
		inputAccount.writeBankAccount(account);
		inputAccount.writeBankAccount(account2);
		
		//close the file
		inputAccount.close();
	}
}
