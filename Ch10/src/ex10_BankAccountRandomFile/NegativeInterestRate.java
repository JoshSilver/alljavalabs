package ex10_BankAccountRandomFile;

public class NegativeInterestRate extends Exception {
	private static final long serialVersionUID = 7784749504303101214L;

	public NegativeInterestRate()
	{
		super("Error: Negative interest rate");
	}
}
