package ex10_BankAccountRandomFile;
import java.io.*;

public class BankAccountFile {
	
	private final int RECORD_SIZE = 24;
	private RandomAccessFile bankFile;
	
	//Constructor
	public BankAccountFile(String fileName) throws FileNotFoundException
	{
		//open file for reading and writing
		bankFile = new RandomAccessFile(fileName, "rw");
	}
	
	/*
	 * The writeBankAccount method writes the
	 * BankAccount argument to the file 
	 * at the current file pointer position
	 */
	public void writeBankAccount(BankAccount account) throws IOException
	{
		bankFile.writeDouble(account.getBalance());
		bankFile.writeDouble(account.getInterestRate());
		bankFile.writeDouble(account.getInterest());
	}
	
	public BankAccount readBankAccount() throws IOException, NegativeStartingBalance, NegativeInterestRate
	{
		double bal = bankFile.readDouble();
		double intRate = bankFile.readDouble();
		double interest = bankFile.readDouble();	
		BankAccount bankAccount = new BankAccount(bal,intRate, interest);
		return bankAccount;
	}
	
	public long getByteNum(long num)
	{
		return RECORD_SIZE * num;
	}
	
	public void moveFilePointer(long num) throws IOException
	{
		bankFile.seek(getByteNum(num));
	}
	
	public void close() throws IOException
	{
		bankFile.close();
	}
}

