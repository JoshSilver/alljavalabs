package ex10_BankAccountRandomFile;

public class BankAccount {
	
	//Fields
	private double balance;
	private double interestRate;
	private double interest;
	
	//Constructor
	public BankAccount(double startBalance, double intRate, double interest) throws NegativeStartingBalance, NegativeInterestRate
	{
		if(startBalance < 0)
			throw new NegativeStartingBalance();
		if (intRate < 0)
			throw new NegativeInterestRate();
		this.balance = startBalance;
		this.interestRate = intRate;
		this.interest = interest;
	}
	
	//methods
	public void deposit(double amount) throws NegativeDepositAmount
	{
		if(amount < 0)
			throw new NegativeDepositAmount();
		this.balance += amount;
	}
	public void withdraw(double amount) throws NegativeWithdrawAmount
	{
		if(amount < 0)
			throw new NegativeWithdrawAmount();
		this.balance -= amount;
	}
	public void addInterest()
	{
		this.interest = this.balance * this.interestRate;
		this.balance += interest;
	}
	public double getBalance()
	{
		return this.balance;
	}
	public double getInterest()
	{
		return this.interest;
	}
	public double getInterestRate()
	{
		return this.interestRate;
	}
	public void setBalance(double num)
	{
		this.balance = num;
	}
}
