package ex10_BankAccountRandomFile;

public class NegativeStartingBalance extends Exception {
	private static final long serialVersionUID = 7760640709787774746L;

	public NegativeStartingBalance()
	{
		super("Error: Negative starting balance");
	}
}
