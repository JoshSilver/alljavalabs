package ex10_BankAccountRandomFile;

public class NegativeDepositAmount extends Exception {
	private static final long serialVersionUID = -1978324391053926185L;

	public NegativeDepositAmount()
	{
		super("Error: Negative deposit amount");
	}
}
