package ex10_BankAccountRandomFile;

public class NegativeWithdrawAmount extends Exception {
	private static final long serialVersionUID = -8599761294470016987L;

	public NegativeWithdrawAmount()
	{
		super("Error: Negative withdraw amount");
	}
}
