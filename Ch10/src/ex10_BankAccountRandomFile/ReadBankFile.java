
package ex10_BankAccountRandomFile;

import java.io.IOException;

public class ReadBankFile {

	public static void main(String[] args) throws IOException, NegativeInterestRate, NegativeStartingBalance {
		//name of file to be read
		String fileName = "BankAccount.dat";
		
		//create BankAccountFile object to read from the file
		BankAccountFile inputAccount = new BankAccountFile(fileName);
		
		//move file pointer to the second record
		inputAccount.moveFilePointer(1);
		
		//read the second record of the file
		BankAccount account2 = inputAccount.readBankAccount();
		
		//move file pointer to the first record
		inputAccount.moveFilePointer(0);
		
		//read the first record of the file
		BankAccount account = inputAccount.readBankAccount();
		
		
		//use the getBalance() of the first record
		System.out.printf("Balance of first account: $%.2f", account.getBalance());
		System.out.printf("\nInterestRate of first account: %.2f", account.getInterestRate());
		System.out.printf("\nInterest earned of first account: $%.2f", account.getInterest());
		
		//use the getBalance() of the second record
		System.out.printf("\n\nBalance of second account: $%.2f", account2.getBalance());
		System.out.printf("\nInterestRate of second account: %.2f", account2.getInterestRate());
		System.out.printf("\nInterest earned of second account: $%.2f", account2.getInterest());
		
		//close the file
		inputAccount.close();
	}
}
