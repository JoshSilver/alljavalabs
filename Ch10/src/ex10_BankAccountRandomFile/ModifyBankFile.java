package ex10_BankAccountRandomFile;

import java.io.IOException;

public class ModifyBankFile {

	public static void main(String[] args) throws IOException, NegativeStartingBalance, NegativeInterestRate {
		
		BankAccount account;
		
		//name of file to be read
		String fileName = "BankAccount.dat";
		
		//create BankAccountFile object to read from the file
		BankAccountFile inputAccount = new BankAccountFile(fileName);
		
		//move file pointer to the second record
		inputAccount.moveFilePointer(1);
		
		//create an object from the second record
		account = inputAccount.readBankAccount();
		
		//set a new balance by adding the interest
		account.addInterest();
		
		//move filePointer back to the beginning of the second record
		inputAccount.moveFilePointer(1);
		
		//save changes to the file
		inputAccount.writeBankAccount(account);
		
		//close the file
		inputAccount.close();
	}
}
