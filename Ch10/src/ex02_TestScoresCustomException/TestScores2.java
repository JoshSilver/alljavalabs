package ex02_TestScoresCustomException;



public class TestScores2 {
	//Fields
	private double[] testScores;
		
	//Constructor
	public TestScores2(double[] array)
	{
		testScores = array;
	}
	
	//method
	public double getAverage() throws InvalidTestScore
	{
		int count = 0;
		double avg = 0;
		for (double num : testScores)
		{
			if(num < 0 || num > 100)
				throw new InvalidTestScore();
			avg += num;
			count++;
		}
		avg = avg / count;
		return avg;
	}
	
	//Custom Exception Handler Class
	public class InvalidTestScore extends Exception 
	{
		private static final long serialVersionUID = 1L;

		public InvalidTestScore()
		{
			super("Invalid test score.");
		}
	}
}
