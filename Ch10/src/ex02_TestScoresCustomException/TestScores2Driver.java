package ex02_TestScoresCustomException;

import ex02_TestScoresCustomException.TestScores2.InvalidTestScore;

public class TestScores2Driver {

	public static void main(String[] args) throws InvalidTestScore 
	{
		double[] array = {95, -1, 101};
		TestScores2 testScores = new TestScores2(array);
		
		//send testScores2 an array that throws an InvalidTestScore exception 
		//when getAverage() is called
		try
		{
			System.out.println(testScores.getAverage());
		}
		catch(InvalidTestScore e)
		{
			System.out.println(e.getMessage());
		}
	}
}
