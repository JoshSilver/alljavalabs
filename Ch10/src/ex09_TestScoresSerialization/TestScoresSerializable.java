package ex09_TestScoresSerialization;

import java.io.Serializable;

public class TestScoresSerializable implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//Fields
	private double[] testScores;
	
	//Constructor
	public TestScoresSerializable(double[] array)
	{
		testScores = array;
	}
	
	//method
	public double getAverage()
	{
		int count = 0;
		double avg = 0;
		for (double num : testScores) 
		{
			if(num < 0 || num > 100)
				throw new IllegalArgumentException("Invalid test score");
			avg += num;
			count++;
		}
		avg = avg / count;
		return avg;
	}
}
