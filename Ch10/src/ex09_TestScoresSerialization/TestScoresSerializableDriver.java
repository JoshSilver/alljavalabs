package ex09_TestScoresSerialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class TestScoresSerializableDriver {

	public static void main(String[] args) throws IOException {
		
		//array of scores 
		double[] scores1 = {90, 95, 94};
		double[] scores2 = {100, 90, 84};
		double[] scores3 = {80, 85, 84};
		double[] scores4 = {86, 95, 100};
		double[] scores5 = {82, 75, 84};
		
		//serializable objects
		TestScoresSerializable testScores1 = new TestScoresSerializable(scores1);
		TestScoresSerializable testScores2 = new TestScoresSerializable(scores2);
		TestScoresSerializable testScores3 = new TestScoresSerializable(scores3);
		TestScoresSerializable testScores4 = new TestScoresSerializable(scores4);
		TestScoresSerializable testScores5 = new TestScoresSerializable(scores5);
		
		//array of serialized objects
		TestScoresSerializable[] testScores = {testScores1, testScores2, testScores3, testScores4, testScores5};
		
		//create Stream objects
		ObjectOutputStream objectOutputFile = new ObjectOutputStream(new FileOutputStream("TestScores.dat"));
		
		//write serialized objects to the file
		for (int i = 0; i < testScores.length; i++) {
			objectOutputFile.writeObject(testScores[i]);
		}
		objectOutputFile.close();
		
		System.out.println("Done");
	}
}
