package ex09_TestScoresSerialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeTestScores {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		
		//create Stream objects
		ObjectInputStream objectInputFile= new ObjectInputStream(new FileInputStream("TestScores.dat"));
		
		//create array to hold deserialized objects
		TestScoresSerializable[] testScores = new TestScoresSerializable[5];
		
		//read the objects from the file
		for (int i = 0; i < testScores.length; i++) 
		{
			testScores[i] = (TestScoresSerializable) objectInputFile.readObject();
		}
		objectInputFile.close();
		
		//display the getAverage() of the objects
		for(int i = 0; i < testScores.length; i++) 
		{
			System.out.printf("%.2f\n", testScores[i].getAverage());
		}
	}
}
