package ex05_PayRollExceptions;

public class InvalidEmpNumException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidEmpNumException()
	{
		super("EmpNum can't be empty");
	}

}
