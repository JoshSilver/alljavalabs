package ex05_PayRollExceptions;

public class PayrollExceptionDriver {

	public static void main(String[] args) {
		
		Payroll payroll = new Payroll();
		
		try
		{
			payroll.setEmployeeName("");
		    payroll.setIdNum("");
		    payroll.setHourlyPayRate(26);
			payroll.setNumHoursWorked(85);
		}
		catch(EmptyStringException e)
		{
			System.out.println(e.getMessage());
		}
		
		catch(InvalidEmpNumException e)
		{
			System.out.println(e.getMessage());
		}
		
		catch(InvalidNumHoursWorkedException e) 
		{
			System.out.println(e.getMessage());
		}
		
		catch(InvalidPayRateException e) 
		{
			System.out.println(e.getMessage());
		}
	}
}
