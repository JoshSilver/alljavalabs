package ex05_PayRollExceptions;

public class InvalidPayRateException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidPayRateException()
	{
		super("Pay rate can't be negative or greater than 25");
	}
}
