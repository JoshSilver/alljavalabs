package ex05_PayRollExceptions;

public class InvalidNumHoursWorkedException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidNumHoursWorkedException()
	{
		super("Hours worked can't be negative or greater than 84");
	}
}
