package ex05_PayRollExceptions;

public class Payroll {
		
	///////////////////
	//Instance Fields
	//////////////////
	private String employeeName;
	private String idNum;
	private double hourlyPayRate;
	private int numHoursWorked;
	
	/////////////////
	// Constructors
	////////////////
	public Payroll(String employeeName, String idNum)
	{
		this.employeeName = employeeName;
		this.idNum = idNum;
	}
	public Payroll() 
	{
	
	}
	
	///////////////////
	//Mutator Methods 
	//////////////////
	public void setEmployeeName(String employeeName) throws EmptyStringException
	{
		if(employeeName.equals(""))
			throw new EmptyStringException();
		this.employeeName = employeeName;
	}
	public void setIdNum(String idNum) throws InvalidEmpNumException
	{
		if(idNum.equals(""))
			throw new InvalidEmpNumException();
		this.idNum = idNum;
	}
	public void setHourlyPayRate(double hourlyPayRate) throws InvalidPayRateException
	{
		if(hourlyPayRate < 0 || hourlyPayRate > 25)
			throw new InvalidPayRateException();
		this.hourlyPayRate = hourlyPayRate;
	}
	public void setNumHoursWorked(int numHoursWorked) throws InvalidNumHoursWorkedException
	{
		if(numHoursWorked < 0 || numHoursWorked > 84)
			throw new InvalidNumHoursWorkedException();
		this.numHoursWorked = numHoursWorked;
	}
	
	////////////////////
	//Accessor Methods
	///////////////////
	public String getEmployeeName()
	{
		return this.employeeName;
	}
		public String getIdNum()
	{
			return this.idNum;
	}
		public double getHourlyPayRate()
	{
			return this.hourlyPayRate;
	}
	public int getNumHoursWorked()
	{
		return this.numHoursWorked;
	}
	
	////////////////////
	//Instance Methods
	///////////////////
	public double calcGrossPay()
	{
		double grossPay= this.numHoursWorked * this.hourlyPayRate;
		return grossPay;
	}
}
