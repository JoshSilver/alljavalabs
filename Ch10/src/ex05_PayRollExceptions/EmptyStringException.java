package ex05_PayRollExceptions;

public class EmptyStringException extends Exception {

	private static final long serialVersionUID = 1L;

	public EmptyStringException()
	{
		super("Name can't be empty");
	}
}
