package ex11_ExceptionProject;

public class InvalidEmployeeNumber extends Exception {
	private static final long serialVersionUID = 3372669299080452548L;
	
	public InvalidEmployeeNumber()
	{
		super("Error: Invalid Employee Number");
	}

}
