package ex11_ExceptionProject;

public class InvalidShift extends Exception {
	private static final long serialVersionUID = 8389913016031449825L;
	
	public InvalidShift()
	{
		super("Error: InvalidShift");
	}
}
