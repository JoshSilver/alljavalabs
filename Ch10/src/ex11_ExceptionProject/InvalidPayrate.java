package ex11_ExceptionProject;

public class InvalidPayrate extends Exception {
	private static final long serialVersionUID = 8602454572197106546L;
	
	public InvalidPayrate()
	{
		super("Error: Negative payrate");
	}

}
