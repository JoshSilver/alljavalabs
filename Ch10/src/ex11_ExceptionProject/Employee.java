package ex11_ExceptionProject;

public class Employee {
	/*
	 * Fields
	 */
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	/*
	 * Constructors
	 */
	public Employee(String n, String num, String date) throws InvalidEmployeeNumber
	{
		this.name = n;
		boolean isValid = isValidEmpNum(num);
		if (isValid)
		{
			this.employeeNumber = num;
		}
		else
		{
			this.employeeNumber = "-1";
		}
		this.hireDate = date;
	}
	public Employee()
	{
		
	}
	
	/*
	 * Mutators
	 */
	public void setName(String n)
	{
		this.name = n;
	}
	public void setEmployeeNumber(String e) throws InvalidEmployeeNumber
	{
		e.toUpperCase();
		boolean isValid;
		isValid = isValidEmpNum(e);
		
		if (isValid)
		{
			this.employeeNumber = e;
		}
		else
		{
			this.employeeNumber = "-1";
		}
	}
	public void setHireDate(String h)
	{
		this.hireDate = h;
	}
	
	/*
	 * Accessors
	 */
	public String getName()
	{
		return this.name;
	}
	public String getEmployerNumber()
	{
		return this.employeeNumber;
	}
	public String getHireDate()
	{
		return this.hireDate;
	}
	/*
	 * Methods
	 */
	private boolean isValidEmpNum(String e) throws InvalidEmployeeNumber
	{
		boolean isValid = true;
		
		if (e.length() != 6)
		{
			throw new InvalidEmployeeNumber();
		}
			
		int i = 0;
		while(isValid && i < 4)
		{
			if (!Character.isDigit(e.charAt(i)))
			{
				throw new InvalidEmployeeNumber();
			}
			i++;
		}
		
		if (isValid && e.charAt(4) != '-')
		{
			throw new InvalidEmployeeNumber();
		}
		
		if (isValid && e.charAt(5) < 'A' || isValid && e.charAt(5) > 'M')
		{
			throw new InvalidEmployeeNumber();
		}

		return isValid;
		
	}
	public String toString()
	{
		String str = " ";
		str = String.format("Employee name: %s" + 
							"\nEmployee number: %s" + 
							"\nHire date: %s", 
							this.name, this.employeeNumber, this.hireDate);
		return str;
	}
}
