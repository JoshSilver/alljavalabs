package ex03_RetailItemExceptions;

public class RetailItem 
{
	//Instance Fields
	private String description;
	private int unitsOnHand;
	private double price;
	
	//Mutator Methods 
	public void setDescription(String description)
	{
			this.description = description;
	}
	public void setUnitsOnHand(int unitsOnHand) throws InvalidUnitsArgument
	{
		if (unitsOnHand < 0)
			throw new InvalidUnitsArgument();
		this.unitsOnHand = unitsOnHand;
	}
	public void setPrice(double price) throws InvalidPriceArgument
	{
		if (price < 0)
			throw new InvalidPriceArgument();
		this.price = price;
	}
	
	//Accessor Methods
	public String getDescription()
	{
		return description;
	}
	public int getUnitsOnHand()
	{
		return unitsOnHand;
	}
	public double getPrice()
	{
		return price;
	}
}
