package ex03_RetailItemExceptions;

public class RetailItemExceptionsDriver {

	public static void main(String[] args) {
		
		RetailItem retailItem = new RetailItem();
	
		//negative value throws an exception
		try
		{
			retailItem.setUnitsOnHand(-1);
		}
		catch(InvalidUnitsArgument e)
		{
			System.out.println(e.getMessage());
		}
		
		//negative value throws an exception
		try
		{
			retailItem.setPrice(-11);
		}
		catch(InvalidPriceArgument e)
		{
			System.out.println(e.getMessage());
		}
	}
}
