package ex03_RetailItemExceptions;

public class InvalidUnitsArgument extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidUnitsArgument()
	{
		super("Units can't be negative");
	}
}
