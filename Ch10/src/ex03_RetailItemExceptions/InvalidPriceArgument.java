package ex03_RetailItemExceptions;

public class InvalidPriceArgument extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidPriceArgument()
	{
		super("Price can't be negative");
	}
}
