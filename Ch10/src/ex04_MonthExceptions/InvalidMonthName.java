package ex04_MonthExceptions;

public class InvalidMonthName extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidMonthName()
	{
		super("Month name is invalid");
	}
}
