package ex04_MonthExceptions;

public class Month {
	private int monthNumber;
	
	// constructors
	public Month() 
	{
		this.monthNumber = 1;
	}

	public Month(int monthNum) throws InvalidMonthNumber 
	{
		if (monthNum < 1 || monthNum > 12) 
		{
			throw new InvalidMonthNumber();
		}
		this.monthNumber = monthNum;
	}

	public Month(String monthName) throws InvalidMonthName 
	{
		switch (monthName) {
		case "January":
			monthNumber = 1;
			break;
		case "February":
			monthNumber = 2;
			break;
		case "March":
			monthNumber = 3;
			break;
		case "April":
			monthNumber = 4;
			break;
		case "May":
			monthNumber = 5;
			break;
		case "June":
			monthNumber = 6;
			break;
		case "July":
			monthNumber = 7;
			break;
		case "August":
			monthNumber = 8;
			break;
		case "September":
			monthNumber = 9;
			break;
		case "October":
			monthNumber = 10;
			break;
		case "November":
			monthNumber = 11;
			break;
		case "December":
			monthNumber = 12;
			break;
		default:
			throw new InvalidMonthName();
		}
		
	}

	// mutators
	public void setMonthNumber(int monthNum) throws InvalidMonthNumber {
		if (monthNum < 1 || monthNum > 12)
		{
			throw new InvalidMonthNumber();
		}
		this.monthNumber = monthNum;
	}

	// accessors
	public int getMonthNumber() 
	{
		return this.monthNumber;
	}

	public MonthName getMonthName() 
	{
		MonthName nameMonth = null;
		switch (this.monthNumber) {
		case 1:
			nameMonth = MonthName.JANUARY;
			break;
		case 2:
			nameMonth = MonthName.FEBRUARY;
			break;
		case 3:
			nameMonth = MonthName.MARCH;
			break;
		case 4:
			nameMonth = MonthName.APRIL;
			break;
		case 5:
			nameMonth = MonthName.MAY;
			break;
		case 6:
			nameMonth = MonthName.JUNE;
			break;
		case 7:
			nameMonth = MonthName.JULY;
			break;
		case 8:
			nameMonth = MonthName.AUGUST;
			break;
		case 9:
			nameMonth = MonthName.SEPTEMBER;
			break;
		case 10:
			nameMonth = MonthName.OCTOBER;
			break;
		case 11:
			nameMonth = MonthName.NOVEMBER;
			break;
		case 12:
			nameMonth = MonthName.DECEMBER;
			break;
		}
		return nameMonth;
	}

	// instance methods
	public String toString() 
	{
		String str = "The month is ";
		MonthName month = getMonthName();
		return str + month;
	}

	public boolean equals(Month obj) 
	{
		if (monthNumber == obj.monthNumber) {
			return true;
		} else
			return false;
	}

	public boolean isGreaterThan(Month obj) 
	{
		if (monthNumber > obj.monthNumber) {
			return true;
		} else
			return false;
	}

	public boolean isLessThan(Month obj) 
	{
		if (monthNumber < obj.monthNumber) {
			return true;
		} else
			return false;
	}
}
