package ex01_LatinTranslator;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class LatinTranslator extends Application {
	//field for label control
	private Label messageLabel;
	private Label englishLabel;

	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		//create label and buttons
		englishLabel = new Label();
		messageLabel = new Label("Click Buttons to translate from Latin to English");
		Button sinisterBTN = new Button("Sinister");
		Button dexterBTN = new Button("Dexter");
		Button mediumBTN = new Button("Medium");
		
		//register event handlers
		sinisterBTN.setOnAction(new SinisterClickHandler());
		dexterBTN.setOnAction(new DexterClickHandler());
		mediumBTN.setOnAction(new MediumClickHandler());
		
		//create a layout and center it
		VBox vBox = new VBox(10, messageLabel, sinisterBTN, dexterBTN, mediumBTN, englishLabel);
		vBox.setAlignment(Pos.CENTER);
		
		//create a scene with vBox as its root node 
		Scene scene = new Scene(vBox, 300, 200);
		
		//set the stage
		primaryStage.setScene(scene);
		
		//give stage a title
		primaryStage.setTitle("Latin Translator");
		
		//show window
		primaryStage.show();
	}
	
	//event handler class for sinisterBTN
	private class SinisterClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			englishLabel.setText("left");
		}
	}
	
	//event handler class for sinisterBTN	
	private class DexterClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			englishLabel.setText("center");
		}
	}
	
	//event handler class for sinisterBTN
	private class MediumClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			englishLabel.setText("right");
		}
	}
}
