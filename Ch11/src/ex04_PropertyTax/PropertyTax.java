package ex04_PropertyTax;

import javafx.application.Application;
import javafx.event.*;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import javafx.scene.layout.VBox;

public class PropertyTax extends Application {
	
	//fields
	private TextField propertyValue;
	private Button calcTotalBTN;
	private Label assessmentValue;
	private Label propertyTax;
	private Label instruction;

	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//instantiate fields
		propertyValue = new TextField();
		calcTotalBTN = new Button("Calculate");
		assessmentValue = new Label();
		propertyTax = new Label();
		instruction = new Label("Enter the value of property to see assessment value and property tax.");
		
		//create container for controls
		VBox vBox = new VBox(10, instruction,  propertyValue, calcTotalBTN, assessmentValue, propertyTax);
		
		//register event handlers
		calcTotalBTN.setOnAction(new calcTotalBTNClickHandler());
		
		//create a scene with hBox as its root node 
		Scene scene = new Scene(vBox, 500, 300);
		
		//set the stage
		primaryStage.setScene(scene);
		
		//give stage a title
		primaryStage.setTitle("PropertyTax");
		
		//show window
		primaryStage.show();
		
		
	}
	
	//EventHandler methods
	private class calcTotalBTNClickHandler implements EventHandler<ActionEvent>
	{

		@Override
		public void handle(ActionEvent arg0) {
		
			double actualValue = Double.parseDouble(propertyValue.getText());
			double assessedValue = actualValue * .6;
			double tax = (actualValue / 100) * .64;
			assessmentValue.setText(String.format("Assessment value: $%.2f", assessedValue));
			propertyTax.setText(String.format("Property tax: $%.2f", tax));
		}
		
	}

}
