package ex08_JoesAutomotive;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;

public class JoesAutomotive extends Application
{
	//Button field
	Button btn;
	//Checkbox Fields
	CheckBox oilChange;
	CheckBox lubeJob;
	CheckBox radiatorFlush;
	CheckBox transmissionFlush;
	CheckBox inspection;
	CheckBox mufflerReplacement;
	CheckBox tireRotation;
	
	//textbox field
	TextField hoursWorked;
	
	//Label fields
	Label total;
	Label totalLabel;
	
	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		hoursWorked = new TextField();
		btn = new Button("Calculate Total");
		total = new Label();
		totalLabel = new Label();
		oilChange = new CheckBox("Oil change: $35.00");
		lubeJob = new CheckBox("Lube job: $25.00");
		radiatorFlush = new CheckBox("Radiator flush: $50.00");
		transmissionFlush = new CheckBox("Transmission flush: $120.00");
		inspection = new CheckBox("Inspection: $35.00");
		mufflerReplacement = new CheckBox("Muffler replacement: $200.00");
		tireRotation = new CheckBox("Tire rotation: $20.00");
		
		//set text prompt of textField
		hoursWorked.setPromptText("Enter number of hours worked");
		//put total Label in VBox
		HBox totalLabels = new HBox(total, totalLabel);
		totalLabels.setPadding(new Insets(0, 0, 0, 10));
		total.setText("Total: ");
		//put btn into VBox
		VBox button = new VBox(btn);
		//set Padding on button
		button.setPadding(new Insets(0, 0, 0, 10));
		//put checkboxes into VBox
		VBox chkbx = new VBox(5, oilChange, lubeJob, radiatorFlush, transmissionFlush, inspection, mufflerReplacement, tireRotation, hoursWorked);
		//set checkbox padding
		chkbx.setPadding(new Insets(10));
		//create gridPane object
		GridPane gridPane = new GridPane();
		//add objects to gridPane
		gridPane.add(chkbx, 0, 0);
		gridPane.add(button, 0, 1);
		gridPane.add(totalLabels, 0, 2);
		gridPane.setVgap(5);

		//register event handler for button click
		btn.setOnAction(e ->
		{
			double totalAmount = 0;
			if(oilChange.isSelected())
			{
				totalAmount += 35;
			}
			if(lubeJob.isSelected())
			{
				totalAmount += 25;
			}
			if(radiatorFlush.isSelected())
			{
				totalAmount += 50;
			}
			if(transmissionFlush.isSelected())
			{
				totalAmount += 120;
			}
			if(inspection.isSelected())
			{
				totalAmount += 35;
			}
			if(mufflerReplacement.isSelected())
			{
				totalAmount += 200;
			}
			if(tireRotation.isSelected())
			{
				totalAmount += 20;
			}
			double partsAndLabor;
			try
			{
				partsAndLabor = Double.parseDouble(hoursWorked.getText()) * 60;
			}
			catch (NumberFormatException exc)
			{
				return;
			}
			
			totalAmount += partsAndLabor;
			totalLabel.setText(String.format("$%.2f", totalAmount));
		});
		
		//create a scene with hBox as its root node 
		Scene scene = new Scene(gridPane, 600, 400);
								
		//set the stage
		primaryStage.setScene(scene);
								
		//give stage a title
		primaryStage.setTitle("Joe's Automotive");
						
		//show window
		primaryStage.show();
	}
}