package ex02_NameFormatter;
import javafx.application.Application;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class NameFormatter extends Application {
	
	//fields
	private TextField fName = new TextField();
	private TextField mName = new TextField();
	private TextField lName = new TextField();
	private TextField title = new TextField();
	Label nameLabel = new Label("Click a format to see your name formatted here");
	
	//main method
	public static void main(String[] args) {
		
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		//set prompt text of TextFields
		fName.setPromptText("Enter first name");
		mName.setPromptText("Enter middle name");
		lName.setPromptText("Enter last name");
		title.setPromptText("Enter title");
		
		//buttons to format name
		Button format1 = new Button("Title First Middle Last");
		Button format2 = new Button("First Middle Last");
		Button format3 = new Button("First Last");
		Button format4 = new Button("Last, First Middle, Title");
		Button format5 = new Button("Last, Middle First");
		Button format6 = new Button("Last, First");
		
		//register event handlers
		format1.setOnAction(new Format1ClickHandler());
		format2.setOnAction(new Format2ClickHandler());
		format3.setOnAction(new Format3ClickHandler());
		format4.setOnAction(new Format4ClickHandler());
		format5.setOnAction(new Format5ClickHandler());
		format6.setOnAction(new Format6ClickHandler());
		
		//create gridPane and HBox, set Vgap
		GridPane gridPane = new GridPane();
		HBox formatBTN = new HBox(format1, format2, format3, format4, format5, format6);
		HBox textFields = new HBox(title, fName, mName, lName);
		gridPane.setVgap(10);
		gridPane.setPadding(new Insets(40));
		//add controls to gridPane
		gridPane.add(textFields, 0, 0);
		gridPane.add(formatBTN, 0, 1);
		gridPane.add(nameLabel, 0, 2);
	
		//create a scene with vBox as its root node 
		Scene scene = new Scene(gridPane, 700, 300);
		
		//set the stage
		primaryStage.setScene(scene);
				
		//give stage a title
		primaryStage.setTitle("Name Formatter");
		
		//show window
		primaryStage.show();
		
	}
	
	//EventHandler methods
	private class Format1ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			nameLabel.setText(title.getText() + "." + " " + fName.getText() + " " + mName.getText() + " " + lName.getText());
		}
	}
	
	private class Format2ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			nameLabel.setText(fName.getText() + " " + mName.getText() + " " + lName.getText());
		}
	}
	
	private class Format3ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			nameLabel.setText(fName.getText() + " " + lName.getText());
		}
	}
	
	private class Format4ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			nameLabel.setText(lName.getText() + "," + " " + fName.getText() + " " + mName.getText() + "," + " " + title.getText() + ".");
		}
	}
	
	private class Format5ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			nameLabel.setText(lName.getText() + "," + " " + mName.getText() + " " + fName.getText());
		}
	}
	
	private class Format6ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0) 
		{
			nameLabel.setText(lName.getText() + "," + " " + fName.getText());
		}
	}
}
