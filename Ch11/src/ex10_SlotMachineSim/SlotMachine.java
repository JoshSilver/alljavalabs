package ex10_SlotMachineSim;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.util.Random;

public class SlotMachine extends Application {
	
	Random random = new Random(); 
	int one;
	int two;
	int three;
	double totalAmountWonEver = 0;

	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//create an image object
		Image strawberry = new Image("file:strawberry.jpg");
		Image apple = new Image("file:apple.png");
		Image lemon = new Image("file:lemon.jpg");
		Image spinBtn = new Image("file:SpinButtonFirst.png");
		ImageView imageView = new ImageView(spinBtn);
		
		//create labels
		Label label = new Label("Amount Inserted: $");
		Label amountWonThisSpin = new Label("Amount won this spin: ");
		Label totalAmountWon = new Label("Total amount won: ");
		Label thisSpinLbl = new Label();
		Label totalAmountLbl = new Label();
		GridPane vBox = new GridPane();
		vBox.add(amountWonThisSpin, 0, 0);
		vBox.add(totalAmountWon, 0, 1);
		
		vBox.add(thisSpinLbl, 1, 0);
		vBox.add(totalAmountLbl, 1, 1);
		thisSpinLbl.setText("0.00");
		totalAmountLbl.setText("0.00");
		label.setFont(new Font("Arial", 20));
		amountWonThisSpin.setFont(new Font("Arial", 20));
		totalAmountWon.setFont(new Font("Arial", 20));
		thisSpinLbl.setFont(new Font("Arial", 20));
		totalAmountLbl.setFont(new Font("Arial", 20));
		vBox.setPadding(new Insets(30));
		
		//create TextField
		TextField amountInserted = new TextField();
		
		//create button object
		Button spin = new Button("", imageView);
		HBox btn = new HBox(spin);
		btn.setAlignment(Pos.CENTER);
		btn.setPadding(new Insets(30));
		spin.setMaxSize(50, 100);
				
		//create ImageView objects for display
		ImageView image1 = new ImageView();
		ImageView image2 = new ImageView();
		ImageView image3 = new ImageView();
		ImageView[] imageViewArray = {image1, image2, image3};
		image1.setImage(strawberry);
		image2.setImage(apple);
		image3.setImage(lemon);
		
		//create HBox for TextField amountInserted and label
		HBox hBox = new HBox(label, amountInserted);
		hBox.setPadding(new Insets(30));
		
		//create gridPane
		GridPane gridPane = new GridPane();
		gridPane.setPadding(new Insets(30));
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		gridPane.add(image1, 0, 0);
		gridPane.add(image2, 1, 0);
		gridPane.add(image3, 2, 0);
		
		//create borderPane
		BorderPane borderPane = new BorderPane();
		borderPane.setTop(gridPane);
		borderPane.setRight(vBox);
		borderPane.setBottom(btn);
		borderPane.setLeft(hBox);
		borderPane.setStyle("-fx-background-color: beige;");

		//create a scene with borderPane as its root node 
		Scene scene = new Scene(borderPane, 975, 700);
		
										
		//set the stage
		primaryStage.setScene(scene);
										
		//give stage a title
		primaryStage.setTitle("Slot Machine");
								
		//show window
		primaryStage.show();
		
		
		
		//register event handler for button click
		spin.setOnAction(e ->
		{
			//simulates slot machine
			int[] slotMachine = new int[3];
			
			//generate random number and put into array
			for(int i = 0; i < slotMachine.length; i++)
			{
				slotMachine[i] = random.nextInt(3);
			}
			
			//iterate the array and setImage()
			for(int i = 0; i < slotMachine.length; i++)
			{
				if(slotMachine[i] == 0)
				{
					imageViewArray[i].setImage(strawberry);

				}
				else if (slotMachine[i] == 1)
				{
					imageViewArray[i].setImage(apple);
				}
				else
				{
					imageViewArray[i].setImage(lemon);
				}
			}
			
			//find a match
			//match three
			try
			{
				if(slotMachine[0] == slotMachine[1] && slotMachine[1] == slotMachine[2])
				{
					double insertion = Double.parseDouble(amountInserted.getText());
					totalAmountWonEver += insertion * 3;
					totalAmountLbl.setText(String.format("$%.2f", totalAmountWonEver));
					thisSpinLbl.setText(String.format("$%.2f", insertion * 3));
				}
				//match two
				else if (slotMachine[0] == slotMachine[1] || slotMachine[0] == slotMachine[2] || slotMachine[1] == slotMachine[2])
				{
					double insertion = Double.parseDouble(amountInserted.getText());
					totalAmountWonEver += insertion * 2;
					totalAmountLbl.setText(String.format("$%.2f", totalAmountWonEver));
					thisSpinLbl.setText(String.format("$%.2f", insertion * 2));
				}
				//match none
				else
				{
					totalAmountLbl.setText(String.format("$%.2f", totalAmountWonEver));
					thisSpinLbl.setText(String.format("$%.2f", 0.00));
				}
			}
			catch(NumberFormatException ex)
			{
				return;
			}
		});
	}
}
