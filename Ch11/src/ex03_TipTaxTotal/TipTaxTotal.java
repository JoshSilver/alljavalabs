package ex03_TipTaxTotal;
import javafx.application.Application;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class TipTaxTotal extends Application {
	
	//fields
	private TextField costOfMeal = new TextField();
	private Label total = new Label("Meal cost:");
	private Label tip = new Label("18% tip:");
	private Label tax = new Label("7% tax:");
	private Label newTotal = new Label("Total:");
	
	private Label totalLabel = new Label();
	private Label tipLabel = new Label();
	private Label taxLabel = new Label();
	private Label newTotalLabel = new Label();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//set prompt text of the costOfMeal Label
		costOfMeal.setPromptText("Enter cost of meal");
		
		//buttons to format name
		Button calcTotalBTN = new Button("Calculate total");
		
		//create container for TextField and Labels
		GridPane gridPane = new GridPane();
		HBox hBox = new HBox(costOfMeal, calcTotalBTN);
		VBox vBox = new VBox(total, tip, tax, newTotal);
		VBox vBox2 = new VBox(totalLabel, tipLabel, taxLabel, newTotalLabel);
		vBox2.setAlignment(Pos.BOTTOM_RIGHT);
		gridPane.add(hBox, 0, 0);
		gridPane.add(vBox, 0, 1);
		gridPane.add(vBox2, 0, 1);
		gridPane.setPadding(new Insets(10));
		gridPane.setVgap(35);
		
		//register event handlers
		calcTotalBTN.setOnAction(new calcTotalBTNClickHandler());
		
		//create a scene with vBox as its root node 
		Scene scene = new Scene(gridPane, 300, 150);
		
		//set the stage
		primaryStage.setScene(scene);
		
		//give stage a title
		primaryStage.setTitle("Tip, Tax, Total");
		
		//show window
		primaryStage.show();
	}
	
	//EventHandler methods
	private class calcTotalBTNClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent arg0)
		{
			double userTotal = 0;
			try
			{
				userTotal = Double.parseDouble(costOfMeal.getText());
			}
			catch(NumberFormatException e)
			{
				costOfMeal.clear();
				totalLabel.setText("");
				tipLabel.setText("");
				taxLabel.setText("");
				newTotalLabel.setText("");
				costOfMeal.setPromptText("Error: Invalid entry");
				return;
			}
			
			double userTip = userTotal * .18;
			double userTax = userTotal * .07;
			double userNewTotal = userTotal + userTip + userTax;
			totalLabel.setText(String.format("$%.2f", userTotal));
			tipLabel.setText(String.format("$%.2f", userTip));
			taxLabel.setText(String.format("$%.2f", userTax));
			newTotalLabel.setText(String.format("$%.2f", userNewTotal));
		}
	}
}
