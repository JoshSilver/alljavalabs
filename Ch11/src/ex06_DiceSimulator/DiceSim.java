package ex06_DiceSimulator;

import java.util.Random;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;

public class DiceSim extends Application{

	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//create Button object
		Button play = new Button("Roll the Dice");
				
		//put the button into a HBox and set alignment
		HBox buttonBox = new HBox(play);
		buttonBox.setAlignment(Pos.CENTER);
				
		//create an image object
		Image one = new Image("file:Dice-1.png");
		Image two = new Image("file:Dice-2.png");
		Image three = new Image("file:Dice-3.png");
		Image four = new Image("file:Dice-4.png");
		Image five = new Image("file:Dice-5.png");
		Image six = new Image("file:Dice-6.png");
		
		//create an ImageView object
		ImageView die1 = new ImageView();
		ImageView die2 = new ImageView();
				
				
		//put the image in an HBox
		HBox hBox = new HBox(die1, die2);
		hBox.setAlignment(Pos.CENTER);
				
		//put the hBox in a BorderPane
		BorderPane pane = new BorderPane();
				
		//add the controls to the BorderPane
		pane.setCenter(hBox);
		pane.setTop(buttonBox);
				
		//wire EventHandlers
		play.setOnAction(e ->
		{
			Random random = new Random();
			int num = random.nextInt(6) + 1;
			if (num == 1)
			{
				die1.setImage(one);
			}
			if (num == 2)
			{
				die1.setImage(two);
			}
			if (num == 3)
			{
				die1.setImage(three);
			}
			if (num == 4)
			{
				die1.setImage(four);
			}
			if (num == 5)
			{
				die1.setImage(five);
			}
			if (num == 6)
			{
				die1.setImage(six);
			}
			
			num = random.nextInt(6) + 1;
			
			if(num == 1)
			{
				die2.setImage(one);
			}
			if(num == 2)
			{
				die2.setImage(two);
			}
			if(num == 3)
			{
				die2.setImage(three);
			}
			if(num == 4)
			{
				die2.setImage(four);
			}
			if(num == 5)
			{
				die2.setImage(five);
			}
			if(num == 6)
			{
				die2.setImage(six);
			}
		});
				
		//create a scene with hBox as its root node 
		Scene scene = new Scene(pane, 1250, 800);
				
		//set the stage
		primaryStage.setScene(scene);
				
		//give stage a title
		primaryStage.setTitle("Dice Game");
		
		//show window
		primaryStage.show();
	}
}


