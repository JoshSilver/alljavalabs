package ex05_HeadsOrTails;

import java.util.Random;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;

public class HeadsOrTails extends Application {

	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//create Button object
		Button play = new Button("Play Heads or Tails");
		
		//put the button into a HBox and set alignment
		HBox buttonBox = new HBox(play);
		buttonBox.setAlignment(Pos.CENTER);
		
		//create an image object
		Image heads = new Image("file:heads.jpg");
		Image tails = new Image("file:tails.jpg");
		
		//create an ImageView object
		ImageView imageView = new ImageView();
		
		
		//put the image in an HBox
		HBox hBox = new HBox(imageView);
		hBox.setAlignment(Pos.CENTER);
		
		//put the hBox in a BorderPane
		BorderPane pane = new BorderPane();
		
		//add the controls to the BorderPane
		pane.setCenter(hBox);
		pane.setTop(buttonBox);
		
		//wire EventHandlers
		play.setOnAction(e ->
		{
			Random random = new Random(); 
			int num = random.nextInt(2);
			if (num == 0)
			{
				imageView.setImage(heads);
			}
			if (num == 1)
			{
				imageView.setImage(tails);
			}
		});
		
		//create a scene with hBox as its root node 
		Scene scene = new Scene(pane, 600, 600);
		
		//set the stage
		primaryStage.setScene(scene);
		
		//give stage a title
		primaryStage.setTitle("Heads or Tails");
		
		//show window
		primaryStage.show();
	}
}
