package ex09_TicTacToeSim;

import java.util.Random;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class TicTacToe extends Application {
	
	Random random = new Random(); 
	int num;

	public static void main(String[] args) {

		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//create an image object
		Image ex = new Image("file:x.jpg");
		Image oh = new Image("file:O.png");
		
		//create ImageView objects for display
		ImageView image1 = new ImageView();
		ImageView image2 = new ImageView();
		ImageView image3 = new ImageView();
		ImageView image4 = new ImageView();
		ImageView image5 = new ImageView();
		ImageView image6 = new ImageView();
		ImageView image7 = new ImageView();
		ImageView image8 = new ImageView();
		ImageView image9 = new ImageView();
		ImageView[][] imageViewArray = new ImageView[][] 
		{
			{image1, image2, image3},
			{image4, image5, image6},
			{image7, image8, image9},
		};
		
		GridPane gridPane = new GridPane();
		gridPane.add(image1, 0, 0);
		gridPane.add(image2, 1, 0);
		gridPane.add(image3, 2, 0);
		gridPane.add(image4, 0, 1);
		gridPane.add(image5, 1, 1);
		gridPane.add(image6, 2, 1);
		gridPane.add(image7, 0, 2);
		gridPane.add(image8, 1, 2);
		gridPane.add(image9, 2, 2);
		gridPane.setAlignment(Pos.CENTER);
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		
		Label winner = new Label();
		
		Button newGame = new Button("New Game");
		VBox btn = new VBox(20, winner, newGame);
		btn.setAlignment(Pos.CENTER);
		
		BorderPane borderPane = new BorderPane();
		borderPane.setCenter(gridPane);
		borderPane.setBottom(btn);
		
		//create a scene with hBox as its root node 
		Scene scene = new Scene(borderPane, 650, 650);
						
		//set the stage
		primaryStage.setScene(scene);
								
		//give stage a title
		primaryStage.setTitle("Tic Tac Toe");
						
		//show window
		primaryStage.show();

		//wire EventHandlers
		newGame.setOnAction(e ->
		{
			final int ARRAY_SIZE = 3;
			int[][] gameBoard = new int[ARRAY_SIZE][ARRAY_SIZE];
			int numXwins = 0;
			int numYwins = 0;
			
			for(int i = 0;i < gameBoard.length; i++)
			{
				for(int j = 0; j < gameBoard[i].length; j++)
				{
					num = random.nextInt(2);
					if(num == 1)
					{
						numXwins += 1;
					}
					else
					{
						numYwins += 1;
					}
					
					if(numXwins >= 5)
					{
						num = 0;
					}
					
					if(numYwins >= 5)
					{
						num = 1;
					}
					gameBoard[i][j] = num;
				}
			}
			
			for(int i = 0;i < gameBoard.length; i++)
			{
				for(int j = 0; j < gameBoard[i].length; j++)
				{
					if(gameBoard[i][j] == 0)
					{
						imageViewArray[i][j].setImage(oh);
					}
					else
					{
						imageViewArray[i][j].setImage(ex);
					}
				}
			}
			
			boolean xIsWinner = false;
			boolean yIsWinner = false;
			for(int i = 0;i < gameBoard.length; i++)
			{
				int total = 0;
				for(int j = 0; j < gameBoard[i].length; j++)
				{
					total += gameBoard[i][j];
				}
				if(total == 3)
				{
					xIsWinner = true;
				}
				if(total == 0)
				{
					yIsWinner = true;
				}
			}
			
			for(int i = 0;i < gameBoard.length; i++)
			{
				int total = 0;
				for(int j = 0; j < gameBoard.length; j++)
				{
					total += gameBoard[j][i];
				}
				if(total == 3)
				{
					xIsWinner = true;
				}
				if(total == 0)
				{
					yIsWinner = true;
				}
			}
			if(gameBoard[0][0] + gameBoard[1][1] + gameBoard[2][2] == 3) 
			{
				xIsWinner = true;
			}
			if(gameBoard[2][0] + gameBoard[1][1] + gameBoard[0][2] == 3) 
			{
				xIsWinner = true;
			}
			if(gameBoard[0][0] + gameBoard[1][1] + gameBoard[2][2] == 0) 
			{
				yIsWinner = true;
			}
			if(gameBoard[2][0] + gameBoard[1][1] + gameBoard[0][2] == 0) 
			{
				yIsWinner = true;
			}
			
			winner.setText("");
			if (xIsWinner)
			{
				winner.setText("X wins");
			}
			if (yIsWinner)
			{
				winner.setText("O wins");
			}
			if (xIsWinner && yIsWinner)
			{
				winner.setText("X and O won");
			}
			if(!xIsWinner && !yIsWinner)
			{
				winner.setText("It's a tie");
			}
		});
	}
}
