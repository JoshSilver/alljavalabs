package ex07_TravelExpenses;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class TravelExpenses extends Application {
	
	//Button field
	private Button calcBTN;
	
	//TextField fields
	private TextField numDays;
	private TextField airFare;
	private TextField carRentalFee;
	private TextField milesDriven;
	private TextField parkingFee;
	private TextField taxiFee;
	private TextField registrationFee;
	private TextField lodgingFee;
	
	//user input fields
	private Label numDaysLabel;
	private Label airFareLabel;
	private Label carRentalFeeLabel;
	private Label milesDrivenLabel;
	private Label parkingFeeLabel;
	private Label taxiFeeLabel;
	private Label registrationFeeLabel;
	private Label lodgingFeeLabel;
	
	//displayed information fields
	private Label expensesIncurred;
	private Label totalAllowableExpense;
	private Label excessAmount;

	public static void main(String[] args) {
		
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		//instantiate fields
		calcBTN = new Button("Calculate Totals");
		
		numDays = new TextField();
		airFare = new TextField();
		carRentalFee = new TextField();
		milesDriven = new TextField();
		parkingFee = new TextField();
		taxiFee = new TextField();
		registrationFee = new TextField();
		lodgingFee = new TextField();
		
		numDaysLabel = new Label("Days of trip");
		airFareLabel = new Label("Amount of airfare");
		carRentalFeeLabel = new Label("Car rental fees");
		milesDrivenLabel = new Label("Miles driven (private vehicle)");
		parkingFeeLabel = new Label("Parking fees");
		taxiFeeLabel = new Label("Taxi fees");
		registrationFeeLabel = new Label("Registration fees");
		lodgingFeeLabel = new Label("Lodging fees (Per night)");
		
		expensesIncurred = new Label();
		totalAllowableExpense = new Label();
		excessAmount = new Label();
		
		//Put TextFields into VBox
		VBox textField = new VBox(10, numDays,  airFare, carRentalFee, milesDriven,
							parkingFee, taxiFee, registrationFee, lodgingFee);
		
		//put labels into VBox
		VBox label = new VBox(18, numDaysLabel, airFareLabel, carRentalFeeLabel, milesDrivenLabel,
							parkingFeeLabel, taxiFeeLabel, registrationFeeLabel, lodgingFeeLabel);
		
		VBox displayLabel = new VBox(10, expensesIncurred, totalAllowableExpense, excessAmount);
		
		VBox btn = new VBox(calcBTN);
		
		//create GridPane
		GridPane pane = new GridPane();
		
		//add to GridPane
		pane.add(label, 0, 0);
		pane.add(textField, 1, 0);
		pane.add(btn, 1, 1);
		pane.add(displayLabel, 2, 0);
		
		//alignment
		textField.setPadding(new Insets(20, 0, 0, 30));
		label.setAlignment(Pos.TOP_RIGHT);
		label.setPadding(new Insets(20, 0, 0, 30));
		btn.setPadding(new Insets(30, 0, 0, 30));
		btn.setAlignment(Pos.BOTTOM_LEFT);
		displayLabel.setPadding(new Insets(30));
		
		//register eventHandler
		calcBTN.setOnAction(e ->
		{
			double days = 0;
			double airFareAmount = 0;
			double carRentalFees = 0;
			double numMilesDriven = 0;
			double parkingFees = 0;
			double taxiFees = 0;
			double registrationFees = 0;
			double lodgingFees = 0;
			
			try
			{
				//info entered by user
				days = Double.parseDouble(numDays.getText());
				airFareAmount = Double.parseDouble(airFare.getText());
				carRentalFees = Double.parseDouble(carRentalFee.getText());
				numMilesDriven = Double.parseDouble(milesDriven.getText());
				parkingFees = Double.parseDouble(parkingFee.getText());
				taxiFees = Double.parseDouble(taxiFee.getText());
				registrationFees = Double.parseDouble(registrationFee.getText());
				lodgingFees = Double.parseDouble(lodgingFee.getText());
			}
			catch(NumberFormatException o)
			{
				return;
			}
			
			//calculated info
			double totalExpenseIncurred = airFareAmount + carRentalFees + parkingFees + taxiFees + registrationFees + (lodgingFees * days);
			double totalAllowableExpenses = (days * 47) + (days * 20) + (days * 40) + (days * 195) + (numMilesDriven * .42);
			
			//display how much was spent
			expensesIncurred.setText(String.format("Total Expenses: $%.2f", totalExpenseIncurred));
			
			//display total of reimbursed travel expenses
			totalAllowableExpense.setText(String.format("Allowable Expenses: $%.2f", totalAllowableExpenses));
			
			//display whether the user owes money or saved money
			if(totalExpenseIncurred > totalAllowableExpenses)
			{
				excessAmount.setText(String.format("You owe $%.2f", totalExpenseIncurred - totalAllowableExpenses));
			}
			else
			{
				excessAmount.setText(String.format("You saved $%.2f", totalAllowableExpenses - totalExpenseIncurred));
			}
		});
		
		//create a scene with hBox as its root node 
		Scene scene = new Scene(pane, 600, 400);
						
		//set the stage
		primaryStage.setScene(scene);
						
		//give stage a title
		primaryStage.setTitle("Travel Expenses");
				
		//show window
		primaryStage.show();
	}
}
