package ex04_LargerThanN;

public class LargerThanN {
	
	private double[] largerThanN;
	private int n;
	
	public LargerThanN(double[] array, int n)
	{
		largerThanN = array;
		this.n = n;
	}
	
	public void largerThanN()
	{
		double[] nums = new double[10];
		
		for (int i = 0; i < largerThanN.length; i++)
		{
			if (n < largerThanN[i])
			{
				nums[i] = largerThanN[i];
			}
		}
		
		for (int i = 0; i < nums.length; i++)
		{
			if(nums[i] > 0)
			{
				System.out.println(nums[i]);
			}
		}
	}
}
