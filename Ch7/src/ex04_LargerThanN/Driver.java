package ex04_LargerThanN;

import java.util.Scanner;

public class Driver {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] largerThanN = new double[10];
		
		System.out.println("Enter in 10 numbers that are greater than 0...");
		for(int i = 0; i < largerThanN.length; i++)
		{
			System.out.print((i + 1) + ": ");
			largerThanN[i] = input.nextDouble();
			while(largerThanN[i] <= 0)
			{
				System.out.print("Enter number " + (i + 1) + " again: ");
				largerThanN[i] = input.nextDouble();
			}
		}
		
		System.out.print("Now choose a number separate from the previous 5...");
		int n = input.nextInt();
		input.close();
		
		LargerThanN moreThanN = new LargerThanN(largerThanN, n);
		
		moreThanN.largerThanN();
	}
}
