package ex09_GradeBookModification;

public class GradeBookMod {

	private final int ROW = 5;
	private final int COL = 4;
	private String[] students = new String[5];
	private char[] letterGrade = new char[] { 'A', 'B', 'C', 'D', 'F' };
	private double[][] scores = new double[ROW][COL];
	
	
	//Constructor
	public GradeBookMod(String[] studentNames, double[][] scores)
	{
		this.students = studentNames;
		this.scores = scores;
	}
	
	//Instance Methods
	/*
	public String getName()
	{
		String str = "";
		
	}
	*/
	public double getAverageTestScoreMod(String name)
	{
		double average = 0;
		int INDEX = 0;
		int count = 0;
		for(String studentName : students)
		{
			if(studentName.equals(name))
			{
				INDEX = count;
			}
			count++;	
		}
		
		//get lowest score
		
		double lowest = scores[INDEX][0];
		for(int column = 0; column < scores[INDEX].length; column++ )
		{
			if(scores[INDEX][column] < lowest)
			{
				lowest = scores[INDEX][column];
			}
		
		//get average and drop lowest score
		double total = 0;
		for(int col = 0; col < scores[INDEX].length; col++ )
		{
			total += scores[INDEX][col];
		}
		double newTotal = total - lowest;
		average = newTotal / (scores[INDEX].length - 1);
		
		}
		return average;
	}
	
	public char getLetterGrade(String name)
	{
		char letter = 'z';
		double average = getAverageTestScoreMod(name);
		
		if (average < 100 && average > 90)
			letter = letterGrade[0];
		
		else if(average < 89 && average > 80)
			letter = letterGrade[1];
		
		else if (average < 79 && average > 70)
			letter = letterGrade[2];
		
		else if(average < 69 && average > 60)
			letter = letterGrade[3];
		
		else if (average < 59 && average > 0)
			letter = letterGrade[4];
		
		return letter;
	}

	
	
	
}
