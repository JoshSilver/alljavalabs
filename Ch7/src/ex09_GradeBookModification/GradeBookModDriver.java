package ex09_GradeBookModification;

import java.util.Scanner;

public class GradeBookModDriver {

	public static void main(String[] args) {

		final int ROW = 5;
		final int COL = 4;
		String[] studentNames = new String[5];
		double[][] scores = new double[ROW][COL];
		
		Scanner input = new Scanner(System.in);
		
		for(int row = 0; row < scores.length; row++)
		{
			System.out.println("What is the name of student #: " + (row+1));
			studentNames[row] = input.nextLine();
			for(int col = 0; col < COL; col++)
			{
				System.out.printf("%s Score %d: ", studentNames[row],(col + 1));
				scores[row][col] = input.nextDouble();
				
				while(scores[row][col] < 0 || scores[row][col] > 100)
				{
					System.out.println("Score must greater than 0 and less than or equal to 100. Re-enter the score.");
					scores[row][col] = input.nextDouble();
				}
			}
			input.nextLine();
		}
		input.close();
		
		GradeBookMod gradeBook = new GradeBookMod(studentNames, scores);
		for (int i = 0; i < studentNames.length; i++)
		{
			System.out.println("Average for " + studentNames[i] + ": " + gradeBook.getAverageTestScoreMod(studentNames[i]));
			System.out.println("LetterGrade for " + studentNames[i] + ": " + gradeBook.getLetterGrade(studentNames[i]));
		}
		
		
	}
}
