package ex01_RainfallClass;

public class Rainfall {
	
	private double[] rainfall;
	
	public Rainfall(double[] monthsArray)
	{
		rainfall = monthsArray;
	}
	
	//methods
	public double getTotalRainfall() 
	{
		double total = 0;
		for(double inches : rainfall)
		{
			total += inches;
		}
		return total;
	}
	
	//getAverage
	public double getAverageMonthlyRainfall() 
	{
		double average = 0;
		average = getTotalRainfall() / rainfall.length;
		return average;
	}
	
	//getMostRainfall
	public String getMostRainfall()
	{
		int month = 0;
		double highest = rainfall[0];			
		for(int i = 0; i < rainfall.length; i++)
		{
			if(rainfall[i] > highest)
			{
				highest = rainfall[i];
				month = i;
			}
		}
		String monthString = " ";
		switch(month)
		{
		case 0: monthString = "January - Month 1";
				break;
		case 1: monthString = "February - Month 2";
				break;
		case 2: monthString = "March - Month 3";
				break;
		case 3: monthString = "April - Month 4";
				break;
		case 4: monthString = "May - Month 5";
				break;
		case 5: monthString = "June - Month 6";
				break;
		case 6: monthString = "July - Month 7";
				break;
		case 7: monthString = "August - Month 8";
				break;
		case 8: monthString = "September - Month 9";
				break;
		case 9: monthString = "October - Month 10";
				break;
		case 10: monthString = "November - Month 11";
				break;
		case 11: monthString = "December - Month 12";
				break;
		}
		return monthString;
	}
	
	//get LeastRainfall
	public String getLeastRainfall()
	{
		int month = 0;
		double lowest = rainfall[0];
		for(int i = 1; i < rainfall.length; i++)
		{
			if(rainfall[i] < lowest)
			{
				lowest = rainfall[i];
				month = i;
			}
		}
		String monthString = " ";
		switch(month)
		{
		case 0: monthString = "January - Month 1";
				break;
		case 1: monthString = "February - Month 2";
				break;
		case 2: monthString = "March - Month 3";
				break;
		case 3: monthString = "April - Month 4";
				break;
		case 4: monthString = "May - Month 5";
				break;
		case 5: monthString = "June - Month 6";
				break;
		case 6: monthString = "July - Month 7";
				break;
		case 7: monthString = "August - Month 8";
				break;
		case 8: monthString = "September - Month 9";
				break;
		case 9: monthString = "October - Month 10";
				break;
		case 10: monthString = "November - Month 11";
				break;
		case 11: monthString = "December - Month 12";
				break;
		}
		return monthString;
	}
}
