package ex01_RainfallClass;
import java.util.Scanner;

public class RainfallDriver {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		
		//ask for how many months of input
		System.out.println("For how many months will you enter total rainfall?");
		final int input = userInput.nextInt();
		double[] monthsArray= new double[input];
		
		//get input and put into array
		for(int i = 0;i < input;++i)
		{
			System.out.print("Month " + (i+1) + ": ");
			monthsArray[i] = userInput.nextDouble();
		}
		userInput.close();
		
		//pass array to RainFall and name the object rainfall
		Rainfall rainfall = new Rainfall(monthsArray);
		
		//output
		System.out.println("\nTotal Rainfall: " + rainfall.getTotalRainfall());
		System.out.println("Average Monthly Rainfall: " + rainfall.getAverageMonthlyRainfall());
		System.out.println("Month With Lowest Rainfall: " + rainfall.getLeastRainfall());
		System.out.println("Month With Highest Rainfall: " + rainfall.getMostRainfall());
	}
}
