package ex06_DriversLicenseExam;

public class DriverExamDriver {

	public static void main(String[] args) {
		
		DriverExam driverExam = new DriverExam();
		
		driverExam.requestInfo();
		System.out.println("Correct: " + driverExam.totalCorrect());
		System.out.println("Incorrect: " + driverExam.totalIncorrect());
		System.out.println("QuestionsMissed: " + driverExam.questionsMissed());
		
		
	}

}
