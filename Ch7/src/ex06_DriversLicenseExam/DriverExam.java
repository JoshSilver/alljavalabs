package ex06_DriversLicenseExam;

import java.util.ArrayList;
import java.util.Scanner;

public class DriverExam {
	private final int SIZE = 20;
	private String[] answers = {"B", "D", "A", "A",
								"C", "A", "B", "A",
								"C", "D", "B", "C",
								"D", "A", "D", "C",
								"C", "B", "D", "A"};
	
	private String[] studentAnswers = new String[SIZE];
	
	//constructor
	public DriverExam()
	{
		
	}
	
	//instance methods
	public boolean passed()
	{
		boolean passed = false;
		if(this.totalCorrect() >= 15)
			passed = true;
		return passed;
	}
	public int totalCorrect()
	{
		int total = 0;
		for (int i = 0; i < studentAnswers.length; i++)
		{
			if (answers[i].equals(studentAnswers[i]))
			{
				total += 1;
			}
		}
		return total;
	}
	public int totalIncorrect()
	{
		int total = 0;
		for (int i = 0; i < studentAnswers.length; i++)
		{
			if (!answers[i].equals(studentAnswers[i]))
			{
				total += 1;
			}
		}
		return total;
	}
	public ArrayList<Integer> questionsMissed()
	{
		ArrayList<Integer> missed = new ArrayList<Integer>();
		for (int i = 0; i < studentAnswers.length; i++)
		{
			if (!answers[i].equals(studentAnswers[i]))
			{
				missed.add(i + 1);
			}
		}
		return missed;
	}
	
	public void requestInfo()
	{
		Scanner input = new Scanner(System.in);
		//DriverExam driverExam = new DriverExam();
		
		System.out.println("Please enter the answers of the student...");
		for(int i = 0; i < studentAnswers.length; i++)
		{
			System.out.print("Answer " + (i + 1) + ": ");
			studentAnswers[i] = input.nextLine().toUpperCase();
			while(!studentAnswers[i].equalsIgnoreCase("a") && !studentAnswers[i].equalsIgnoreCase("b") && !studentAnswers[i].equalsIgnoreCase("c") && !studentAnswers[i].equalsIgnoreCase("d"))
			{
				System.out.print("Answer " + (i + 1) + "  should be 'a', 'b', 'c', or 'd'. Try again: ");
				studentAnswers[i] = input.nextLine().toUpperCase();
			}
		}
	}
}
