package ex05_ChargeAccountModification;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ValidAccountModification {
	private final int SIZE = 18;
	private int[] validAccounts = new int[SIZE];

	public boolean isValid(int num)
	{
		boolean isValid = false;
		for (int number : validAccounts)
		{
			if (number == num)
			{
				isValid = true;
			}
		}
		return isValid;
	}
	
	public void getValidAccounts() throws IOException
	{
		ValidAccountModification validAccount = new ValidAccountModification();
		int i = 0;
		
		//open the file
		File file = new File("ValidAccounts.txt");
		Scanner inputFile = new Scanner(file);
		
		//read the file contents into the array
		while (inputFile.hasNext() && i < validAccounts.length)
		{
			validAccounts[i] = inputFile.nextInt();
			i++;
		}
		//close the file
		inputFile.close();
	}
}
