package ex05_ChargeAccountModification;

import java.io.IOException;
import java.util.Scanner;

public class Driver {

	public static void main(String[] args) throws IOException {
		
		ValidAccountModification validAccount = new ValidAccountModification();
		Scanner input = new Scanner(System.in);
		
		validAccount.getValidAccounts();
		
		//ask user for an account # until a valid one is entered
		System.out.println("Enter an account number to see if it's valid: ");
		int accountNum = input.nextInt();
		boolean isValid = validAccount.isValid(accountNum);
		
		if(isValid)
		{
			System.out.println("That account is valid.");
		}
		else
		{
			System.out.println("That account is NOT valid.");
		}
		input.close();
	}
}
