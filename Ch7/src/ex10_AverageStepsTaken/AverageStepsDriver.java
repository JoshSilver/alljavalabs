package ex10_AverageStepsTaken;

import java.io.IOException;

public class AverageStepsDriver {

	public static void main(String[] args) throws IOException {
		
		AverageStepsTaken averageStepsTaken = new AverageStepsTaken();
		averageStepsTaken.displayAverage();
	}
}