package ex10_AverageStepsTaken;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class AverageStepsTaken {
	private int[][] months = new int[12][];
	
	//constructor
	public AverageStepsTaken() throws IOException
	{
		months[0] = new int[31];
		months[1] = new int[28];
		months[2] = new int[31];
		months[3] = new int[30];
		months[4] = new int[31];
		months[5] = new int[30];
		months[6] = new int[31];
		months[7] = new int[31];
		months[8] = new int[30];
		months[9] = new int[31];
		months[10] = new int[30];
		months[11] = new int[31];
		
		File file = new File("steps.txt");
		Scanner inputFile = new Scanner(file);
		
		for(int row = 0; row < months.length; row++)
		{
			for(int col = 0; col < months[row].length; col++)
			{
				if(inputFile.hasNext())
				{
					months[row][col] = inputFile.nextInt();
				}
			}
		}
		inputFile.close();
	}
	
	//display method
	public void displayAverage()
	{
		int total = 0;
		int average = 0;
		for(int row = 0; row < months.length; row++)
		{
			total = 0;
			for(int col = 0; col < months[row].length; col++)
			{
				total += months[row][col];
				average = total / months[row].length;
			}
			System.out.println("\nTotal for month " + (row + 1) + ": " + total);
			System.out.println("Average for month " + (row + 1) + ": " + average);
		}
	}
}
