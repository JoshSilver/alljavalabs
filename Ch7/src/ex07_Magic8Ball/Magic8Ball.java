package ex07_Magic8Ball;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Magic8Ball {
	//field
	private String[] response = new String[12];
	
	//constructor
	public Magic8Ball() throws IOException
	{
		File file = new File("Magic8Ball.txt");
		Scanner inputFile = new Scanner(file);
		
		int i = 0;
		while (inputFile.hasNext() && i < response.length)
		{
			response[i] = inputFile.nextLine();
			i++;
		}
		inputFile.close();
	}
	
	//method
	public String returnAnswer()
	{
		String str = "";
		Random random = new Random(); 
		int num = random.nextInt(12);
		for(int i = 0; i < response.length; i++)
		{
			str = response[num];
		}
		return str + "\n";
	}
}
