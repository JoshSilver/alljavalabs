package ex07_Magic8Ball;

import java.io.IOException;

import java.util.Scanner;
import java.lang.System;

public class Driver {

	public static void main(String[] args) throws IOException {

		Magic8Ball magic8Ball = new Magic8Ball();
		
		Scanner input = new Scanner(System.in);
		
		for(;;)
		{
			System.out.println("Ask me a question and ill answer it.  Type \"no\" to exit");
			String question = input.nextLine();
			if(question.equals("no"))
			{
				System.exit(0);
				input.close();
			}
			System.out.println(magic8Ball.returnAnswer());
		}
	}
}
