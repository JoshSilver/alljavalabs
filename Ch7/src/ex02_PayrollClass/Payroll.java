package ex02_PayrollClass;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Payroll {

	private int[] idNumber = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489};
	private int[] hours = new int[7];
	private double[] payRate = new double [7];
	private double[] wages = new double [7];
	
	//constructors
	public Payroll()
	{
		
	}
	
	public void setGrossPay() 
	{
		for(int i = 0; i < wages.length; i++)
		{
			wages[i] = payRate[i] *= hours[i];
		}
	}
	
	public double getGrossPay(int idNum)
	{
		double grossPay = 0;
		for(int i = 0; i < idNumber.length; i++)
		{
			if(idNum == idNumber[i])
			{
				grossPay = wages[i];
			}
		}
		return grossPay;
	}
	
	public int getIdNumber(int idNum)
	{
		int num = 0;
		for (int id : idNumber)
		{
			if (id == idNum)
				num = id;
		}
		return num;
	}
	
	public int[] getAllIdNumbers()
	{
		int[] idNums = new int[7];
		for(int i = 0; i < idNumber.length; i++)
		{
			idNums[i] = idNumber[i];
		}
		return idNums;
	}
	public void requestInfo()
	{
		Scanner input = new Scanner(System.in);
		for (int i = 0; i < idNumber.length; i++)
		{
			System.out.println("Enter the hours and payrate of employee " + idNumber[i]);
			System.out.print("Hours: ");
			hours[i] = input.nextInt();
			while(hours[i] < 0)
			{
				System.out.println("Enter an hour amount that isn't negative for employee " + idNumber[i]);
				System.out.print("Hours: ");
				hours[i] = input.nextInt();
			}
			System.out.print("Payrate: ");
			payRate[i] = input.nextDouble();
			while(payRate[i] < 6.00) 
			{
				System.out.println("Enter a payrate that is more than $6.00 for employee " + idNumber[i]);
				System.out.print("Payrate: ");
				payRate[i] = input.nextInt();
			}
		}
		input.close();
	}
	public void display(Payroll payRoll)
	{
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		for(int i = 0; i < idNumber.length; i++)
		{
			System.out.println("Employee idNumber: " + idNumber[i]);
			System.out.println("Gross pay: $" + decimalFormat.format(payRoll.getGrossPay(idNumber[i])));
		}
	}
}
