package ex02_PayrollClass;

public class PayrollDriver {

	public static void main(String[] args) {
		
		Payroll payRoll = new Payroll();
		payRoll.requestInfo();
		payRoll.setGrossPay();
		payRoll.display(payRoll);
	}
}
