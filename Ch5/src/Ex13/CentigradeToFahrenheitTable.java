package Ex13;

public class CentigradeToFahrenheitTable {

	public static void main(String[] args) {

		// display a table of centigrade temperatures and there equivalents
		System.out.println("Centigrade\tFahrenheit");
		for (double i = 0; i <= 20; ++i) {
			double fahrenheit = ((9f / 5f) * i) + 32;
			System.out.printf("%.0f\t\t%.2f\n", i, fahrenheit);
		}
	}
}
