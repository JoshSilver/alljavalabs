package Ex09;

import java.util.Scanner;

public class PayRollDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int employeeNumber = -1;
		double grossPay = 0;
		double stateTax = 0;
		double federalTax = 0;
		double ficaWithholding = 0;
		double totalTax = 0;

		while (employeeNumber != 0) {

			// ask for employee number
			System.out.println("What's your employee number?");
			employeeNumber = keyboard.nextInt();
			if (employeeNumber == 0) {
				break;
			}

			// ask for gross pay
			System.out.println("What's your gross pay?");
			grossPay = keyboard.nextInt();

			// ask for state tax
			do {
				System.out.println("How much did state tax take?");
				stateTax = keyboard.nextInt();
				totalTax = stateTax;
			} while (stateTax > grossPay);

			// ask for federal tax
			do {
				System.out.println("How much did federal tax take?");
				federalTax = keyboard.nextInt();
				totalTax = federalTax;
			} while (federalTax > grossPay);

			// ask for FICA withholdings
			do {
				System.out.println("How much did FICA tax take?");
				ficaWithholding = keyboard.nextInt();
				totalTax = ficaWithholding;
			} while (ficaWithholding > grossPay);

			// conditionals
			while (totalTax > grossPay) {
				System.out.println(
						"The state tax, federal tax, and FICA tax have a sum greater than grosspay, please re-enter tax information");

				System.out.println("How much did state tax take?");
				stateTax = keyboard.nextInt();
				totalTax = stateTax;

				System.out.println("How much did federal tax take?");
				federalTax = keyboard.nextInt();
				totalTax = federalTax;

				System.out.println("How much did FICA tax take?");
				ficaWithholding = keyboard.nextInt();
				totalTax = ficaWithholding;
			}
			// keyboard.close();

			// instantiate PayRoll object
			PayRoll payRoll = new PayRoll(employeeNumber, grossPay, stateTax, federalTax, ficaWithholding);

			// Display
			System.out.printf("Gross pay: $%,.2f\n", payRoll.getGrossPay());
			System.out.printf("State tax: $%,.2f\n", payRoll.getStateTax());
			System.out.printf("Federal Tax: $%,.2f\n", payRoll.getFederalTax());
			System.out.printf("FICA withholdings: $%,.2f\n", payRoll.getFicaTax());
			System.out.printf("Net pay: $%,.2f\n\n", payRoll.calcNetPay());

		}
		keyboard.close();
	}
}
