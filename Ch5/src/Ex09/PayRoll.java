package Ex09;

public class PayRoll {
	// Instance variables
	private int idNumber;
	private double grossPay;
	private double stateTax;
	private double federalTax;
	private double fica;

	// constructors
	public PayRoll(int idNumber, double grossPay, double stateTax, double federalTax, double fica) {
		this.idNumber = idNumber;
		this.grossPay = grossPay;
		this.stateTax = stateTax;
		this.federalTax = federalTax;
		this.fica = fica;
	}

	public PayRoll() {

	}

	// accessors
	public double getGrossPay() {
		return this.grossPay;
	}

	public double getStateTax() {
		return this.stateTax;
	}

	public double getFederalTax() {
		return this.federalTax;
	}

	public double getFicaTax() {
		return this.fica;
	}

	// instance method
	public double calcNetPay() {
		double netPay = 0;
		netPay = grossPay - stateTax - federalTax - fica;

		return netPay;
	}
}
