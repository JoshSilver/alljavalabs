package Ex10;

public class SavingsAccount {
	// instance fields
	private double annualInterestRate;
	private double balance;
	private double interestEarned;

	// constructor
	public SavingsAccount(double startingBalance, double interestRate) {
		this.balance = startingBalance;
		this.annualInterestRate = interestRate / 100;
	}

	public SavingsAccount() {

	}

	// accessors
	public double getBalance() {
		return this.balance;
	}

	public double getInterestEarned() {
		return this.interestEarned;
	}

	// mutators
	public void withdraw(double withdrawAmount) {
		this.balance -= withdrawAmount;
	}

	public void deposit(double depositAmount) {
		this.balance += depositAmount;
	}

	// calculate monthly interest + keep total of interest earned
	public void monthlyInterest() {
		double monthlyInterestRate;
		double previousBalance = this.balance;
		monthlyInterestRate = (annualInterestRate / 12);
		this.balance = (this.balance * monthlyInterestRate) + this.balance;
		this.interestEarned += this.balance - previousBalance;
	}
}