package Ex10;

import java.util.Scanner;

public class SavingsAccountDriver {

	public static void main(String[] args) {
		// open scanner object
		Scanner keyboard = new Scanner(System.in);

		// get information from user
		System.out.println("What's the annual interest rate of your account?");
		double interest = keyboard.nextDouble();
		System.out.println("What's the starting balance of your account?");
		double startingBalance = keyboard.nextDouble();
		SavingsAccount savingsAccount = new SavingsAccount(startingBalance, interest);
		System.out.println("How many months have passed since you established your account?");
		double months = keyboard.nextDouble();

		// ask for deposit and withdraw amounts per month
		double amountDeposit = 0;
		double amountWithdraw = 0;

		for (int i = 1; i <= months; ++i) {
			System.out.printf("How much money did you deposit in month %d?\n", i);
			double deposit = keyboard.nextDouble();
			savingsAccount.deposit(deposit);
			System.out.printf("How much money did you withdraw in month %d?\n", i);
			double withdraw = keyboard.nextDouble();
			savingsAccount.withdraw(withdraw);
			amountDeposit += deposit;
			amountWithdraw += withdraw;
			savingsAccount.monthlyInterest();
		}
		keyboard.close();

		// print information
		System.out.printf("\nStarting balance: $%,.2f\n", startingBalance);
		System.out.printf("Ending balance: $%,.2f\n", savingsAccount.getBalance());
		System.out.printf("Total amount of deposits: $%,.2f\n", amountDeposit);
		System.out.printf("Total amount of withdraws: $%,.2f\n", amountWithdraw);
		System.out.printf("Total interest earned: $%,.2f", savingsAccount.getInterestEarned());
	}
}
