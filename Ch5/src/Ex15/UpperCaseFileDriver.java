package Ex15;

import java.io.IOException;

public class UpperCaseFileDriver {

	public static void main(String[] args) throws IOException {

		// reads the file lab14.txt and saves it to lab15.txt
		UpperCaseFile upperCaseFile = new UpperCaseFile("lab14.txt", "lab15.txt");
		System.out.print("The characters in file lab14.txt have been converted to upper case and saved as lab15.txt.");

		// this method prints to the console
		// upperCaseFile.toUpperCase();
	}

}
