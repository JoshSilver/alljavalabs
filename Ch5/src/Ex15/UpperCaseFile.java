package Ex15;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class UpperCaseFile {

	File openFile;
	Scanner readFile;

	// constructor
	public UpperCaseFile(String fileName, String fileName2) throws IOException {

		openFile = new File(fileName);
		readFile = new Scanner(openFile);

		String allCaps = "";
		while (readFile.hasNext()) {
			String string = readFile.nextLine() + " ";
			allCaps += string.toUpperCase();
		}
		PrintWriter outputFile = new PrintWriter(fileName2);
		outputFile.println(allCaps);
		outputFile.close();
		readFile.close();

	}

	// method
	public void toUpperCase() {
		while (readFile.hasNext()) {
			String string = readFile.nextLine();
			String upper = string.toUpperCase();
			System.out.println(upper);
		}

	}
}
