package Ex12;

import java.util.Scanner;

public class BarChart {

	public static void main(String[] args) {
		// instantiate Scanner
		Scanner keyboard = new Scanner(System.in);

		// ask for sales for 5 days
		System.out.println("What are the sales for store one?");
		double sales1 = keyboard.nextDouble();

		System.out.println("What are the sales for store two?");
		double sales2 = keyboard.nextDouble();

		System.out.println("What are the sales for store three?");
		double sales3 = keyboard.nextDouble();

		System.out.println("What are the sales for store four?");
		double sales4 = keyboard.nextDouble();

		System.out.println("What are the sales for store five?");
		double sales5 = keyboard.nextDouble();
		keyboard.close();

		// use display() to display the number of asterisks for each store
		System.out.println("SALES BAR CHART");
		System.out.println("Store 1: " + display(sales1));
		System.out.println("Store 2: " + display(sales2));
		System.out.println("Store 3: " + display(sales3));
		System.out.println("Store 4: " + display(sales4));
		System.out.println("Store 5: " + display(sales5));
	}

	// display method
	public static String display(double num) {
		String asterisk = "";
		int num2 = (int) (num / 100);

		for (int i = 1; i <= num2; ++i) {
			asterisk += '*';
		}
		return asterisk;
	}
}
