package Ex04;

import java.util.Scanner;

public class PenniesForPay {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		double pennies = .01;
		int daysWorked;
		double salary = 0;

		// request how many days worked
		System.out.println("How many days have you worked?");
		daysWorked = keyboard.nextInt();

		// make sure they choose a number greater than one
		while (daysWorked < 1) {
			System.out.println("Please choose a number of days greater than one.  How many days have you worked?");
			daysWorked = keyboard.nextInt();
		}
		keyboard.close();

		for (double i = 1, j = 2; i <= daysWorked; ++i) {
			System.out.printf("On day %.0f you made $%,.2f.\n", i, pennies);
			salary += pennies;
			pennies *= j;
		}
		System.out.printf("Your total salary is $%,.2f", salary);
	}
}