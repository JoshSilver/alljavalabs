package Ex06;

public class Population {
	private double organisms;
	private double avgPopulationIncrease;
	private double daysMultiplied;

	// constructor
	public Population(double organisms, double avgPopulationIncrease, double daysMultiplied) {
		this.organisms = organisms;
		this.avgPopulationIncrease = avgPopulationIncrease;
		this.daysMultiplied = daysMultiplied;
	}

	public Population() {

	}

	// instance method
	public void sizeOfPopulation() {
		double avgIncrease = this.avgPopulationIncrease / 100f;
		for (int i = 1; i <= daysMultiplied; i++) {
			double afterIncrease = organisms + (organisms * avgIncrease);
			// population += (this.organisms * avgIncrease);
			System.out.printf("The population after day %d is %.2f\n", i, afterIncrease);
			organisms = afterIncrease;
		}
	}
}
