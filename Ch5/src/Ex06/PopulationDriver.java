package Ex06;

import java.util.Scanner;

public class PopulationDriver {

	public static void main(String[] args) {
		// Scanner object
		Scanner keyboard = new Scanner(System.in);

		// ask for starting organisms
		System.out.println("To see how fast an organism wil populate, first enter how many there are starting out.");
		double organisms = keyboard.nextInt();
		while (organisms < 2) {
			System.out
					.println("There should be at least two organisms, will you re-enter how many organisms there are?");
			organisms = keyboard.nextInt();
		}

		// ask for percentage of daily increase
		System.out.println("By what percent do they increase daily?");
		double dailyIncrease = keyboard.nextInt();
		while (dailyIncrease < 0) {
			System.out.println("Sorry, you can't have a negative number there, how much do they increase?");
			dailyIncrease = keyboard.nextInt();
		}

		// ask for number of days multiplied
		System.out.println("How many days will they be increasing?");
		double numDays = keyboard.nextInt();
		while (numDays < 1) {
			System.out.println("Please enter at least one day.");
			numDays = keyboard.nextInt();
		}
		keyboard.close();

		// instantiate Population object with given information
		Population population = new Population(organisms, dailyIncrease, numDays);

		// display with instance method
		population.sizeOfPopulation();

	}

}
