package Ex07;

import java.util.Scanner;

public class AverageRainfall {
	public static void main(String[] args) {
		// Scanner object
		Scanner keyboard = new Scanner(System.in);

		// ask user for number of years
		System.out.println("How many years of rainfall?");
		int years = keyboard.nextInt();
		while (years < 1) {
			System.out.println("There needs to be at least one year. How many years of rainfall?");
			years = keyboard.nextInt();
		}

		// ask user how much rain fell per month
		int rainFall = 0;
		int totalRainFall = 0;
		for (int y = 1; y <= years; ++y) {
			System.out.printf("I'll ask how much ran fell each month in year %d\n", y);
			for (int m = 1; m <= 12; ++m) {
				System.out.printf("Month %d: ", m);
				rainFall = keyboard.nextInt();
				while (rainFall < 0) {
					System.out.println("Rainfall can't be negative. How much rain was there?");
					rainFall = keyboard.nextInt();
				}
				totalRainFall += rainFall;
			}
		}
		keyboard.close();

		// display number of months, total inches of rainfall, average rainfall per
		// month
		System.out.printf("Number of months: %d\n", years * 12);
		System.out.printf("Total rainfall: %din.\n", totalRainFall);
		System.out.printf("Average rainfall per month: %din.\n", totalRainFall / (years * 12));
	}
}
