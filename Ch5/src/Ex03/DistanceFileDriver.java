package Ex03;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import Ex02.DistanceTraveled;

public class DistanceFileDriver {

	public static void main(String[] args) throws IOException {
		// scanner
		Scanner keyboard = new Scanner(System.in);

		// ask user for info
		System.out.println("How fast were you driving?");
		float speed = keyboard.nextFloat();
		while (speed < 0) {
			System.out.println("That is not a valid speed.  Please enter a positive number for speed.");
			speed = keyboard.nextFloat();
		}

		System.out.println("How many hours did you drive?");
		float hours = keyboard.nextFloat();
		while (hours < 1) {
			System.out.println("That is not a valid time.  Please enter a number greater than one for time.");
			hours = keyboard.nextFloat();
		}
		keyboard.close();

		// instantiate DistanceTravled
		DistanceTraveled distanceTraveled = new DistanceTraveled(speed, hours);
		PrintWriter outputFile = new PrintWriter("Distance.txt");

		float mph = distanceTraveled.getDistance() / hours;
		for (int i = 1; i <= hours; i++) {
			outputFile.println("Hour" + i + ": " + mph * i);
		}
		outputFile.close();
	}
}
