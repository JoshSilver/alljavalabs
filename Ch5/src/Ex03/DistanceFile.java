package Ex03;

public class DistanceFile {
	// instance variables
	private float speedMPH;
	private float hoursTraveled;

	// constructor
	public DistanceFile(float speed, float hours) {
		this.speedMPH = speed;
		this.hoursTraveled = hours;
	}

	// instance method
	public float getDistance() {
		float distance = (this.speedMPH * hoursTraveled);
		return distance;
	}
}
