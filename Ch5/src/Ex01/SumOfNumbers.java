package Ex01;

import java.util.Scanner;

public class SumOfNumbers {

	public static void main(String[] args) {
		// instantiate scanner object
		Scanner keyboard = new Scanner(System.in);

		// ask the user for a positive nonzero integer value
		System.out.print("Enter a positive nonzero numer: ");
		int number = keyboard.nextInt();
		keyboard.close();
		for (int i = 1, y = 0;; i++) {
			y += i;
			if (i == number) {
				System.out.println("The sum of numbers is " + y + ".");
				break;
			}
		}
	}
}
