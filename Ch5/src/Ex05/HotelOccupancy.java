package Ex05;

import java.util.Scanner;

public class HotelOccupancy {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int totalRooms = 0;
		int numberOfRoomsOccupied = 0;

		// ask how many floors the hotel has
		System.out.println("How many floors does the hotel have?");
		int floors = keyboard.nextInt();
		while (floors < 1) {
			System.out.println("The hotel should have at least one floor, how many floors are there?");
			floors = keyboard.nextInt();
		}

		for (int i = 1; i <= floors; i++) {
			// ask how many rooms on each floor
			System.out.printf("How many rooms are on floor %d?\n", i);
			int roomsOnFloor = keyboard.nextInt();
			while (roomsOnFloor < 10) {
				System.out.printf("There should be at least 10 rooms on a floor. How many rooms are on floor %d?\n", i);
				roomsOnFloor = keyboard.nextInt();
			}
			// add rooms per floor to total floors
			totalRooms += roomsOnFloor;

			// ask how many rooms are occupied per floor
			System.out.printf("How many rooms are occupied on floor %d?\n", i);
			int occupiedRooms = keyboard.nextInt();

			// add rooms occupied per floor to total rooms occupied
			numberOfRoomsOccupied += occupiedRooms;
		}
		keyboard.close();

		// calculate vacant rooms and occupancy rate
		int vacantRooms = totalRooms - numberOfRoomsOccupied;
		float occupancyRate = (float) numberOfRoomsOccupied / (float) totalRooms;

		// display number of rooms, number of occupied rooms, number of vacant rooms,
		// and occupancy rate
		System.out.println("Total number of rooms: " + totalRooms);
		System.out.println("Number of occupied rooms: " + numberOfRoomsOccupied);
		System.out.println("Number of vacant rooms: " + vacantRooms);
		System.out.printf("Occupancy rate: %.2f%%", occupancyRate * 100);
	}

}
