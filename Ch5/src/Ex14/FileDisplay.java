package Ex14;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileDisplay {

	File openFile;
	Scanner readFile;

	// constructor
	public FileDisplay(String fileName) throws IOException {
		openFile = new File(fileName);
		readFile = new Scanner(openFile);
	}

	// methods
	public void displayHead() {
		for (int i = 1; i <= 5; ++i) {
			String string = readFile.nextLine();
			System.out.println(string);
		}
		readFile.close();
	}

	public void displayContents() {
		while (readFile.hasNext()) {
			String string = readFile.nextLine();
			System.out.println(string);
		}
		readFile.close();
	}

	public void displayWithLineNumbers() {
		for (int i = 1;; ++i) {
			String string = readFile.nextLine();
			System.out.println(i + ":" + string);
			if (readFile.hasNext() == false)
				break;
		}
		readFile.close();
	}
}