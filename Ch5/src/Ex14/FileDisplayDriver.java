package Ex14;

import java.io.IOException;

public class FileDisplayDriver {

	public static void main(String[] args) throws IOException {
		// instantiate FileDisplay object with name of file as arg
		FileDisplay displayHead = new FileDisplay("lab14.txt");
		FileDisplay displayContents = new FileDisplay("lab14.txt");
		FileDisplay displayWithLineNumbers = new FileDisplay("lab14.txt");

		// display first 5 lines
		displayHead.displayHead();
		System.out.println("");

		// display all lines
		displayContents.displayContents();
		System.out.println("");

		// display all lines with line number
		displayWithLineNumbers.displayWithLineNumbers();
		System.out.println("");
	}

}
