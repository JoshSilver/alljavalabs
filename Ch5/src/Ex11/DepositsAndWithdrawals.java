package Ex11;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import Ex10.SavingsAccount;

public class DepositsAndWithdrawals {

	public static void main(String[] args) throws IOException {
		// instantiate savingsAccount objects
		SavingsAccount savingsAccount = new SavingsAccount(500, 5);

		// instantiate objects that open and read from .txt files
		File myDeposits = new File("Deposits.txt");
		Scanner deposits = new Scanner(myDeposits);
		File myWithdrawals = new File("Withdrawals.txt");
		Scanner withdrawals = new Scanner(myWithdrawals);

		// read from .txt files using while loops
		double depositAmount = 0;
		while (deposits.hasNext()) {
			double number = deposits.nextDouble();
			depositAmount += number;
		}
		deposits.close();
		double withdrawAmount = 0;
		while (withdrawals.hasNext()) {
			double number = withdrawals.nextDouble();
			withdrawAmount += number;
		}

		// close withdrawals and deposits Scanner
		deposits.close();
		withdrawals.close();

		// perform necessary instance methods
		savingsAccount.deposit(depositAmount);
		savingsAccount.withdraw(withdrawAmount);
		savingsAccount.monthlyInterest();

		// print info
		System.out.printf("Total: $%,.2f\n", savingsAccount.getBalance());
		System.out.printf("Interest Earned: $%,.2f", savingsAccount.getInterestEarned());
	}
}
