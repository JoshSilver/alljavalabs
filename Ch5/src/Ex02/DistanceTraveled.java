package Ex02;

public class DistanceTraveled {
	// instance variables
	private float speedMPH;
	private float hoursTraveled;

	// constructor
	public DistanceTraveled(float speed, float hours) {
		this.speedMPH = speed;
		this.hoursTraveled = hours;
	}

	// instance method
	public float getDistance() {
		float distance = (this.speedMPH * hoursTraveled);
		return distance;
	}
}
