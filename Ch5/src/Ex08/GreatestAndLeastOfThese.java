package Ex08;

import java.util.Scanner;

public class GreatestAndLeastOfThese {

	public static void main(String[] args) {
		// Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);

		// Sentinel value
		final int QUIT = -99;

		// ask for a series of numbers
		System.out.println("Enter in a series of numbers, when you're done enter -99.");
		int userInput = keyboard.nextInt();

		// input userInput into min and max variables
		int max = userInput;
		int min = userInput;

		// until a sentinel value is entered, ask for a value and determine if that
		// value is the min or max value entered
		while (userInput != QUIT) {
			userInput = keyboard.nextInt();
			if (userInput > max && userInput != -99) {
				max = userInput;
			}
			if (userInput < min && userInput != -99) {
				min = userInput;
			}
		}
		keyboard.close();

		// print minimum and maximum values
		System.out.printf("Minimum Value: %d\n", min);
		System.out.printf("Maximum Value: %d\n", max);
	}
}
