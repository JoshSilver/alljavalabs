import java.util.Scanner;
public class FastFood {
	public static void main(String[] args) 
	{
		//variables
		final float CHEESEBURGER_PRICE = 1.50f;
		final float HAMBURGER_PRICE = 1.25f;
		final float SODA_PRICE = 1.95f;
		final float FRIES_PRICE = .95f;
		int numberOfCheeseburgers;
		int numberOfHamburgers;
		int numberOfSodas;
		int numberOfFries;
		float total;
		String name;
		
		//create Scanner
		Scanner keyboard = new Scanner(System.in);
		
		//request information from the user
		System.out.println("How many hamburgers do you want?");
		numberOfHamburgers = keyboard.nextInt();
		
		System.out.println("How many cheeseburgers do you want?");
		numberOfCheeseburgers = keyboard.nextInt();
		
		System.out.println("How many sodas do you want?");
		numberOfSodas = keyboard.nextInt();
		
		System.out.println("How many fries do you want?");
		numberOfFries = keyboard.nextInt();
		
		//remove newline
		keyboard.nextLine();
		
		System.out.println("What is your name?");
		name = keyboard.nextLine();
		
		//calculate total
		total = (numberOfHamburgers * HAMBURGER_PRICE) + 
				(numberOfCheeseburgers * CHEESEBURGER_PRICE) + 
				(numberOfSodas * SODA_PRICE) + 
				(numberOfFries * FRIES_PRICE);
		
		//display information to user
		
		System.out.println("\n" + name.toUpperCase());
		System.out.printf("Total: $%,.2f", total);		
	}
}