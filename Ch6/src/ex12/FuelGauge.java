package ex12;

public class FuelGauge {
	/********************
	 * Instance Methods *
	 ********************/
	public final int MAX_GALLONS = 15;
	private int gallons;
	
	/****************
	 * Constructors *
	 ****************/
	public FuelGauge()
	{
		
	}
	public FuelGauge(int g)
	{
		this.gallons = g;
	}
	
	/************
	* Accessors *
	*************/
	public int getGallons() 
	{
		return this.gallons;
	}
	
	/************
	 * Mutators *
	 ************/
	public void incrementGallons()
	{
		if (this.gallons < MAX_GALLONS)
		{
			this.gallons += 1;
		}
		else
		{
			System.out.println("You have a full tank");
		}
	}
	public void decrementGallons()
	{
		if (this.gallons > 0)
		{
			this.gallons -= 1;
		}
		if (this.gallons <= 0)
			System.out.println("You have an empty tank");
	}
}
