package ex12;

public class FuelGaugeDriver {

	public static void main(String[] args) {
		FuelGauge fuelGauge = new FuelGauge();
		Odometer odometer = new Odometer(0, fuelGauge);
		
		//fill up fuelGauge
		System.out.println("Before fill up...");
		System.out.println("Current mileage: " + odometer.getMileage());
		System.out.println("Current gallons: " + fuelGauge.getGallons());
		for(int i = 1; i <= 15; i++)
		{
			fuelGauge.incrementGallons();
		}
		
		//after fill up
		System.out.println("After fill up...");
		System.out.println("Current mileage: " + odometer.getMileage());
		System.out.println("Current gallons: " + fuelGauge.getGallons());
		
		System.out.println("Going for a drive...");
		for (;;)
		{
			System.out.println("\nCurrent gallons: " + fuelGauge.getGallons());
			System.out.println("Current mileage: " + odometer.getMileage());
			
			odometer.incrementMileage();
			if(fuelGauge.getGallons() <= 0)
				break;
		}
		
		
		
	}

}
