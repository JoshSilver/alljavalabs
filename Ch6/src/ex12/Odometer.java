package ex12;

public class Odometer {
	/********************
	 * Instance Methods *
	 ********************/
	public final int MAX_MILEAGE = 999999;
	public final int MPG = 24;
	private int mileage;
	private int setPoint;
	private FuelGauge fuelGauge;
	
	/****************
	 * Constructors *
	 ****************/
	public Odometer(int m, FuelGauge fg)
	{
		this.mileage = m;
		fuelGauge = fg;
	}
	
	/************
	* Accessors *
	*************/
	public int getMileage()
	{
		return this.mileage;
	}
	
	/***********
	 * Mutator *
	 ***********/
	public void incrementMileage()
	{
		this.mileage++;
		this.setPoint++;
		if(this.setPoint == 24)
		{
			fuelGauge.decrementGallons();
			this.setPoint = 0;
		}
		
		if (this.mileage > 999999)
		{
			this.mileage = 0;
		}
	}
}
