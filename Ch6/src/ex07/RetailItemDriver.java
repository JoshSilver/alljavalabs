package ex07;

public class RetailItemDriver {

	public static void main(String[] args) {
		RetailItem item = new RetailItem("VideoGame", 123, 40, 60);
		//System.out.println(item.toString());
		item.setWholeSaleCost(45);
		item.setRetailSaleCost(65);
		System.out.println(item.getWholeSaleCost());
		System.out.println(item.getRetailSaleCost());
		//System.out.println(item.toString());
	}

}
