package ex07;

import java.text.DecimalFormat;

public class RetailItem {
	private String description;
	private int itemNumber;
	private CostData cost;
	
	/*
	 * Constructor
	 */
	public RetailItem(String desc, int itemNum, double wholesale, double retail)
	{
		description = desc;
		itemNumber = itemNum;
		cost = new CostData(wholesale, retail);
	}
	public RetailItem(RetailItem obj) 
	{
		description = obj.description;
		itemNumber = obj.itemNumber;
		cost = obj.cost;
	}
	
	/*
	 * toString Method
	 */
	public String toString() 
	{
		String str;
		str = String.format("Description: %s\n" +
							"Item Number: %s\n" +
							"Wholesale Cost: $%,.2f\n" +
							"Retail Price: $%,.2f\n",
							description, itemNumber,
							cost.wholesale, cost.retail);
		return str;
	}
	
	/**
	 * CostData accessors
	 */
	public double getWholeSaleCost()
	{
		return cost.wholesale;
	}
	public double getRetailSaleCost()
	{
		return cost.retail;
	}
	public String getRetailSaleCostString()
	{
		double retailCost = getRetailSaleCost();
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String subTotalString = decimalFormat.format(retailCost);
		return subTotalString;
	}
	
	/**
	 * CostData accessor
	 */
	public void setWholeSaleCost(double w)
	{
		cost.wholesale = w;
	}
	public void setRetailSaleCost(double r)
	{
		cost.retail = r;
	}
	
	
	/*
	 * CostData inner class
	 */
	private class CostData
	{
		private double wholesale, retail;
		
		/**
		 * Constructor
		 */
		public CostData(double w, double r)
		{
			wholesale = w;
			retail = r;
		}
	
	}
}
