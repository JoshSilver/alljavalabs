package ex06;

public class EmployeeDriver {

	public static void main(String[] args) {
		Employee employee1 = new Employee();
		Employee employee2 = new Employee("Josh", 123);
		Employee employee3 = new Employee("Kevin", 213, "IT", "Programmer");

		System.out.println(employee1.toString());
		System.out.println(employee2.toString());
		System.out.println(employee3.toString());
	}

}
