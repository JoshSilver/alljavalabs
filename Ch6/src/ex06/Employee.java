package ex06;

public class Employee {
	// Instance Fields
	private String _name;
	private int _idNumber;
	private String _department;
	private String _position;

	// Constructor
	public Employee() {
		_name = "";
		_idNumber = 0;
		_department = "";
		_position = "";
	}

	public Employee(String name, int idNum, String department, String position) {
		_name = name;
		_idNumber = idNum;
		_department = department;
		_position = position;
	}

	public Employee(String name, int idNum) {
		_name = name;
		_idNumber = idNum;
		_department = "";
		_position = "";
	}

	// Mutators
	public void setName(String name) {
		_name = name;
	}

	public void setIdNumber(int idNum) {
		_idNumber = idNum;
	}

	public void setDepartment(String department) {
		_department = department;
	}

	public void setPosition(String position) {
		_position = position;
	}

	// Accessors
	public String getName() {
		return _name;
	}

	public int getIdNumber() {
		return _idNumber;
	}

	public String getDepartment() {
		return _department;
	}

	public String getPosition() {
		return _position + "\n";
	}

	public String toString() {
		String str = " ";
		str = "\nName: " + _name + "\nId Number: " + _idNumber + "\nDepartment: " + _department + "\nPosition: "
				+ _position;
		return str;
	}
}
