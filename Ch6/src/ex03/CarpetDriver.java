package ex03;

import java.util.Scanner;

public class CarpetDriver {

	public static void main(String[] args) {
		// instantiate scanner object
		Scanner keyboard = new Scanner(System.in);
		// ask for info
		System.out.println("What is the length of the room?");
		double length = keyboard.nextDouble();
		System.out.println("What is the width of the room?");
		double width = keyboard.nextDouble();
		System.out.println("What is the price per square foot of the carpet you want?");
		double price = keyboard.nextDouble();
		keyboard.close();
		// instantiate RoomDimension
		RoomDimension dimension = new RoomDimension(length, width);
		// instantiate RoomCarpet
		RoomCarpet carpet = new RoomCarpet(dimension, price);
		// print total cost
		System.out.print(dimension.toString());
		System.out.print(carpet.toString());
	}

}
