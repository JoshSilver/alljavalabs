package ex03;

public class RoomDimension {
	private double length;
	private double width;

	// constructors
	public RoomDimension(double l, double w) {
		this.length = l;
		this.width = w;
	}

	// return area of the room
	public double calcArea() {
		double area = 0;
		area = length * width;
		return area;
	}

	// toString()
	public String toString() {
		String str = "";
		str = String.format("Length: %.2f" +
							"\nWidth: %.2f" +
							"\nArea: %.2f",
							length, width, calcArea());
		return str;
		}
	}


