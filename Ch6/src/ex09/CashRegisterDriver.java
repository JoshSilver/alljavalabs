package ex09;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import ex07.RetailItem;


public class CashRegisterDriver {

	public static void main(String[] args) throws IOException 
	{
		//instantiate RetailItem
		RetailItem item = new RetailItem("VideoGame", 123, 50, 60);
				
		//instantiate scanner and ask for how many items are purchased
		Scanner input = new Scanner(System.in);
		System.out.print("How many items are you purchasing?");
		int itemCount = input.nextInt();
		input.close();
		
		//instantiate CashRegister and PrintWriter
		CashRegister register = new CashRegister(item, itemCount);
		PrintWriter salesReceiptFile = new PrintWriter("SalesReceipt.txt");
		
		//print to file
		salesReceiptFile.println("SALES RECEIPT");
		salesReceiptFile.println("Unit Price: $" + item.getRetailSaleCostString());
		salesReceiptFile.println("Quantity: " + itemCount);
		salesReceiptFile.println("Subtotal: $" + register.getSubTotal());
		salesReceiptFile.println("Sales Tax: $" + register.getSalesTax());
		salesReceiptFile.println("Total: $" + register.getTotal());
		salesReceiptFile.close();
		
		//print to console
		System.out.println(register.toString());
	}
}
