package ex09;

import java.text.DecimalFormat;

import ex07.RetailItem;

public class CashRegister {
	RetailItem item;
	int itemCount;

	/*
	 * Constructor
	 */
	public CashRegister(RetailItem item, int itemCount)
	{
		this.item = new RetailItem(item);
		this.itemCount = itemCount;
	}
	
	/*
	 * toString Method
	 */
	public String toString()
	{
		String str = "";
		str = String.format("Subtotal: $%,.2f" +
							"\nSalesTax: $%,.2f" +
							"\nTotal: $%,.2f",
							calcSubTotal(), calcSalesTax(), calcTotal());
		return str;
	}
	
	/*
	 * Instance Methods return double
	 */
	public double calcSubTotal()
	{
		double subtotal = 0;
		subtotal = item.getRetailSaleCost() * itemCount;
		return subtotal;
	}
	public double calcSalesTax()
	{
		double salesTax = 0;
		salesTax = (item.getRetailSaleCost() * .06) * itemCount;
		return salesTax;
	}
	public double calcTotal()
	{
		double total = 0;
		total = calcSubTotal() + calcSalesTax() ;
		return total;
	}
	
	/**
	 * 
	 * Methods return strings
	 */
	public String getSubTotal()
	{
		double subtotal = calcSubTotal();
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String subTotalString = decimalFormat.format(subtotal);
		return subTotalString;
	}
	
	public String getSalesTax()
	{
		double salesTax = calcSalesTax();
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String salesTaxString = decimalFormat.format(salesTax);
		return salesTaxString;
	}
	public String getTotal()
	{
		double total = calcTotal();
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String totalString = decimalFormat.format(total);
		return totalString;
	}
	
	
}
