package ex01;

public class Area {
	// area of circle
	public static double calcArea(double radius) {
		double area = 0;
		area = Math.PI * (radius * radius);
		return area;
	}

	// area of rectangle
	public static double calcArea(double width, double length) {
		double area = 0;
		area = width * length;
		return area;
	}

	// area of cylinder
	public static double calcArea(float radius, float height) {
		double area = 0;
		area = Math.PI * (radius * radius) * height;
		return area;
	}
}
