package HOT;

public class OverloadedMethods {

	public static int addNums(int num1, int num2)
	{
		int total = num1 + num2;
		return total;
	}
	public static int addNums(int num1, int num2, int num3)
	{
		int total = num1 + num2 + num3;
		return total;
	}
}
