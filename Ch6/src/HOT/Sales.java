package HOT;

import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

public class Sales {

	public static void main(String[] args) throws IOException {
		double totalSales = 0;
		Scanner userInput = new Scanner(System.in);
		PrintWriter outputFile = new PrintWriter("WeeklySales.txt");
		DecimalFormat decimal = new DecimalFormat("#.00");
		for(int i = 1; i <=5; ++i)
		{
			System.out.println("Enter in sales for day " + i);
			double input = userInput.nextDouble();
			while(input <= 0)
			{
				System.out.println("Enter in sales for day " + i);
				input = userInput.nextInt();
			}
			outputFile.println("Day" + i + ": $" + decimal.format(input));
			totalSales += input;
		}
		outputFile.println("Total Sales: $" + decimal.format(totalSales));
		userInput.close();
		outputFile.close();
	}
}
