package HOT;

public class Book {
	private String name;
	private String publishDate;
	private String author;
	
	//constructors
	public Book(String name, String publishDate, String author)
	{
		this.name = name;
		this.publishDate = publishDate;
		this.author = author;
	}
	public Book(Book obj)
	{
		name = obj.name;
		publishDate = obj.publishDate;
		author = obj.author;
	}
	
	public String toString()
	{
		String str = "";
		str = String.format("\nBookName: %s" +
							"\nPublishDate: %s" +
							"\nAuthor: %s",this.name, this.publishDate, this.author);
		return str;
	}
}
