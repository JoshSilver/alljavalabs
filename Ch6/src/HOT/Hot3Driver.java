package HOT;

public class Hot3Driver {

	public static void main(String[] args) {
		System.out.println("First method: " + OverloadedMethods.addNums(5, 2));
		System.out.println("Second method: " + OverloadedMethods.addNums(5, 2, 1));
	}

}
