package HOT;

public class LibraryDriver {

	public static void main(String[] args) {
		Book book = new Book("JAVA Early Objects", "2\\12\\18", "Tony Gaddis");
		Library library = new Library("Library Class", book);
		System.out.println(library.toString());
	}

}
