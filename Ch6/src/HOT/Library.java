package HOT;

public class Library {
	private String name;
	private Book book;
	
	//Constructors
	public Library (String name, Book book)
	{
		this.name = name;
		this.book = new Book(book);
	}
	
	//toString
	public String toString()
	{
		String str = " ";
		str = String.format("Class name: %s" +
							 "%s",this.name, book.toString());
		return str;
	}
}
