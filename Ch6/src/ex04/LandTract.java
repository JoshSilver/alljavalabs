package ex04;

public class LandTract {
	private double length;
	private double width;

	// constructor
	public LandTract(double l, double w) {
		length = l;
		width = w;
	}

	// method to return area
	public double calcArea() {
		double area = 0;
		area = length * width;
		return area;
	}

	public boolean equals(LandTract two) {
		boolean status;
		if (length == (two.length) && width == (two.width)) {
			status = true;
		} else
			status = false;
		return status;
	}
}
