package ex04;

import java.util.Scanner;

public class LandTractDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		System.out.print("Length of first tract: ");
		double lengthOne = keyboard.nextDouble();

		System.out.print("Width of first tract: ");
		double widthOne = keyboard.nextDouble();

		System.out.print("Length of second tract: ");
		double lengthTwo = keyboard.nextDouble();

		System.out.print("Width of second tract: ");
		double widthTwo = keyboard.nextDouble();

		LandTract tractOne = new LandTract(lengthOne, widthOne);
		LandTract tractTwo = new LandTract(lengthTwo, widthTwo);

		System.out.printf("Area of Tract One: %.2f\n", tractOne.calcArea());
		System.out.printf("Area of Tract Two: %.2f\n", tractTwo.calcArea());
		keyboard.close();

		if (tractOne.equals(tractTwo)) {
			System.out.println("Both tracts of land are equal.");

		} else
			System.out.println("Both tracts of land are not equal.");
	}

}
