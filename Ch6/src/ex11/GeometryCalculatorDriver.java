package ex11;
import java.util.Scanner;

public class GeometryCalculatorDriver {

	public static void main(String[] args) {
		//instantiate scanner
		Scanner userInput = new Scanner(System.in);
		
		//provide user a menu and ask for selection
		System.out.println("1. Calculate the Area of a circle.");
		System.out.println("2. Calculate the Area of a rectangle.");
		System.out.println("3. Calculate the Area of a triangle.");
		System.out.println("4. Quit");
		System.out.println("");
		System.out.println("Enter your choice (1-4)");
		
		int input = userInput.nextInt();
		
		while (input < 1 || input > 4)
		{
			System.out.println("Error: Please choose a number 1-4.");
			input = userInput.nextInt();
		}
		if (input == 1)
		{
			System.out.println("Enter in the radius of a circle.");
			float radius = userInput.nextFloat();
			System.out.println(GeometryCalculator.getArea(radius));
		}
		if (input == 2)
		{
			System.out.println("Enter in the length of a rectangle.");
			float length = userInput.nextFloat();
			System.out.println("Enter in the width of a rectangle.");
			float width = userInput.nextFloat();
			System.out.println(GeometryCalculator.getArea(length, width));
		}
		if (input == 3)
		{
			System.out.println("Enter in the base of a triangle.");
			double base = userInput.nextDouble();
			System.out.println("Enter in the height of a triangle.");
			double height = userInput.nextDouble();
			System.out.println(GeometryCalculator.getArea(base, height));
		}
		if (input == 4)
		{
			System.out.println("EXIT");
			System.exit(0);
		}
		userInput.close();
	}
}
