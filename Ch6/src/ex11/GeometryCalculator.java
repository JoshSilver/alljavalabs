package ex11;

public class GeometryCalculator {
	
	/*
	 * Area of a circle
	 */
	public static String getArea(float r) 
	{
		String str = "";
		if (r < 0)
		{
			str = "Error: Negative values are not allowed.";
		}
		else
		{
			str += (float)Math.PI * (float)Math.pow(r, 2);
		}
		return str;
	}
	/*
	 * Area of rectangle
	 */
	public static String getArea(float l, float w)
	{
		String str = "";
		if (l < 0 || w < 0)
		{
			str = "Error: Negative values are not allowed.";
		}
		else
		{
			str += l * w;
		}
		return str;
	}


	/*
	 * Area of triangle
	 */
	public static String getArea(double b, double h)
	{
		String str = "";
		if (b < 0 || h < 0)
		{
			str = "Error: Negative values are not allowed.";
		}
		else
		{
			str += b * h * .5f;
		}
		return str;
	}
}
