package ex08;


import ex07.RetailItem;

public class CashRegister {
	RetailItem item;
	int itemCount;

	/*
	 * Constructor
	 */
	public CashRegister(RetailItem item, int itemCount)
	{
		this.item = item;
		this.itemCount = itemCount;
	}
	
	/*
	 * toString Method
	 */
	public String toString()
	{
		String str = "";
		str = String.format("Subtotal: $%,.2f" +
							"\nSalesTax: $%,.2f" +
							"\nTotal: $%,.2f",
							calcSubTotal(), calcSalesTax(), calcTotal());
		return str;
	}
	
	/*
	 * Instance Methods
	 */
	public double calcSubTotal()
	{
		double subtotal = 0;
		subtotal = item.getRetailSaleCost() * itemCount;	
		return subtotal;
	}
	public double calcSalesTax()
	{
		double salesTax = 0;
		salesTax = (item.getRetailSaleCost() * .06) * itemCount;
		return salesTax;
	}
	public double calcTotal()
	{
		double total = 0;
		total = calcSubTotal() + calcSalesTax() ;
		return total;
	}
}
