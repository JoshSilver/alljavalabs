package ex08;

import ex07.RetailItem;

import java.util.Scanner;

public class CashRegisterDriver {

	public static void main(String[] args) {
		//instantiate RetailItem
		RetailItem item = new RetailItem("VideoGame", 123, 50, 60);
		item.setRetailSaleCost(10);
		//instantiate scanner and ask for how many items are purchased
		Scanner input = new Scanner(System.in);
		System.out.print("How many items are you purchasing?");
		int itemCount = input.nextInt();
		
		CashRegister register = new CashRegister(item, itemCount);
		System.out.println(register.toString());
		input.close();
	}

}
