package ex10;

public class PoliceOfficer {
	/*
	 * Instance fields
	 */
	private String name;
	private String badgeNumber;
	
	/*
	 * Constructors
	 */
	public PoliceOfficer(String name, String num)
	{
		this.name = name;
		this.badgeNumber = num;
	}
	
	/*
	 * Accessors
	 */
	public String getName()
	{
		return this.name;
	}
	public String getbadgeNumber() 
	{
		return this.badgeNumber;
	}
	
	/*
	 * Instance methods
	 */
	public boolean isTimeExpired(ParkingMeter meter, ParkedCar car)
	{
		boolean isExpired = false;
		if( meter.getMinutesPurchased() - car.getMinutesParked() < 0)
		{
			isExpired = true;
		}
		return isExpired;
	}
	public void issueTicket(ParkedCar car, PoliceOfficer officer)
	{
		ParkingTicket ticket = new ParkingTicket(car);
		System.out.println(ticket.reportVehicle());
		System.out.println(ticket.reportFineAmount());
		System.out.println(ticket.reportOfficer(officer));
	}
	
}
