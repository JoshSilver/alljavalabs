package ex10;

public class ParkedCar {
	private String make;
	private String model;
	private String color;
	private String plateNumber;
	private float minutesParked;
	
	
	/*
	 * Constructors
	 */
	public ParkedCar(String make, String model, String color, String plateNumber, float minutesParked)
	{
		this.make = make;
		this.model = model;
		this.color = color;
		this.plateNumber = plateNumber;
		this.minutesParked = minutesParked;
	}
	public ParkedCar(ParkedCar obj)
	{
		this.make = obj.make;
		this.model = obj.model;
		this.color = obj.color;
		this.plateNumber = obj.plateNumber;
		this.minutesParked = obj.minutesParked;
	}
	
	/*
	 * Accessors
	 */
	public String getMake()
	{
		return this.make;
	}
	public String getModel()
	{
		return this.model;
	}
	public String getColor()
	{
		return this.color;
	}
	public String getPlateNumber()
	{
		return this.plateNumber;
	}
	public float getMinutesParked()
	{
		return this.minutesParked;
	}
}
