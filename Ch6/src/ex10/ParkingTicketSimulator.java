package ex10;


public class ParkingTicketSimulator {

	public static void main(String[] args) {
		
		//Instantiate ParkedCar and ParkingMeter objects
		ParkedCar carOne = new ParkedCar("Kia", "Optima", "Grey", "123 456", 120);
		ParkingMeter meterOne = new ParkingMeter(60);
		
		ParkedCar carTwo = new ParkedCar("Honda", "Civic", "White", "654 321", 58);
		ParkingMeter meterTwo = new ParkingMeter(60);
		
		ParkedCar carThree = new ParkedCar("Ford", "Mustang", "Silver", "987 654", 300);
		ParkingMeter meterThree = new ParkingMeter(60);
		
		//Instantiate PoliceOfficer object
		PoliceOfficer officer = new PoliceOfficer("Rando", "P123456");
		boolean isExpired = officer.isTimeExpired(meterOne, carOne);
		if(isExpired == true)
		{
			System.out.println(carOne.getMake() + " is expired.");
			officer.issueTicket(carOne, officer);
		}
		else 
		{
			System.out.println("\n" + carOne.getMake() + " is not expired.");
		}
		
		isExpired = officer.isTimeExpired(meterTwo, carTwo);
		if(isExpired == true)
		{
			System.out.println("\n" + carTwo.getMake() + " is expired.");
			officer.issueTicket(carTwo, officer);
		}
		else 
		{
			System.out.println("\n" + carTwo.getMake() + " is not expired.");
		}
		isExpired = officer.isTimeExpired(meterThree, carThree);
		if(isExpired == true)
		{
			System.out.println("\n" + carThree.getMake() + " is expired.");
			officer.issueTicket(carThree, officer);
		}
		else 
		{
			System.out.println("\n" + carThree.getMake() + " is not expired.");
		}
	}
}
