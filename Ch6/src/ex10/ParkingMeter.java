package ex10;

public class ParkingMeter {
	/*
	 * Instance fields
	 */
	private float minutesPurchased;
	
	
	/*
	 * Constructor + Copy Constructor
	 */
	public ParkingMeter(float minutes)
	{
		this.minutesPurchased = minutes;
	}
	public ParkingMeter(ParkingMeter obj)
	{
		this.minutesPurchased = obj.minutesPurchased;
	}
	
	/*
	 * Accessors
	 */
	public float getMinutesPurchased()
	{
		return this.minutesPurchased;
	}
}
