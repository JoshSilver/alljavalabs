package ex10;

public class ParkingTicket {
	/*
	 * Instance variables
	 */
	ParkedCar car;
	
	/*
	 * Constructors
	 */
	public ParkingTicket(ParkedCar car)
	{
		this.car = car;
	}
	
	/*
	 * Instance Methods
	 */
	public String reportVehicle()
	{
		String str = "";
		str = String.format("\nMake: %s" +
							"\nModel: %s" +
							"\nColor: %s"+
							"\nLicense Number: %s",
							car.getMake(), car.getModel(), car.getColor(), car.getPlateNumber());
		return str;
	}
	public float reportFineAmount()
	{
		float fineAmount = 0;
		float hoursOver = 0;
		hoursOver = car.getMinutesParked() / 60f;
		
		if(hoursOver <= 1)
		{
			fineAmount += 25;
		}
		if (hoursOver > 1) 
		{
			fineAmount += ((hoursOver - 1) * 10) + 25;
		}
		return fineAmount;
	}
	public String reportOfficer(PoliceOfficer officer)
	{
		String str = "";
		str = String.format("\nOfficer Name: %s" +
							"\nOfficer Badge Number: %s",
							officer.getName(), officer.getbadgeNumber());
		return str;
	}
}
