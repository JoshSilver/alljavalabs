package ex13;

public class Player 
{
	private String name;
	private Die die1;
	private int points = 50;
	
	/**
	 * Constructor
	 */
	public Player(String name)
	{
		this.name = name;
	}
	
	/**
	 * Mutator
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	public void subtractPoint(int value)
	{
		int score = this.getScore();
		this.points -= value;
		if (this.points < 1) {
			this.points = score + value;
		}
	}
	
	/**
	 * Accessors
	 */
	public String getName()
	{
		return this.name;
	}
	public int getScore()
	{
		return this.points;
	}
}
