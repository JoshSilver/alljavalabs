package ex13;

import java.util.Random;
import java.util.Scanner;

public class Die 
{
	// instance fields
	private int sides;
	private int value;
	
	// constructor
	public Die(int numSides)
	{
		sides = numSides;
		roll();
	}
	
	// method
	public void roll()
	{
		Random rand = new Random();
		value = rand.nextInt(6) + 1;
	}
	
	// accessors
	public int getSides()
	{
		return sides;
	}
	
	
	public int getValue()
	{
		return value;
	}
}
