package ex13;

import java.util.Scanner;

public class FirstToOneGame 
{
	public static void main(String[] args)
	{
		Die die1 = new Die(6);
		Die die2 = new Die(6);
		Scanner input = new Scanner(System.in);
		
		System.out.print("FIRST TO ONE");
		
		System.out.printf("\n\nWhat is the name of Player one? ");
		String name1 = input.nextLine();
		Player playerOne = new Player(name1);
		
		System.out.printf("What is the name of Player two? ");
		String name2 = input.nextLine();
		Player playerTwo = new Player(name2);
		
		System.out.print("How many rounds do you want to play?");
		int rounds = input.nextInt();
		input.nextLine();
		for(int i = 1;i <= rounds;++i)
		{
			System.out.print("\n" + playerOne.getName() + " press \"Enter\" to roll the die");
			String playerOneRoll = input.nextLine();
			die1.roll();
			die2.roll();
			System.out.println("You rolled a " + die1.getValue() + " and " + die2.getValue());
			int playerOneRollTotal = die1.getValue() + die2.getValue();
			playerOne.subtractPoint(playerOneRollTotal);
			System.out.println("Your score is " + playerOne.getScore());
			
			if(playerOne.getScore() == 1)
			{
				System.out.println(playerOne.getName() + " is the Winner!");
			}
					
			
				//-----------------------------------------------------------------------//
			
			
			
			System.out.printf("\n" + playerTwo.getName() + " press \"Enter\" to roll the die");
			String playerTwoRoll = input.nextLine();
			die1.roll();
			die2.roll();
			System.out.println("You rolled a " + die1.getValue() + " and " + die2.getValue());
			int playerTwoRollTotal = die1.getValue() + die2.getValue();
			playerTwo.subtractPoint(playerTwoRollTotal);
			System.out.println("Your score is " + playerTwo.getScore());
			
			if(playerTwo.getScore() == 1)
			{
				System.out.println(playerTwo.getName() + " is the Winner!");
			}
			
		}
		
		if(playerOne.getScore() < playerTwo.getScore())
		{
			System.out.print(playerOne.getName() + " wins!");
		}
		else
			System.out.print(playerTwo.getName() + " wins!");
		
		System.out.println(" ");
		System.out.println(playerOne.getName() + playerOne.getScore());
		System.out.println(playerTwo.getName() + playerTwo.getScore());
	}
}
