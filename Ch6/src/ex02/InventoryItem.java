package ex02;

public class InventoryItem {
	private String description;
	private int units;

	// constructors
	public InventoryItem() {
		this.description = "";
		this.units = 0;
	}

	public InventoryItem(String d) {
		this.description = d;
		units = 0;
	}

	public InventoryItem(String d, int u) {
		this.description = d;
		units = u;
	}

	// copy constructor
	public InventoryItem(InventoryItem item2) {
		description = item2.description;
		units = item2.units;
	}

	// accessors
	public void setDescription(String d) {
		this.description = d;
	}

	public void setUnits(int u) {
		this.units = u;
	}

	public String getDescription() {
		return this.description;
	}

	public int getUnits() {
		return units;
	}

	// toString method
	public String toString() {
		return description + " " + units;
	}
}
