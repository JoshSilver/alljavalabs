package ex02;

public class InventoryItemDriver {

	public static void main(String[] args) {
		// create object to be cloned
		InventoryItem item1 = new InventoryItem("Sword", 3);

		// clone object
		InventoryItem item2 = new InventoryItem(item1);

		// print objects toString()
		System.out.println(item2);

		// repeat using a different constructor
		InventoryItem item3 = new InventoryItem("Shield");
		InventoryItem item4 = new InventoryItem(item3);
		System.out.println(item4);
	}
}
