package ex14;

import java.util.Scanner;

public class HeadsOrTailsGame {
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("HEADS OR TAILS GAME");
		
		System.out.printf("\n\nWhat is the name of Player one? ");
		String name1 = input.nextLine();
		Player playerOne = new Player(name1);
		
		System.out.print("What is the name of Player two? ");
		String name2 = input.nextLine();
		Player playerTwo = new Player(name2);
		
		for(;;)
		{
			System.out.printf("\n%s, pick heads or tails...", playerOne.getName());
			playerOne.setDecision(input.nextLine());
			while (!playerOne.getDecision().equalsIgnoreCase("heads")  && !playerOne.getDecision().equalsIgnoreCase("tails") )
			{
				System.out.printf("Try again %s, heads or tails?", playerOne.getName());
				playerOne.setDecision(input.nextLine());
			}
			
			System.out.printf("%s, pick heads or tail...", playerTwo.getName());
			playerTwo.setDecision(input.nextLine());
			while (!playerTwo.getDecision().equalsIgnoreCase("heads")  && !playerTwo.getDecision().equalsIgnoreCase("tails"))
			{
				System.out.printf("Try again %s, heads or tails?", playerTwo.getName());
				playerTwo.setDecision(input.nextLine());
			}
			
			Coin coin = new Coin();
			System.out.printf("\n" + "The coin landed on " + coin.getSideUp() + "\n");
			
			if(coin.getSideUp().equalsIgnoreCase(playerOne.getDecision()))
			{
				playerOne.addPoint();
				System.out.println(playerOne.getName() + " gets a point!");
			} 
			else {
				playerOne.subtractPoint();
				System.out.println(playerOne.getName() + " loses a point!");
			}
				
			if(coin.getSideUp().equalsIgnoreCase(playerTwo.getDecision()))
			{
				playerTwo.addPoint();
				System.out.println(playerTwo.getName() + " gets a point!");
			}
			else {
				playerTwo.subtractPoint();
				System.out.println(playerTwo.getName() + " loses a point!");
			}
				
			
			if (playerOne.getScore() == 5 || playerTwo.getScore() == 5)
			{
				if(playerOne.getScore() > playerTwo.getScore())
				{
					System.out.println(playerOne.getName() + " wins " + playerOne.getScore() + " to " + playerTwo.getScore());
					input.close();
					System.exit(0);
				} 
				else if (playerOne.getScore() < playerTwo.getScore())
				{
					System.out.println(playerTwo.getName() + " wins " + playerTwo.getScore() + " to " + playerOne.getScore());
					input.close();
					System.exit(0);
				}
				else
				{
					System.out.println("It was a tie.");
					input.close();
					System.exit(0);
				}
			}
		}
	}
}
