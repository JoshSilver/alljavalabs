package ex14;

import java.util.Random;

public class Coin {
	private String sideUp;
	
	/**
	 * Constructor
	 */
	public Coin()
	{
		toss();
	}
	
	/**
	 * Instance Methods
	 */
	public void toss()
	{
		Random rand = new Random(); 
		int value = rand.nextInt(1);
		if(value == 0) 
			sideUp = "heads";
		else 
			sideUp = "tails";
	}
	
	/**
	 * Accessor
	 */
	public String getSideUp()
	{
		return sideUp;
	}
}

