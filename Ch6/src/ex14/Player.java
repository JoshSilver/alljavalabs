package ex14;

public class Player {
	private String name;
	private String decision;
	private int score;
	
	/**
	 * Constructor
	 */
	public Player(String name)
	{
		this.name = name;
	}
	
	/**
	 * Mutator
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	public void setDecision(String decision)
	{
		this.decision = decision;
	}
	public void addPoint()
	{
		this.score += 1;
	}
	public void subtractPoint()
	{
		this.score -= 1;
		if (this.score < 0) {
			this.score = 0;
		}
	}
	
	/**
	 * Accessors
	 */
	public String getName()
	{
		return this.name;
	}
	public String getDecision()
	{
		return this.decision;
	}
	public int getScore()
	{
		return this.score;
	}
}
