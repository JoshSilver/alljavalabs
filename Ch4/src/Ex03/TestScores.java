package Ex03;

public class TestScores {
	// Instance Fields
	private double testScore1;
	private double testScore2;
	private double testScore3;

	// Constructor
	public TestScores() {

	}

	public TestScores(double testScore1, double testScore2, double testScore3) {
		this.testScore1 = testScore1;
		this.testScore2 = testScore2;
		this.testScore3 = testScore3;
	}

	// Mutator Methods
	public void setTestScore1(double testScore) {
		this.testScore1 = testScore;
	}

	public void setTestScore2(double testScore) {
		this.testScore2 = testScore;
	}

	public void setTestScore3(double testScore) {
		this.testScore3 = testScore;
	}

	// Accessor Methods
	public double getTestScore1() {
		return this.testScore1;
	}

	public double getTestScore2() {
		return this.testScore2;
	}

	public double getTestScore3() {
		return this.testScore3;
	}

	// Instance Method
	public double calcAverage() {
		double average = (testScore1 + testScore2 + testScore3) / 3f;
		return average;
	}

	public String letterGrade(double average) {
		if (average < 60) {
			return "F. :(";
		} else if (average <= 69) {
			return "D. :/";
		} else if (average <= 79) {
			return "C. :|";
		} else if (average <= 89) {
			return "B. :]";
		} else if (average < 100) {
			return "A. Good job!!";
		} else
			return null;
	}
}
