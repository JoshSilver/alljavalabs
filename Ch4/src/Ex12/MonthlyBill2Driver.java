package Ex12;

import java.util.Scanner;

public class MonthlyBill2Driver {

	public static void main(String[] args) {
		// Instantiate objects
		Scanner keyboard = new Scanner(System.in);
		MonthlyBill2 bill = new MonthlyBill2();

		// Get Information from user
		System.out.println("Do you have cell provider package A, B, or C?");
		String userInput = keyboard.nextLine();
		char input = userInput.charAt(0);
		bill.setSubscriptionPackage(input);

		System.out.println("How many minutes did you use?");
		float numMinutes = keyboard.nextFloat();
		bill.setNumOfMinutes(numMinutes);
		keyboard.close();

		// Print Information
		System.out.printf("The total charge is $%.2f.", bill.calcTotal());

		if (bill.getSubscriptionPackage() == 'A' || bill.getSubscriptionPackage() == 'a') {
			if (bill.calcTotal() > bill.calcB()) {
				float totalA = bill.calcTotal() - bill.calcB();
				System.out.printf("\nIf you had Package B you would have saved $%.2f.", totalA);
			}
			if (bill.calcTotal() > 69.99) {
				float totalC = bill.calcTotal() - 69.99f;
				System.out.printf("\nIf you had Package C you would have saved $%.2f.", totalC);
			}
		}
		if (bill.getSubscriptionPackage() == 'B' || bill.getSubscriptionPackage() == 'b') {
			if (bill.calcTotal() > 69.99) {
				float totalC = bill.calcTotal() - 69.99f;
				System.out.printf("\nIf you had Package C you would have saved $%.2f.", totalC);
			}
		}
	}
}
