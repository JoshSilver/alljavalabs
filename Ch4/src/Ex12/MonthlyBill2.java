package Ex12;

public class MonthlyBill2 {
	// Instance Variables
	char subscriptionPackage;
	float numOfMinutes;

	// Mutator Method
	public void setSubscriptionPackage(char subscription) {
		this.subscriptionPackage = subscription;
	}

	public void setNumOfMinutes(float numMinutes) {
		this.numOfMinutes = numMinutes;
	}

	// Accessor
	public char getSubscriptionPackage() {
		return this.subscriptionPackage;
	}

	// Instance MEthods
	public float calcTotal() {
		float total = 0;
		if (subscriptionPackage == 'A' || subscriptionPackage == 'a') {
			total += 39.99;
			if (numOfMinutes > 450) {
				total += (numOfMinutes - 450) * .45f;
			}
		} else if (subscriptionPackage == 'B' || subscriptionPackage == 'b') {
			total += 59.99;
			if (numOfMinutes > 900) {
				total += (numOfMinutes - 450) * .40f;
			}
		} else if (subscriptionPackage == 'C' || subscriptionPackage == 'c') {
			total += 69.99;
		}
		return total;
	}

	public float calcB() {
		float total = 0;
		total += 59.99;
		if (numOfMinutes > 900) {
			total += (numOfMinutes - 450) * .40f;
		}
		return total;
	}

	public float calcC() {
		float total = 0;
		total += 69.99;
		return total;
	}
}
