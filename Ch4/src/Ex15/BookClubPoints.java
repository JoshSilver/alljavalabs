package Ex15;

import java.util.Scanner;

public class BookClubPoints {

	public static void main(String[] args) {
		// Variables
		int numPurchases;
		int numPoints = 0;

		// Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);

		// Request info from user
		System.out.print("How many books have you purchased this month?");
		numPurchases = keyboard.nextInt();
		keyboard.close();

		if (numPurchases == 0) {
			numPoints = 0;
			System.out.printf("You've been awarded %d points!", numPoints);
		} else if (numPurchases == 1) {
			numPoints = 5;
			System.out.printf("You've been awarded %d points!", numPoints);
		} else if (numPurchases == 2) {
			numPoints = 15;
			System.out.printf("You've been awarded %d points!", numPoints);
		} else if (numPurchases == 3) {
			numPoints = 30;
			System.out.printf("You've been awarded %d points!", numPoints);
		} else if (numPurchases >= 4) {
			numPoints = 60;
			System.out.printf("You've been awarded %d points!", numPoints);
		}

	}

}
