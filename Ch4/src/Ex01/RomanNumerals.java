package Ex01;

import java.util.Scanner;

public class RomanNumerals {

	public static void main(String[] args) {
		int userResponse;
		char repeat;
		String response;
		Scanner keyboard = new Scanner(System.in);

		do {
			System.out.print("Enter a number 1-10 to see the Roman Numeral equivalent.");
			userResponse = keyboard.nextInt();
			keyboard.nextLine();
			if (userResponse == 1) {
				System.out.println("I");
			} else if (userResponse == 2) {
				System.out.println("II");
			} else if (userResponse == 3) {
				System.out.println("III");
			} else if (userResponse == 4) {
				System.out.println("IV");
			} else if (userResponse == 5) {
				System.out.println("V");
			} else if (userResponse == 6) {
				System.out.println("VI");
			} else if (userResponse == 7) {
				System.out.println("VII");
			} else if (userResponse == 8) {
				System.out.println("VIII");
			} else if (userResponse == 9) {
				System.out.println("IX");
			} else if (userResponse == 10) {
				System.out.println("X");
			} else {
				System.out.println("That is outside the range of 1-10.");
			}

			System.out.println("Would you like to try again? type yes or no");
			response = keyboard.nextLine();
			keyboard.close();
			repeat = response.charAt(0);
		} while (repeat == 'y' || repeat == 'Y');
	}
}
