package Ex02;

import java.util.Scanner;

public class TimeCalculator {

	public static void main(String[] args) {
		int userInput;
		Scanner keyboard = new Scanner(System.in);

		System.out.println("Please enter a number of seconds...");
		userInput = keyboard.nextInt();
		keyboard.close();
		if (userInput >= 60 && userInput < 3600) {
			int numOfMinutes;
			int numOfSeconds;
			numOfMinutes = userInput / 60;
			numOfSeconds = userInput % 60;

			System.out.println(numOfMinutes + " Minutes");
			System.out.println(numOfSeconds + " Seconds");

		} else if (userInput >= 3600 && userInput < 86400) {
			int numOfHours;
			int numOfMinutes;
			int numOfSeconds;

			numOfHours = userInput / 3600;
			numOfMinutes = (userInput % 3600) / 60;
			numOfSeconds = (userInput % 3600) % 60;

			System.out.println(numOfHours + " Hours");
			System.out.println(numOfMinutes + " Minutes");
			System.out.println(numOfSeconds + " Seconds");
		} else if (userInput >= 86400) {
			int numOfDays;
			int numOfHours;
			int numOfMinutes;
			int numOfSeconds;

			numOfDays = userInput / 86400;
			numOfHours = (userInput % 86400) / 3600;
			numOfMinutes = ((userInput % 86400) % 3600) / 60;
			numOfSeconds = (userInput % 86400) % 3600 % 60;

			System.out.println(numOfDays + " Day/s");
			System.out.println(numOfHours + " Hours");
			System.out.println(numOfMinutes + " Minutes");
			System.out.println(numOfSeconds + " Seconds");
		}
	}
}
