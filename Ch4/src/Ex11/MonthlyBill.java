package Ex11;

public class MonthlyBill {
	// Instance Variables
	char subscriptionPackage;
	float numOfMinutes;

	// Mutator Method
	public void setSubscriptionPackage(char subscription) {
		this.subscriptionPackage = subscription;
	}

	public void setNumOfMinutes(float numMinutes) {
		this.numOfMinutes = numMinutes;
	}

	// Instance MEthods
	public float calcTotal() {
		float total = 0;
		if (subscriptionPackage == 'A' || subscriptionPackage == 'a') {
			total += 39.99;
			if (numOfMinutes > 450) {
				total += (numOfMinutes - 450) * .45f;
			}
		} else if (subscriptionPackage == 'B' || subscriptionPackage == 'b') {
			total += 59.99;
			if (numOfMinutes > 900) {
				total += (numOfMinutes - 450) * .40f;
			}
		} else if (subscriptionPackage == 'C' || subscriptionPackage == 'c') {
			total += 69.99;
		}
		return total;
	}
}
