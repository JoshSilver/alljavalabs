package Ex11;

import java.util.Scanner;

public class MonthlyBillDriver {

	public static void main(String[] args) {
		// Instantiate objects
		Scanner keyboard = new Scanner(System.in);
		MonthlyBill bill = new MonthlyBill();

		// Get Information from user
		System.out.println("Do you have cell provider package A, B, or C?");
		String userInput = keyboard.nextLine();
		char input = userInput.charAt(0);
		bill.setSubscriptionPackage(input);

		System.out.println("How many minutes did you use?");
		float numMinutes = keyboard.nextFloat();
		bill.setNumOfMinutes(numMinutes);
		keyboard.close();

		// Print Information
		System.out.printf("The total charge is $%.2f.", bill.calcTotal());
	}

}
