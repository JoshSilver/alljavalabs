package Ex10;

public class Temperature {
	// Instance fields
	private int temp;

	// Mutator method
	public void setTemp(int temp) {
		this.temp = temp;
	}

	// Accessor method
	public int getTemp() {
		return this.temp;
	}

	// Instance Method
	public boolean isEthylFreezing() {
		if (this.temp <= -173) {
			return true;
		} else
			return false;
	}

	public boolean isEthylBoiling() {
		if (this.temp >= 172) {
			return true;
		} else
			return false;
	}

	public boolean isOxygenFreezing() {
		if (this.temp <= -362) {
			return true;
		} else
			return false;
	}

	public boolean isOxygenBoiling() {
		if (this.temp >= -306) {
			return true;
		} else
			return false;
	}

	public boolean isWaterFreezing() {
		if (this.temp <= 32) {
			return true;
		} else
			return false;
	}

	public boolean isWaterBoiling() {
		if (this.temp >= 212) {
			return true;
		} else
			return false;
	}
}
