package Ex10;

import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) {
		// Instantiate objects
		Scanner keyboard = new Scanner(System.in);
		Temperature temp = new Temperature();

		// Request input from user
		System.out.println("What is the temperature outside?");
		int temperature = keyboard.nextInt();
		keyboard.close();
		temp.setTemp(temperature);

		// Print information
		if (temp.isEthylBoiling() == true) {
			System.out.println("Ethyl will boil at that temperature.");
		}
		if (temp.isEthylFreezing() == true) {
			System.out.println("Ethyl will freeze at that temperature.");
		}
		if (temp.isWaterFreezing() == true) {
			System.out.println("Water will freeze at that temperature.");
		}
		if (temp.isWaterBoiling() == true) {
			System.out.println("Water will boil at that temperature.");
		}
		if (temp.isOxygenBoiling() == true) {
			System.out.println("Oxygen will boil at that temperature.");
		}
		if (temp.isOxygenFreezing() == true) {
			System.out.println("Oxygen will freeze at that temperature.");
		}

	}

}
