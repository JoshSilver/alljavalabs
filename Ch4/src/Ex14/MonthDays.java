package Ex14;

public class MonthDays {
	int month;
	int year;

	// Constructor
	public MonthDays(int month, int year) {
		this.month = month;
		this.year = year;
	}

	public MonthDays() {

	}

	// Instance MEthod
	public int getNumberOfDays() {
		// Local Variable
		int days = 0;

		switch (month) {
		case 1:
			days = 31;
			break;
		case 2:
			days = 28;
			break;
		case 3:
			days = 31;
			break;
		case 4:
			days = 30;
			break;
		case 5:
			days = 31;
			break;
		case 6:
			days = 30;
			break;
		case 7:
			days = 31;
			break;
		case 8:
			days = 31;
			break;
		case 9:
			days = 30;
			break;
		case 10:
			days = 31;
			break;
		case 11:
			days = 30;
			break;
		case 12:
			days = 31;
			break;
		}
		if (month == 2) {
			if (this.year % 100 == 0) {
				if (this.year % 400 == 0) {
					days = 29;
				}
			}
			if (this.year % 100 != 0) {
				if (this.year % 4 == 0) {
					days = 29;
				}
			}
		}
		return days;
	}
}
