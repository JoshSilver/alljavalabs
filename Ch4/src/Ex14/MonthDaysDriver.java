package Ex14;

import java.util.Scanner;

public class MonthDaysDriver {

	public static void main(String[] args) {
		// Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);

		// Request info from user
		System.out.print("Enter a month (1-12): ");
		int month = keyboard.nextInt();

		System.out.print("Enter a year: ");
		int year = keyboard.nextInt();
		keyboard.close();

		// Instantiate MonthDays
		MonthDays monthDays = new MonthDays(month, year);
		System.out.printf("%d", monthDays.getNumberOfDays());
	}

}
