package HOT;

import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {
		// Instantiate Scanner
		Scanner keyboard = new Scanner(System.in);

		// Request user info of first yard
		System.out.println("What is the length of the first yard in feet?");
		float firstLength = keyboard.nextFloat();
		System.out.println("What is the width of the first yard in feet?");
		float firstWidth = keyboard.nextFloat();

		// request user info of second yard
		System.out.println("What is the length of the second yard in feet?");
		float secondLength = keyboard.nextFloat();
		System.out.println("What is the width of the second yard in feet?");
		float secondWidth = keyboard.nextFloat();
		keyboard.close();

		// Instantiate firstLawn object using constructor with 2 args
		Lawn firstLawn = new Lawn(firstLength, firstWidth);

		// Instantiate secondLawn object with default constructor and use getters and
		// setters
		Lawn secondLawn = new Lawn();
		secondLawn.setLength(secondLength);
		secondLawn.setWidth(secondWidth);

		// Calculate sq feet of both lawns
		float firstSqFt = firstLawn.calcSquareFeet();
		float secondSqFt = secondLawn.calcSquareFeet();
		final float SEASON = 20;

		// Decide what the weeklyFee and seaonalFee will be for firstLawn
		float weeklyFee = 0;
		if (firstSqFt < 400) {
			weeklyFee = 25;
		} else if (firstSqFt >= 400 && firstSqFt < 600) {
			weeklyFee = 35;
		} else if (firstSqFt >= 600) {
			weeklyFee = 50;
		}
		float firstWeeklyFee = weeklyFee;
		float firstSeasonalFee = firstWeeklyFee * SEASON;

		// Decide what weeklyFee and seasonalFee will be for secondLawn
		if (secondSqFt < 400) {
			weeklyFee = 25;
		} else if (secondSqFt >= 400 && secondSqFt < 600) {
			weeklyFee = 35;
		} else if (secondSqFt >= 600) {
			weeklyFee = 50;
		}
		float secondWeeklyFee = weeklyFee;
		float secondSeasonalFee = secondWeeklyFee * SEASON;

		// Print fees of firstLawn
		System.out.printf("The weekly fee for the first yard is $%,.2f.\n", firstWeeklyFee);
		System.out.printf("The seaonal fee for the first yard is $%,.2f.\n", firstSeasonalFee);

		// Print fees of seconLawn
		System.out.printf("The weekly fee for the second yard is $%,.2f.\n", secondWeeklyFee);
		System.out.printf("The seaonal fee for the second yard is $%,.2f.\n", secondSeasonalFee);
	}
}
