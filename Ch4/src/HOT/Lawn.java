package HOT;

public class Lawn {
	// Instance Fields
	private float length, width;

	// Constructor
	public Lawn(float length, float width) {
		this.length = length;
		this.width = width;
	}

	public Lawn() {

	}

	// Mutator
	public void setLength(float length) {
		this.length = length;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	// Accessor
	public float getLength() {
		return this.length;
	}

	public float getwidth() {
		return this.width;
	}

	// Instance method that returns sq. feet of lawn
	public float calcSquareFeet() {
		float sqFt = this.length * this.width;
		return sqFt;
	}

}
