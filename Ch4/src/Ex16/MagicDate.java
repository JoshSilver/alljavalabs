package Ex16;

public class MagicDate {
	// local variables
	private int month;
	private int day;
	private int year;

	// constructor
	public MagicDate(int month, int day, int year) {
		this.month = month;
		this.day = day;
		this.year = year;
	}

	public MagicDate() {

	}

	// instance method
	public boolean isMagic() {
		// local variables
		boolean isMagic = false;
		int monthDay = month * day;

		if (year == monthDay) {
			isMagic = true;
		}
		return isMagic;
	}
}
