package Ex16;

import java.util.Scanner;

public class MagicDateDriver {

	public static void main(String[] args) {
		// instantiate Scanner object
		Scanner keyboard = new Scanner(System.in);

		// Request date from user
		System.out.print("Enter a digit for the month: ");
		int month = keyboard.nextInt();

		System.out.print("Enter a digit for the day: ");
		int day = keyboard.nextInt();

		System.out.print("Enter two digits for the year: ");
		int year = keyboard.nextInt();
		keyboard.close();

		// instantiate MagicDate object
		MagicDate magicDate = new MagicDate(month, day, year);
		boolean isMagic = magicDate.isMagic();

		if (isMagic) {
			System.out.printf("The date you entered is magical!");
		} else
			System.out.printf("The date you entered is NOT magical!");
	}
}
