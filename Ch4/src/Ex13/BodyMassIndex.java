package Ex13;

public class BodyMassIndex {
	// Instance Variables
	private float bodyMassIndex;

	// Constructor
	public BodyMassIndex(float height, float weight) {
		this.bodyMassIndex = (weight / (height * height)) * 703;
	}

	public BodyMassIndex() {

	}

	// Accessor
	public float getBodyMassIndex() {
		return this.bodyMassIndex;
	}
}
