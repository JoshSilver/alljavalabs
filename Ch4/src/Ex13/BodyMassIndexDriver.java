package Ex13;

import java.util.Scanner;

public class BodyMassIndexDriver {

	public static void main(String[] args) {
		// Instantiate Scanner object
		Scanner keyboard = new Scanner(System.in);

		// Request info from user
		System.out.println("What is your height in inches?");
		float height = keyboard.nextFloat();

		System.out.println("What is your weight in lbs?");
		float weight = keyboard.nextFloat();
		keyboard.close();

		// Instantiate BodyMassIndex and put into variable
		BodyMassIndex bmi = new BodyMassIndex(height, weight);
		float bodyMassIndex = bmi.getBodyMassIndex();

		// Display information
		if (bodyMassIndex < 18.5) {
			System.out.printf("With a BMI of %.2f, you are underweight.", bodyMassIndex);
		} else if (bodyMassIndex >= 18.5 && bodyMassIndex <= 25) {
			System.out.printf("With a BMI of %.2f, you're weight is optimal.", bodyMassIndex);
		} else if (bodyMassIndex > 25) {
			System.out.printf("With a BMI of %.2f, you are overweight.", bodyMassIndex);
		}
	}
}
