package Ex19;

import java.util.Scanner;

public class DiagnosticTreeDriver {

	public static void main(String[] args) {
		/*
		 * String userInput = "";
		 * 
		 * Scanner keyboard = new Scanner(System.in); System.out.
		 * println("Reboot the computer and try to reconnect.\nDid that fix the problem? "
		 * ); userInput = keyboard.nextLine(); if (userInput.charAt(0) == 'n') {
		 * 
		 * } else { keyboard.close(); return; }
		 * 
		 * System.out.
		 * println("Reboot the router and try to reconnect.\nDid that fix the problem? "
		 * ); userInput = keyboard.nextLine(); if (userInput.charAt(0) == 'n') {
		 * 
		 * } else { keyboard.close(); return; }
		 * 
		 * System.out.println(
		 * "Make sure the cables between the router and the modem are plugged in firmly.\nDid that fix the problem? "
		 * ); userInput = keyboard.nextLine(); if (userInput.charAt(0) == 'n') {
		 * 
		 * } else { keyboard.close(); return; }
		 * 
		 * System.out.
		 * println("Move the router to a new location.\nDid that fix the problem? ");
		 * userInput = keyboard.nextLine(); keyboard.close(); if (userInput.charAt(0) ==
		 * 'n') {
		 * 
		 * } else { keyboard.close(); return; }
		 * System.out.println("Get a new router. ");
		 */

		// instantiate objects
		DiagnosticTree diagnosticTree = new DiagnosticTree();
		Scanner keyboard = new Scanner(System.in);

		// variables
		String userInput;
		boolean yes;

		// request info from user
		System.out.print("Reboot the computer and try to reconnect.\nDid that fix the problem? ");
		userInput = keyboard.nextLine();
		yes = diagnosticTree.isDiagnosticTreeValid(userInput);
		if (yes) {
			keyboard.close();
			return;
		}

		System.out.print("Reboot the router and try to reconnect.\nDid that fix the problem? ");
		userInput = keyboard.nextLine();
		yes = diagnosticTree.isDiagnosticTreeValid(userInput);
		if (yes) {
			keyboard.close();
			return;
		}

		System.out.print(
				"Make sure the cables between the router and the modem are plugged in firmly.\nDid that fix the problem? ");
		userInput = keyboard.nextLine();
		yes = diagnosticTree.isDiagnosticTreeValid(userInput);
		if (yes) {
			keyboard.close();
			return;
		}

		System.out.println("Move the router to a new location.\nDid that fix the problem? ");
		userInput = keyboard.nextLine();
		yes = diagnosticTree.isDiagnosticTreeValid(userInput);
		if (yes) {
			keyboard.close();
			return;
		}
		keyboard.close();
	}
}
