package Ex09;

public class SpeedOfSound {
	// Instance Field
	private double distance;

	// Mutator Method
	public void setDistance(double distance) {
		this.distance = distance;
	}

	// Accessor Method
	public double getDistance() {
		return this.distance;
	}

	// Instance Methods
	public double getSpeedInAir() {
		double time = 0;
		time = this.distance / 1100f;
		return time;
	}

	public double getSpeedInWater() {
		double time = 0;
		time = this.distance / 4900f;
		return time;
	}

	public double getSpeedInSteel() {
		double time = 0;
		time = this.distance / 16400f;
		return time;
	}
}
