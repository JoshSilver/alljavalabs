package Ex09;

import java.util.Scanner;

public class SpeedOfSoundDriver {

	public static void main(String[] args) {
		// Instantiate Scanner and SpeedOfSound objects
		Scanner keyboard = new Scanner(System.in);
		SpeedOfSound speedOfSound = new SpeedOfSound();

		// Request input from user
		System.out.println(
				"Please select a medium to view the speed of sound, press '1' for Air, '2' for Water, or '3' for Steel.");
		double userInput = keyboard.nextDouble();

		System.out.println("What is the distance a soundwave will travel?");
		double distance = keyboard.nextDouble();
		keyboard.close();

		// set distance of speedOfSound object
		speedOfSound.setDistance(distance);

		if (userInput == 1) {
			System.out.printf("%.2f feet per second.", speedOfSound.getSpeedInAir());
		} else if (userInput == 2) {
			System.out.printf("%.2f feet per second.", speedOfSound.getSpeedInWater());
		} else if (userInput == 3) {
			System.out.printf("%.2f feet per second.", speedOfSound.getSpeedInSteel());
		}
	}
}
