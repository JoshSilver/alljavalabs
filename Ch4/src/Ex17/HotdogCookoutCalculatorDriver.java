package Ex17;

import java.util.Scanner;

public class HotdogCookoutCalculatorDriver {

	public static void main(String[] args) {
		// instantiate Scanner
		Scanner keyboard = new Scanner(System.in);

		// request info from user
		System.out.print("How many people are coming to the party?");
		int partyGoers = keyboard.nextInt();

		System.out.print("How many Hotdogs will each person get?");
		int hotdogsGivenOut = keyboard.nextInt();
		keyboard.close();

		// instantiate HotdogCookoutCalculator
		HotdogCookoutCalculator hotdog = new HotdogCookoutCalculator(partyGoers, hotdogsGivenOut);

		System.out.printf("You need a minimum of %d hotdog packages.", hotdog.minHotdogPacks());
		System.out.printf("You need a minimum of %d bun packages.", hotdog.minBunPacks());
		System.out.printf("You will have %d hotdogs leftover.", hotdog.hotdogsLeftOver());
		System.out.printf("You will have %d buns leftover.", hotdog.bunsLeftOver());

	}
}
