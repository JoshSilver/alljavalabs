package Ex17;

public class HotdogCookoutCalculator {
	// instance variables
	private int numPeople;
	private int numHotdogsGiven;
	private final int HOTDOG_PACKAGE = 10;
	private final int BUN_PACKAGE = 8;

	// constructor
	public HotdogCookoutCalculator(int numPeople, int numHotdogsGiven) {
		this.numPeople = numPeople;
		this.numHotdogsGiven = numHotdogsGiven;
	}

	// instance methods
	public int minHotdogPacks() {
		int minPacks = 0;
		minPacks = (numHotdogsGiven * numPeople);
		if (minPacks % HOTDOG_PACKAGE != 0) {
			minPacks = (numHotdogsGiven * numPeople) / HOTDOG_PACKAGE + 1;
		} else {
			minPacks = (numHotdogsGiven * numPeople) / HOTDOG_PACKAGE;
		}
		return minPacks;
	}

	public int minBunPacks() {
		int minPacks = 0;
		minPacks = (numHotdogsGiven * numPeople);
		if (minPacks % BUN_PACKAGE != 0) {
			minPacks = (numHotdogsGiven * numPeople) / BUN_PACKAGE + 1;
		} else {
			minPacks = (numHotdogsGiven * numPeople) / BUN_PACKAGE;
		}
		return minPacks;
	}

	public int hotdogsLeftOver() {
		int hotdogsNeeded = (numHotdogsGiven * numPeople);
		int hotdogPacks = minHotdogPacks();
		int hotdogsPurchased = hotdogPacks * HOTDOG_PACKAGE;
		int leftover = 0;
		if (hotdogsPurchased % hotdogsNeeded != 0) {
			leftover = hotdogsPurchased - hotdogsNeeded;
		} else
			leftover = 0;

		return leftover;
	}

	public int bunsLeftOver() {
		int bunsNeeded = (numHotdogsGiven * numPeople);
		int bunPacks = minBunPacks();
		int bunsPurchased = bunPacks * BUN_PACKAGE;
		int leftover = 0;
		if (bunsPurchased % bunsNeeded != 0) {
			leftover = bunsPurchased - bunsNeeded;
		} else
			leftover = 0;

		return leftover;
	}
}
