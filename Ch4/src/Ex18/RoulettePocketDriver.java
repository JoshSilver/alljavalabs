package Ex18;

import java.util.Scanner;

public class RoulettePocketDriver {

	public static void main(String[] args) {
		// instantiate scanner objects
		Scanner keyboard = new Scanner(System.in);

		// gather info from user
		System.out.println("Enter a pocket number: ");
		String pocketNumber = keyboard.nextLine();
		keyboard.close();

		// instantiate RoulettePacket object
		RoulettePocket roulettePocket = new RoulettePocket(pocketNumber);

		// print info to user
		System.out.printf("%s", roulettePocket.getPocketColor());
	}

}
