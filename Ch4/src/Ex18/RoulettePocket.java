package Ex18;

public class RoulettePocket {
	// instance variables
	private int number;

	// constructor
	public RoulettePocket(String pocketNumber) {
		number = Integer.parseInt(pocketNumber);
	}

	// method
	public String getPocketColor() {
		String pocketColor = "";

		if (number >= 0 && number <= 36) {
			if (number == 0 || number == 00) {
				pocketColor = "That's a green pocket.";
			} else if (number >= 1 && number <= 10) {
				if (number % 2 == 0) {
					pocketColor = "That's a black pocket.";
				} else
					pocketColor = "That's a red pocket.";
			} else if (number >= 11 && number <= 18) {
				if (number % 2 == 0) {
					pocketColor = "That's a red pocket.";
				} else
					pocketColor = "That's a black pocket.";
			} else if (number >= 19 && number <= 28) {
				if (number % 2 == 0) {
					pocketColor = "That's a black pocket.";
				} else
					pocketColor = "That's a red pocket.";
			} else if (number >= 29 && number <= 36) {
				if (number % 2 == 0) {
					pocketColor = "That's a red pocket.";
				} else {
					pocketColor = "That's a black pocket.";
				}
			}
		} else {
			pocketColor = "Out of range";
		}
		return pocketColor;
	}
}
