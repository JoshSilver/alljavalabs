package Ex08;

public class RunnerDriver {

	public static void main(String[] args) {
		Race race = new Race();

		System.out.println(race.getFirstPlace() + " got first place.");
		System.out.println(race.getSecondPlace() + " got second place.");
		System.out.println(race.getThirdPlace() + " got third place.");
	}

}
