package Ex08;

public class Race {
	// Instance Variables
	private Runners runner1;
	private Runners runner2;
	private Runners runner3;

	// Constructor
	public Race() {
		runner1 = new Runners("Josh", 5);
		runner2 = new Runners("Nick", 9);
		runner3 = new Runners("Lin", 4);
	}

	public String getFirstPlace() {
		if (runner1.getRunTime() < runner2.getRunTime() && runner1.getRunTime() < runner3.getRunTime()) {
			return runner1.getRunnerName();
		} else if (runner2.getRunTime() < runner1.getRunTime() && runner2.getRunTime() < runner3.getRunTime()) {
			return runner2.getRunnerName();
		} else
			return runner3.getRunnerName();
	}

	public String getSecondPlace() {
		if (runner1.getRunTime() > runner2.getRunTime() && runner1.getRunTime() < runner3.getRunTime()
				|| runner1.getRunTime() < runner2.getRunTime()) {
			return runner1.getRunnerName();
		} else if (runner2.getRunTime() > runner1.getRunTime() && runner2.getRunTime() < runner3.getRunTime()
				|| runner2.getRunTime() < runner1.getRunTime()) {
			return runner2.getRunnerName();
		} else
			return runner3.getRunnerName();
	}

	public String getThirdPlace() {
		if (runner1.getRunTime() < runner2.getRunTime() && runner1.getRunTime() < runner3.getRunTime()
				|| runner1.getRunTime() > runner2.getRunTime()) {
			return runner1.getRunnerName();
		} else if (runner2.getRunTime() < runner1.getRunTime() && runner2.getRunTime() > runner3.getRunTime()
				|| runner2.getRunTime() > runner1.getRunTime()) {
			return runner2.getRunnerName();
		} else
			return runner3.getRunnerName();
	}
}
