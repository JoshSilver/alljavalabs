package Ex08;

public class Runners {
	// Instance Fields
	private String runner;
	private double runTime;

	// constructor
	public Runners(String runner, double time) {
		this.runner = runner;
		this.runTime = time;
	}

	// Accessor
	public String getRunnerName() {
		return this.runner;
	}

	public double getRunTime() {
		return this.runTime;
	}
}
