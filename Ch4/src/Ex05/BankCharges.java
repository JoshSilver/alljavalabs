package Ex05;

public class BankCharges {
	// Instance Fields
	private double accountTotal;
	private int numOfChecks;
	final private int MONTHLYFEE = 10;

	// Mutators
	public void setAccountTotal(double accountTotal) {
		this.accountTotal = accountTotal;
	}

	public void setNumOfChecks(int numOfChecks) {
		this.numOfChecks = numOfChecks;
	}

	// Instance Method
	public double calcServiceFees(int numOfChecks) {

		// local variables
		double fee = 0;

		if (accountTotal < 400) {
			accountTotal -= 15;
			fee += 15;
		}
		if (numOfChecks < 20) {
			fee += (numOfChecks * .10) + MONTHLYFEE;
			accountTotal -= (numOfChecks * .10) + MONTHLYFEE;
		} else if (numOfChecks >= 20 && numOfChecks <= 39) {
			fee += (numOfChecks * .08) + MONTHLYFEE;
			accountTotal -= (numOfChecks * .08) + MONTHLYFEE;
		} else if (numOfChecks >= 40 && numOfChecks <= 59) {
			fee += (numOfChecks * .06) + MONTHLYFEE;
			accountTotal -= (numOfChecks * .06) + MONTHLYFEE;
		} else if (numOfChecks >= 60) {
			fee += (numOfChecks * .04) + MONTHLYFEE;
			accountTotal -= (numOfChecks * .04) + MONTHLYFEE;
		}
		return fee;
	}

	public double getAccountTotal() {
		return accountTotal;
	}
}
