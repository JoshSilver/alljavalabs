package Ex05;

import java.util.Scanner;

public class BankChargesDriver {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		BankCharges bankCharge = new BankCharges();

		System.out.println("What is the initial balance of the account?");
		double balance = keyboard.nextDouble();
		bankCharge.setAccountTotal(balance);

		System.out.println("How many checks did you write?");
		int checksWritten = keyboard.nextInt();
		bankCharge.setNumOfChecks(checksWritten);
		keyboard.close();

		System.out.printf("Total fees: $%.2f\n", bankCharge.calcServiceFees(checksWritten));
		System.out.printf("Account total: $%.2f\n", bankCharge.getAccountTotal());
	}
}
