package Ex07;

public class FatGram {
	// Instance Fields
	private float fatGrams;
	private float calories;
	private float caloriesFromFat;

	// constructor
	public FatGram(float fatGrams, float calories) {
		this.fatGrams = fatGrams;
		this.calories = calories;
		this.caloriesFromFat = fatGrams * 9;

	}

	// Accessor Method

	public float getCaloriesFromFat() {
		return this.caloriesFromFat;
	}

	public float getCalories() {
		return this.calories;
	}

	// Instance Methods
	public float calcCaloriesFromFatPercentage() {
		float caloriesFromFatPercentage = 0;
		caloriesFromFatPercentage = (this.caloriesFromFat / this.calories) * 100;
		return caloriesFromFatPercentage;
	}
}
