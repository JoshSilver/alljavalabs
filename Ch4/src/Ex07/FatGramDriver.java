package Ex07;

import java.util.Scanner;

public class FatGramDriver {

	public static void main(String[] args) {
		// Instantiate Scanner and FatGram objects
		Scanner keyboard = new Scanner(System.in);

		// Request info from user
		System.out.println("How many grams of fat is the food you ate?");
		float gramsFat = keyboard.nextFloat();

		System.out.println("How many calories are in the food you ate?");
		float inputCalories = keyboard.nextFloat();
		keyboard.close();

		// Instantiate FatGram object
		FatGram fatGram = new FatGram(gramsFat, inputCalories);
		float caloriesFromFatPercentage = fatGram.calcCaloriesFromFatPercentage();
		float caloriesFromFat = fatGram.getCaloriesFromFat();
		float totalCalories = fatGram.getCalories();

		if (caloriesFromFatPercentage < 30) {
			System.out.printf("The Percentage of Calories from fat is %.2f%%, which means it's low in fat.",
					caloriesFromFatPercentage);
		} else if (caloriesFromFatPercentage > 30 && caloriesFromFatPercentage < 100) {
			System.out.printf("The Percentage of Calories from fat is %.2f%%.", caloriesFromFatPercentage);
		} else if (caloriesFromFatPercentage > 100) {
			System.out.printf("Error!! More fat calories than total calories in impossible!");
		}

		// System.out.printf("The Percentage of Calories from fat is %.2f%%.",
		// fatGram.calcCaloriesFromFat());
	}

}
