package Ex06;

public class ShippingCharges {
	// Instance Fields
	private double packageWeight;
	private int milesShipped;

	// Mutator Method
	public void setPackageWeight(double packageWeight) {
		this.packageWeight = packageWeight;
	}

	public void setMilesShipped(int milesShipped) {
		this.milesShipped = milesShipped;
	}

	// Instance Method
	public double calcShipping() {
		double shippingCost = 0;
		int perFiveHundredMiles = this.milesShipped / 500;
		if (milesShipped <= 500) {
			if (packageWeight <= 2) {
				shippingCost = 1.10;
			} else if (packageWeight > 2 && shippingCost <= 6) {
				shippingCost = 2.20;
			} else if (packageWeight > 6 && shippingCost <= 10) {
				shippingCost = 3.70;
			} else if (packageWeight > 10) {
				shippingCost = 4.80;
			}
		} else if (milesShipped > 500 && milesShipped % 500 != 0) {
			if (packageWeight <= 2) {
				shippingCost = 1.10 * perFiveHundredMiles + 1;
			} else if (packageWeight > 2 && shippingCost <= 6) {
				shippingCost = 2.20 * perFiveHundredMiles + 1;
			} else if (packageWeight > 6 && shippingCost <= 10) {
				shippingCost = 3.70 * perFiveHundredMiles + 1;
			} else if (packageWeight > 10) {
				shippingCost = 4.80 * perFiveHundredMiles + 1;
			}
		} else if (milesShipped > 500 && milesShipped % 500 == 0) {
			if (packageWeight <= 2) {
				shippingCost = 1.10 * perFiveHundredMiles;
			} else if (packageWeight > 2 && shippingCost <= 6) {
				shippingCost = 2.20 * perFiveHundredMiles;
			} else if (packageWeight > 6 && shippingCost <= 10) {
				shippingCost = 3.70 * perFiveHundredMiles;
			} else if (packageWeight > 10) {
				shippingCost = 4.80 * perFiveHundredMiles;
			}
		}
		return shippingCost;
	}
}
