package Ex06;

import java.util.Scanner;

public class ShippingChargesDriver {

	public static void main(String[] args) {
		// Instantiate Scanner and ShippingCharges objects
		Scanner keyboard = new Scanner(System.in);
		ShippingCharges shippingCharge = new ShippingCharges();

		// Request info from user
		System.out.print("How much does your package weigh in kg?");
		double packageWeight = keyboard.nextDouble();
		shippingCharge.setPackageWeight(packageWeight);

		System.out.print("How far does it need to ship?");
		int milesShipped = keyboard.nextInt();
		keyboard.close();
		shippingCharge.setMilesShipped(milesShipped);

		System.out.printf("The total cost of shipping for %.2fkg is $%.2f.", packageWeight,
				shippingCharge.calcShipping());
	}

}
