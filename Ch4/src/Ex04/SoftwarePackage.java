package Ex04;

public class SoftwarePackage {
	// Instance Fields
	private final float PRICE = 99;
	private int numUnitSold;

	// Constructor
	public SoftwarePackage() {

	}

	// Mutator
	public void setNumUnitSold(int numUnitSold) {
		this.numUnitSold = numUnitSold;
	}

	// Instance Method
	public float totalCost() {
		float totalWithDiscount = 0;
		float total = numUnitSold * PRICE;
		if (numUnitSold < 10) {
			totalWithDiscount = total;
			System.out.println("Less than 10.");
		} else if (numUnitSold >= 10 && numUnitSold <= 19) {
			totalWithDiscount = total - (total * .2f);
			System.out.println("10-19.");
		} else if (numUnitSold >= 20 && numUnitSold <= 49) {
			totalWithDiscount = total - (total * .3f);
			System.out.println("20-49.");
		} else if (numUnitSold >= 50 && numUnitSold <= 99) {
			totalWithDiscount = total - (total * .4f);
			System.out.println("50-99.");
		} else if (numUnitSold >= 100) {
			totalWithDiscount = total - (total * .5f);
			System.out.println("100 or more.");
		}
		return totalWithDiscount;
	}
}
