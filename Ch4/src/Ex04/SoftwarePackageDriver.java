package Ex04;

import java.util.Scanner;

public class SoftwarePackageDriver {

	public static void main(String[] args) {
		// Instantiate Scanner and SoftwarePackage
		Scanner keyboard = new Scanner(System.in);
		SoftwarePackage softwarePackage = new SoftwarePackage();

		// Request Info from user
		System.out.println("How many Software Packages are you buying?");
		int numUnitSold = keyboard.nextInt();
		keyboard.close();

		// Set the number of units sold
		softwarePackage.setNumUnitSold(numUnitSold);

		// Print total cost of packages
		System.out.printf("Total cost: $%.2f", softwarePackage.totalCost());
	}
}
