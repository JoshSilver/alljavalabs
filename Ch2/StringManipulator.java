import java.util.Scanner;
public class StringManipulator {
	public static void main(String[] args) 
	{
		String favCity;
		int cityLength;
		String cityUpper;
		String cityLower;
		String firstChar;
		
		//create Scanner
		Scanner keyboard = new Scanner(System.in);
	
		//prompt user for a string of favorite city
		System.out.println("Please enter the name of your favorite city.");
		favCity = keyboard.nextLine();
		
		cityLength = favCity.length();
		cityUpper = favCity.toUpperCase();
		cityLower = favCity.toLowerCase();
		firstChar = favCity.substring(0,1);
		
		System.out.println(cityLength);
		System.out.println(cityUpper);
		System.out.println(cityLower);
		System.out.println(firstChar);
	}
}