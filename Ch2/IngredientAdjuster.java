import java.util.Scanner;
public class IngredientAdjuster {
	public static void main(String[] args) 
	{
		//variables
		int cookies;
		float sugar;
		float butter;
		float flour;
		
		//create Scanner
		Scanner keyboard = new Scanner(System.in);
	
		//prompt user for an int
		System.out.println("How many cookies do you want to make?");
		cookies = keyboard.nextInt();
		
		//calculate sugar
		sugar = (1.5f / 48f) * (float)cookies;
		//calculate butter
		butter = (1f / 48f) * (float)cookies;
		//calculate flour
		flour = (2.75f / 48f) * (float)cookies;
		
		//print to screen
		System.out.println("To make " + cookies + " cookies, you will need " + sugar + " cups of sugar, " +
			butter + " cups of butter and " + flour + " cups of flour.");
	}
}