import java.util.Scanner;

public class TestAverage {
	public static void main(String[] args)
	{
		double testScore1;
		double testScore2;
		double testScore3;
		double average;
		
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user for a double
		System.out.println("Please enter 3 test scores.");
		testScore1 = keyboard.nextDouble();
		testScore2 = keyboard.nextDouble();
		testScore3 = keyboard.nextDouble();
		
		average = ((testScore1 + testScore2 + testScore3) / 3);
		
		System.out.println("First test: " + testScore1);
		System.out.println("Second test: " + testScore2);
		System.out.println("Third test: " + testScore3);
		System.out.println("Average: " + average);
	} 
}