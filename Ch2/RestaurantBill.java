import java.util.Scanner;
import java.text.*;
public class RestaurantBill {
	public static void main(String[] args)
	{
		//variables
		float tax = .075f;
		float tip = .18f;
		float tipAmount;
		float taxAmount;
		float bill;
		float total;
		
		//formats bill, taxAmount, tipAmount, and total
		NumberFormat nf = new DecimalFormat("$#,###.00");
		
		//scanner to accept user input
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user for a float
		System.out.println("How much was the bill of your meal?");
		
		//calculate
		bill = keyboard.nextFloat();
		tipAmount = bill * tip;
		taxAmount = bill * tax;
		total = bill + tipAmount + taxAmount;
		
		//print information
		System.out.println("Meal charge: " + nf.format(bill));
		System.out.println("Tax amount: " + nf.format(taxAmount));
		System.out.println("Tip amount: " + nf.format(tipAmount));
		System.out.println("Total bill: " + nf.format(total));
	}
}