public class EnergyDrinkConsumption {
	public static void main(String[] args) 
	{
		//variables
		int totalSurveyed = 15000;
		final float ONEORMORE = 0.18f;
		final float CITRUSDRINKS = 0.58f;
		int oneOrMoreTotal;
		int citrusTotal;
		
		oneOrMoreTotal = (int)((float)totalSurveyed * (float)ONEORMORE);
		citrusTotal = (int)((float)oneOrMoreTotal * (float)CITRUSDRINKS);
		
		//print to screen
		System.out.printf("A total of %d people bought one or more energy drinks per week and %d people preferred citrus flavors.", oneOrMoreTotal, citrusTotal);
	}
}