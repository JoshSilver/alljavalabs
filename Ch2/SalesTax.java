import java.util.Scanner;

public class SalesTax {
	public static void main(String[] args)
	{
		double purchase;
		double totalSalesTax;
		double total;
		double stateSalesTax = .055;
		double countySalesTax = .02;
		
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user for a double
		System.out.println("Enter the amount of your purchase.");
		purchase = keyboard.nextDouble();
		
		totalSalesTax = (purchase * stateSalesTax) + (countySalesTax * purchase);
		total = purchase + totalSalesTax;
		
		System.out.println("Purchase amount: " + purchase);
		System.out.println("County Sales Tax: " + countySalesTax);
		System.out.println("State Sales Tax: " + stateSalesTax);
		System.out.println("Total Sales Tax: " + totalSalesTax);
		System.out.println("Total: " + total);
		
	} 
}