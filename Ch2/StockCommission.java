import java.util.Scanner;
import java.text.*;
public class StockCommission {
	public static void main(String[] args)
	{
		//variables
		float stockAmount = 1000f;
		float commissionAmount = 0.02f;
		float pricePerShare = 25.50f;
		float stockAmountPaid;
		float commissionAmountPaid;
		float totalAmountPaid; // stockAmount + commissionAmount
		
		//formats bill, taxAmount, tipAmount, and total
		NumberFormat nf = new DecimalFormat("$#,###.##");
		
		//scanner to accept user input
		Scanner keyboard = new Scanner(System.in);
		
		//calculate
		stockAmountPaid = stockAmount * pricePerShare;
		commissionAmountPaid = stockAmountPaid * commissionAmount;
		totalAmountPaid = stockAmountPaid + commissionAmountPaid;
		
		//print information
		System.out.println(nf.format(stockAmountPaid));
		System.out.println(nf.format(commissionAmountPaid));
		System.out.println(nf.format(totalAmountPaid));
	}
}