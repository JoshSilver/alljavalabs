public class Initials {
	public static void main(String[] args)
	{
		String firstName = "Joshua";
		String middleName = "Stuart";
		String lastName = "Silver";
		char firstInitial = 'J';
		char secondInitial = 'S';
		char thirdInitial = 'S';
		
		System.out.println("First name: " + firstName);
		System.out.println("Middle name: " + middleName);
		System.out.println("Last name: " + lastName);
		System.out.println("First initial: " + firstInitial);
		System.out.println("Second initial: " + secondInitial);
		System.out.println("Third initial: " + thirdInitial);
	}
}