import java.util.Scanner;
public class CookieCalories {
	public static void main(String[] args)
	{
		
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user for int
		System.out.println("How many cookies did you eat?");
		int numCookies = keyboard.nextInt();
		
		//calculate calories based on cookies eaten
		int calories = numCookies * 75;
		
		System.out.println("By eating " + numCookies + " cookies you have consumed " + calories + " calories.");
	} 
}