import java.util.Scanner;

public class MaleAndFemalePercentages {
	public static void main(String[] args)
	{
		int male;
		int female;
		float malePercentage;
		float femalePercentage;
		
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user for an int
		System.out.println("How many males are in your class?");
		male = keyboard.nextInt();
		
		//prompt user for an int
		System.out.println("How many females are in your class?");
		female = keyboard.nextInt();
		
		malePercentage = ((float)male / ((float)male + (float)female)) * 100;
		femalePercentage = ((float)female / ((float)male + (float)female)) * 100;
		
		System.out.println(malePercentage);
		System.out.println(femalePercentage);
		
	} 
}