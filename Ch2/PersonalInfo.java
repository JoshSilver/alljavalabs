public class PersonalInfo {
	public static void main(String[] args)
	{
		String name = "Josh";
		String address = "2165 Flamingo Dr.";
		String city = "Florissant";
		String state = "Missouri";
		int zip = 63031;
		String phoneNumber = "314-358-6310";
		String collegeMajor = "Information Technology";
		
		System.out.println(name);
		System.out.println(address + " " + city + " " + state);
		System.out.println(phoneNumber);
		System.out.println(collegeMajor);
	}
}