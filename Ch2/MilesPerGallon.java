import java.util.Scanner;

public class MilesPerGallon {
	public static void main(String[] args)
	{
		double milesDriven;
		double gasConsumed; //gallons
		double mpg;
		
		Scanner keyboard = new Scanner(System.in);
		
		//prompt user for a double
		System.out.println("How many miles have you driven?");
		milesDriven = keyboard.nextDouble();
		
		System.out.println("How many gallons of gasoline were consumed?");
		gasConsumed = keyboard.nextDouble();
		
		mpg = milesDriven / gasConsumed;
		
		System.out.println("Miles Per Gallon of your vehicle: " + mpg);
	} 
}