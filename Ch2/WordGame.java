import java.util.Scanner;
public class WordGame {
	public static void main(String[] args) 
	{
		//variables
		String name;
		String age;
		String city;
		String college;
		String profession;
		String animal;
		String animalName;
		
		//create Scanner
		Scanner keyboard = new Scanner(System.in);
		
		//gather information from the user
		System.out.println("Please enter your name...");
		name = keyboard.nextLine();
		
		System.out.println("Please enter your age...");
		age = keyboard.nextLine();
		
		System.out.println("Please enter your city...");
		city = keyboard.nextLine();
		
		System.out.println("Please enter your college...");
		college = keyboard.nextLine();
		
		System.out.println("Please enter your profession...");
		profession = keyboard.nextLine();
		
		System.out.println("Please enter your favorite animal...");
		animal = keyboard.nextLine();
		
		System.out.println("Please enter what you would name your favorite animal...");
		animalName = keyboard.nextLine();
		
		//display information to the user
		System.out.printf("There once was a person named %s who lived in %s.  At the age of %s, \n" + 
		"%s went to college at %s.  %s graduated and went to work as a %s.\n" +
		" Then, %s adopted an %s named %s. They both lived happily ever after!", name, city, age, name, college, name, profession, name, animal, animalName);
	}
}