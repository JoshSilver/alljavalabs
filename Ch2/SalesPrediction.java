
public class SalesPrediction {
	public static void main(String[] args)
	{
		double sales = 8300000;
		double eastCoastSales = .65;
		
		double totalSales = sales * eastCoastSales;
		
		System.out.println("The East Coast Sales division has totaled " + totalSales + ".");
		
	} 
}