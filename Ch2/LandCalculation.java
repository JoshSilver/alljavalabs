public class LandCalculation {
	public static void main(String[] args)
	{
		double acre = 43560;
		double tract = 389767;
		
		double numAcres = tract / acre;
		
		System.out.println("There are " + numAcres + " acres in " + tract + " square feet of land." );
		
	} 
}